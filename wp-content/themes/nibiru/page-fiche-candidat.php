<?php
/*
Template Name: Page Formnulaire candidats
*/
?>
<?php 
get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 
echo do_shortcode("[nav_bar_noslide slider='none']");
?>

<?php 
$action = $_POST['action'];
$sexe = esc_html( $_POST['sexe'] );
$nom = esc_html( $_POST['nom'] );
$prenom = esc_html( $_POST['prenom'] );
$adresse = esc_html( $_POST['adresse'] );
$codepostal = esc_html( $_POST['codepostal'] );
$ville = esc_html( $_POST['ville'] );
$teldomicile = esc_html( $_POST['teldomicile'] );
$telbureau = esc_html( $_POST['telbureau'] );
$cellulaire = esc_html( $_POST['cellulaire'] );
$mail = esc_html( $_POST['mail'] );
$authorCoordPro = esc_html( $_POST['authorCoordPro'] );
$facebook = esc_html( $_POST['facebook'] );
$twitter = esc_html( $_POST['twitter'] );
$autresReseaux = esc_html( $_POST['autresReseaux'] );
$connuParSiteReseau = esc_html( $_POST['connuParSiteReseau'] );
$connuParContact = esc_html( $_POST['connuParContact'] );
$connuParAutre = esc_html( $_POST['connuParAutre'] );
$typeContrat = esc_html( $_POST['typeContrat'] );
$tempsContrat = esc_html( $_POST['tempsContrat'] );
$objectifSalaire = esc_html( $_POST['objectifSalaire'] );
$secteur = esc_html( $_POST['secteur'] );
$scteurAutre = esc_html( $_POST['scteurAutre'] );
$objectifsPro = esc_html( $_POST['objectifsPro'] );
$secteurEmploi = esc_html( $_POST['secteurEmploi'] );
$mobilite = esc_html( $_POST['mobilite'] );
$newsletter = esc_html( $_POST['newsletter'] );
$listeDiffusion = esc_html( $_POST['listeDiffusion'] );
$offresService = esc_html( $_POST['offresService'] );
$engagement = esc_html( $_POST['engagement'] );
if ( $newsletter == "") {
	$newsletter = "Non";
}
if ( $listeDiffusion == "") {
	$listeDiffusion = "Non";
}
if ( $offresService == "") {
	$offresService = "Non";
}
if ( $engagement == "") {
	$engagement = "Non";
}

if ($action == "add") {
	
	$admin_to      = 'info@sicotterecrutement.ca';
    $admin_subject = 'Nouvelle fiche de renseignement candidat';
    $admin_message = "<table>
					
						<tr>
							<td>Sexe:</td>
							<td>".$sexe."</td>
						</tr>

						<tr>
							<td>Nom:</td>
							<td>".$nom."</td>
						</tr>
						<tr>
							<td>Prénom:</td>
							<td>".$prenom."</td>
						</tr>

						<tr>
							<td>Adresse:</td>
							<td>".$adresse."</td>
						</tr>

						<tr>
							<td>Code postal:</td>
							<td>".$codepostal."</td>
						</tr>
						<tr>
							<td>Ville:</td>
							<td>".$ville."</td>
						</tr>

						<tr>
							<td>Téléphone domicile:</td>
							<td>".$teldomicile."</td>
						</tr>
						<tr>
							<td>Bureau:</td>
							<td>".$telbureau."</td>
						</tr>

						<tr>
							<td>Cellulaire:</td>
							<td>".$cellulaire."</td>
						</tr>
						<tr>
							<td>@</td>
							<td>".$mail."</td>
						</tr>

						<tr>
							<td>Pouvons-nous vous joindre à vos coordonnées professionnelles:</td>
							<td>".$authorCoordPro."</td>
						</tr>

						<tr>
							<td>
								<h3><?php echo __('Réseaux sociaux') ?></h3>
							</td>
							<td></td>
						</tr>

						<tr>
							<td>Facebook:</td>
							<td>".$facebook."</td>
						</tr>
						<tr>
							<td>Twitter:</td>
							<td>".$twitter."</td>
						</tr>
						<tr>
							<td>Autres:</td>
							<td>".$autresReseaux."</td>
						</tr>

						<tr>
							<td><h3>Comment avez-vous entendu parler de SICOTTE Recrutement:</h3></td>
							<td></td>
						</tr>
						<tr>
							<td>Internet</td>
							<td></td>
						</tr>
						<tr>
							<td>Site ou réseau social:</td>
							<td>".$connuParSiteReseau."</td>
						</tr>
						<tr>
							<td>Contact</td>
							<td></td>
						</tr>
						<tr>
							<td>Nom de votre contact:</td>
							<td>".$connuParContact."</td>
						</tr>
						<tr>
							<td>Autres:</td>
							<td></td>
						</tr>
						<tr>
							<td>Precisez:</td>
							<td>".$connuParAutre."</td>
						</tr>

						<tr>
							<td><h3>Objectifs professionnels:</h3></td>
							<td></td>
						</tr>

						<tr>
							<td>Type de contrat:</td>
							<td></td>
						</tr>
						<tr>
							<td>".$typeContrat."</td>
							<td>".$tempsContrat."</td>
						</tr>

						<tr>
							<td>Objectif salarial:</td>
							<td>".$objectifSalaire." K$</td>
						</tr>

						<tr>
							<td>Votre secteur d'activité:</td>
							<td>".$secteur."</td>
						</tr>
						<tr>
							<td>Si autre, precisez:</td>
							<td>".$scteurAutre."</td>
						</tr>

						<tr>
							<td>Vos objectifs professionnels à court, moyen et long termes:</td>
							<td>".$objectifsPro."</td>
						</tr>

						<tr>
							<td>Secteur géographique de votre recherche d'emploi:</td>
							<td>".$secteurEmploi."</td>
						</tr>

						<tr>
							<td>Mobilité:</td>
							<td>".$mobilite."</td>
						</tr>

						<tr>
							<td><h3>Mises à jour et offres:</h3></td>
							<td></td>
						</tr>

						<tr>
							<td>Souhaitez-vous recevoir nos Newsletter?</td>
							<td>".$newsletter."</td>
						</tr>
						<tr>
							<td>Souhaitez-vous faire partie de notre liste de diffusion d\'offres d\'emplois?</td>
							<td>".$listeDiffusion."</td>
						</tr>
						<tr>
							<td>Souhaitez-vous recevoir notre offre de services de Profil Professionel en Ligne?</td>
							<td>".$offresService."</td>
						</tr>

						<tr>
							<td>Engagement à ne pas contacter notre client et à garder ces informations confidentielles</h3></td>
							<td>".$engagement."</td>
						</tr>

					</table>";
            
    $admin_headers  = 'MIME-Version: 1.0' . "\r\n";
    $admin_headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $admin_headers .= 'From: "' . $nom . '"<' . $mail . '>' . "\r\n";
    $envoiMail = wp_mail( $admin_to, $admin_subject, $admin_message, $admin_headers );

    if ($envoiMail == true) {
    	echo "<script>alert('Votre fiche nous a bien été transmise')</script>"; 
    }

}

?>


<div class="po-page">
		
	<div class="container" style="width: 60%;">
		
		<div class="row" style="text-align: center; margin-top: 80px; margin-bottom: 80px;">
			
			<span class="fa fa-user fa-4x" style="color: #f4545f; font-size: 120px;"></span>
			<h1>FICHE RENSEIGNEMENTS CANDIDAT</h1>

		</div>

		<div class="row" style="background-color: rgb(245,245,245); padding: 10px;">
			
			<form id='formSubmission' method='POST' action="" enctype="multipart/form-data">
				<table width="100%" cellpadding="10" cellspacing="5">
					
					<tr>
						<td class="td-2"><?php echo __('Sexe') ?></td>
						<td class="td-2">
							<select name="sexe">
								<option value="M">M</option>
								<option value="F">F</option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Nom') ?></td>
						<td class="td-2"><input type="text" name="nom"></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Prénom') ?></td>
						<td class="td-2"><input type="text" name="prenom"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Adresse') ?></td>
						<td class="td-2"><input type="text" name="adresse"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Code postal') ?></td>
						<td class="td-2"><input type="text" name="codepostal"></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Ville') ?></td>
						<td class="td-2"><input type="text" name="ville"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Téléphone domicile') ?></td>
						<td class="td-2"><input type="tel" name="teldomicile"></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Bureau') ?></td>
						<td class="td-2"><input type="tel" name="telbureau"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Cellulaire') ?></td>
						<td class="td-2"><input type="tel" name="cellulaire"></td>
					</tr>
					<tr>
						<td class="td-2">@</td>
						<td class="td-2"><input type="email" name="mail"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Pouvons-nous vous joindre à vos coordonnées professionnelles') ?></td>
						<td class="td-2">
							<select name="authorCoordPro">
								<option value="oui"><?php echo __('Oui') ?></option>
								<option value="non"><?php echo __('Non') ?></option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;">
							<h3><?php echo __('Réseaux sociaux') ?></h3>
						</td>
						<td></td>
					</tr>

					<tr>
						<td class="td-2">Facebook</td>
						<td class="td-2"><input name="facebook"></td>
					</tr>
					<tr>
						<td class="td-2">Twitter</td>
						<td class="td-2"><input name="twitter"></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Autres') ?></td>
						<td class="td-2"><input name="autresReseaux"></td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;"><h3><?php echo __('Comment avez-vous entendu parler de SICOTTE Recrutement?') ?></h3></td>
						<td></td>
					</tr>
					<tr>
						<td class="td-1" style="font-weight: bold;">Internet</td>
						<td></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Site ou réseau social:') ?></td>
						<td class="td-2"><input type="text" name="connuParSiteReseau"></td>
					</tr>
					<tr>
						<td class="td-1" style="font-weight: bold;">Contact</td>
						<td></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Nom de votre contact:') ?></td>
						<td class="td-2"><input type="text" name="connuParContact"></td>
					</tr>
					<tr>
						<td class="td-1" style="font-weight: bold;"><?php echo __('Autres') ?></td>
						<td></td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Precisez:') ?></td>
						<td class="td-2"><input type="text" name="connuParAutre"></td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;"><h3><?php echo __('Objectifs professionnels') ?></h3></td>
						<td></td>
					</tr>

					<tr>
						<td class="td-1" style="font-weight: bold;"><?php echo __('Type de contrat') ?></td>
						<td></td>
					</tr>
					<tr>
						<td class="td-2">
							<select name="typeContrat">
								<option value="stage"><?php echo __('Stage') ?></option>
								<option value="permanent"><?php echo __('Poste permanent') ?></option>
								<option value="temporaire"><?php echo __('Poste temporaire') ?></option>
							</select>
						</td>
						<td class="td-2">
							<select name="tempsContrat">
								<option value="tempsPartiel"><?php echo __('Temps partiel') ?></option>
								<option value="tempsPlein"><?php echo __('Temps plein') ?></option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Objectif salarial') ?></td>
						<td class="td-2"><input type="number" name="objectifSalaire">K$</td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Votre secteur d\'activité') ?></td>
						<td class="td-2">
							<select name="secteur">
								<option value="marketingCommMedia"><?php echo __('Marketing, communication et médias') ?></option>
								<option value="environnement"><?php echo __('Environnement') ?></option>
								<option value="juridique"><?php echo __('Juridique') ?></option>
								<option value="informatique"><?php echo __('Informatique') ?></option>
								<option value="sante"><?php echo __('Santé') ?></option>
								<option value="vitivinicole"><?php echo __('Secteur vitivinicole') ?></option>
								<option value="aeronautique"><?php echo __('Aéronautique') ?></option>
								<option value="autre"><?php echo __('Autre') ?></option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="td-2"><?php echo __('Si autre, precisez:') ?></td>
						<td class="td-2"><input type="text" name="scteurAutre"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Vos objectifs professionnels à court, moyen et long termes') ?></td>
						<td class="td-2"><input type="text" name="objectifsPro"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Secteur géographique de votre recherche d\'emploi') ?></td>
						<td class="td-2"><input type="text" name="secteurEmploi"></td>
					</tr>

					<tr>
						<td class="td-2"><?php echo __('Mobilité') ?></td>
						<td class="td-2">
							<select name="mobilite">
								<option value="voiture"><?php echo __('Voiture') ?></option>
								<option value="transport"><?php echo __('Transports en commun') ?></option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;">
							<h3><?php echo __('Mises à jour et offres') ?></h3>
						</td>
						<td></td>
					</tr>

					<tr>
						<td class="">
							<input type="checkbox" name="newsletter" value="Oui"><?php echo __('Souhaitez-vous recevoir nos Newsletter?') ?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="">
							<input type="checkbox" name="listeDiffusion" value="Oui"><?php echo __('Souhaitez-vous faire partie de notre liste de diffusion d\'offres d\'emplois?') ?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="">
							<input type="checkbox" name="offresService" value="Oui"><?php echo __('Souhaitez-vous recevoir notre offre de services de Profil Professionel en Ligne?') ?>
						</td>
						<td></td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;">
							<div style="border: 1px solid; padding: 12px;">
								<h4 style="font-weight: bold;"><?php echo __('Profil Professionnel en Ligne') ?></h4>
								<h6><?php echo __('SICOTTE Recrutement est conscient de l\'émergence des réseaux sociaux et de l\'impact de ce phénomène sur le marche de l\'emploi.') ?></br>
								<?php echo __('Puisqu\'il est plus que jamais vital de soigner sa visibilité sur le web et tout particulièrement dans une perspective de recherche d\'emploi, SICOTTE Recrutement met à la disposition de ses candidats un service permettant de mettre chaque candidature en valeur grâce au ') ?><span style="font-weight: bold;"><?php echo __('Profil Professionnel en Ligne') ?></span><?php echo __('. Informez-vous dès maintenant auprès de votre recruteur de talent SICOTTE Recrutement.') ?></h6>
							</div>
						</td>
						<td></td>
					</tr>

					<tr>
						<td class="td-1" style="padding-top: 50px;">
							<h3><?php echo __('Engagement à ne pas contacter notre client et à garder ces informations confidentielles') ?></h3>
						</td>
						<td></td>
					</tr>

					<tr>
						<td class="td-1">
							<p><?php echo __('Comme condition à la consideration de ma demande d\'emploi, je m\'engage envers SICOTTE Recrutement à ne pas contacter l\'entreprise en besoin de recrutement. Je prends conscience que si une telle situation veanit à se produire, ma candidature serait écartée d\'office. Je comprends que SICOTTE Recrutement respecte les conditions de confidentialité et de discrétion demandées par ses clients.') ?></p><input type="checkbox" name="engagement" value="Oui">
						</td>
						<td></td>
					</tr>
					<input type='hidden' name='action' value='add' />
					<tr>
						<td class="td-1" style="padding-top: 75px;">
							<input class="boutSubmit" type='submit' value='<?php echo __("Envoyez vos renseignements"); ?>' name='submit'>
						</td>
						<td></td>
					</tr>

				</table>
			</form>

		</div>

	</div>		

	<div style="height: 2px; background-color: #848484; width: 12%; margin-left: 44%; margin-top: 2%;"></div>

    <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

    <div class="clearfix"></div>

    <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

    <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
    <?php echo do_shortcode('[footerSicotte]'); ?>

</div>

<style type="text/css">
	
.po-page {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg');
}

body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p, 
body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
    color: #F4545F;
}

.icon-box { zoom: 200%; }

#nblikefacebook {
	zoom: 100% !important;
}

.footer-container-bottom {
	display: none;
}

td {
	width: 50%;
}

.td-1 > div, .td-1 > h3, .td-1 > input, .td-1 > p {
	width: 200%;
}

.td-2 {
	font-size: 18px;
}

.td-1 {
	font-size: 20px;
}

.td-2 {
	border-bottom: 1px dotted;
	padding-top: 5px;
	padding-bottom: 5px;
}

input[type="text"] {
	width: 100%;
}

input[type="checkbox"] {
	margin-right: 10px;
}

@media (max-width: 768px) {
	.container {
		width: 100% !important;
	}
	table {
		/*zoom: 50%;
        -moz-transform: scale(0.5);
        -webkit-transform: scale(1.0);*/
        table-layout: fixed;
	}
	td {
		white-space: -o-pre-wrap; 
	    word-wrap: break-word;
	    white-space: pre-wrap; 
	    white-space: -moz-pre-wrap; 
	    white-space: -pre-wrap;
	} 
	td > select, td > input {
		width: 90%;
	}
}

@media (max-width: 330px) {
	body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
		font-size: 17px;
	}
}


</style>

<?php get_footer(); ?>