<?php
/*
Template Name: Page Marketing de recrutement
*/
?>
<?php 
get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 
echo do_shortcode("[nav_bar_noslide slider='none']");
?>


<div class="po-page">
		
	<div class="container">
		
		<div class="row" style="text-align: center; margin-top: 50px; margin-bottom: 50px;">

			<span class="fa fa-bar-chart" style="color: #f4545f; font-size: 120px;"></span>
			
			<h1 style="color: #848484; margin-bottom: 50px; font-weight: bold;"><?php echo __('Marketing de recrutement') ?></h1>

			<h3><?php echo __('PROMOUVEZ VOTRE OFFRE AVEC NOTRE SERVICE D\'AFFICHAGE') ?></h3>
			<h5><?php echo __('Vous possédez votre propre service de recrutement et souhaitez bénéficier de notre service d\'affichage?') ?></h5>

		</div>

		<div class="row">

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">250$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li> 
				</ul>
			</div>

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">500$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li>
				<li><?php echo __('Affichage ciblé sur plus de 13 sites d\'emploi généralistes gratuits') ?></li> 
				</ul>
			</div>
			
		</div>

		<div class="row">

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">750$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li>
				<li><?php echo __('Affichage ciblé sur plus de 13 sites d\'emploi généralistes gratuits') ?></li> 
				<li><?php echo __('Rédaction d’un article détaillé sur votre offre sur '); ?><span style="font-weight: bold;">APOLLO1.TV</span><?php echo __(' section carrières (4000 visiteurs uniques par mois) ainsi que sur le portail '); ?><span style="font-weight: bold;">La Réussite</span><?php echo __(' de La '); ?><span style="font-weight: bold;">Métropole.com</span><?php echo __(' (plus de 150 000 visiteurs uniques par mois)') ?></li>
				</ul>
			</div>

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">1000$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li>
				<li><?php echo __('Affichage ciblé sur plus de 13 sites d\'emploi généralistes gratuits') ?></li> 
				<li><?php echo __('Rédaction d’un article détaillé sur votre offre sur '); ?><span style="font-weight: bold;">APOLLO1.TV</span><?php echo __(' section carrières (4000 visiteurs uniques par mois) ainsi que sur le portail '); ?><span style="font-weight: bold;">La Réussite</span><?php echo __(' de La '); ?><span style="font-weight: bold;">Métropole.com</span><?php echo __(' (plus de 150 000 visiteurs uniques par mois)'); ?></li>
				<li><?php echo __('Transmission ciblée de l\'offre d\'emploi aux candidats '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' (près de 30000 membres)') ?></li>
				</ul>
				
			</div>
			
		</div>

		<div class="row">

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">1250$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li>
				<li><?php echo __('Affichage ciblé sur plus de 13 sites d\'emploi généralistes gratuits') ?></li> 
				<li><?php echo __('Rédaction d’un article détaillé sur votre offre sur '); ?><span style="font-weight: bold;">APOLLO1.TV</span><?php echo __(' section carrières (4000 visiteurs uniques par mois) ainsi que sur le portail '); ?><span style="font-weight: bold;">La Réussite</span><?php echo __(' de La '); ?><span style="font-weight: bold;">Métropole.com</span><?php echo __(' (plus de 150 000 visiteurs uniques par mois)'); ?></li>
				<li><?php echo __('Transmission ciblée de l\'offre d\'emploi aux candidats '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' (près de 30000 membres)') ?></li>
				<li><?php echo __('Promotion de votre entreprise et de vos offres aux '); ?><span style="font-weight: bold;">Jeudis d'APOLLO</span><?php echo __(', événements de réseautage professionnel mensuel parrainés par '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' (entre 300 et 1000 participants par mois)'); ?></li>
				</ul>
			</div>

			<div class="col-md-6">
				<h3 style="color: #f4545f; font-weight: bold;">2500$</h3>
				<ul>
				<li><?php echo __('Affichage mensuel sur '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></li>
				<li>
					<div>
						<span><?php echo __('Affichage sur les réseaux sociaux '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span></span>
						<div style="width: 100%;">
							<span class="fa fa-facebook"></span>
							<span class="fa fa-twitter"></span>
							<span class="fa fa-pinterest"></span>
							<span class="fa fa-linkedin"></span>
							<span class="icon-viadeo"></span>
						</div>
					</div>
				</li>
				<li><?php echo __('Affichage ciblé sur plus de 13 sites d\'emploi généralistes gratuits') ?></li> 
				<li><?php echo __('Rédaction d’un article détaillé sur votre offre sur '); ?><span style="font-weight: bold;">APOLLO1.TV</span><?php echo __(' section carrières (4000 visiteurs uniques par mois) ainsi que sur le portail '); ?><span style="font-weight: bold;">La Réussite</span><?php echo __(' de La '); ?><span style="font-weight: bold;">Métropole.com</span><?php echo __(' (plus de 150 000 visiteurs uniques par mois)'); ?></li>
				<li><?php echo __('Transmission ciblée de l\'offre d\'emploi aux candidats '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' (près de 30000 membres)') ?></li>
				<li><?php echo __('Promotion de votre entreprise et de vos offres aux '); ?><span style="font-weight: bold;">Jeudis d'APOLLO</span><?php echo __(', événements de réseautage professionnel mensuel parrainés par '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' (entre 300 et 1000 participants par mois)'); ?></li>
				<li><?php echo __('Marketing de recrutement: rédaction et production d\'un communiqué spécifique à votre offre en collaboration avec '); ?><span style="font-weight: bold;">APOLLO L'AGENCE</span><?php echo __(' et transmission à 30 000 membres du réseau '); ?><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(' et '); ?><span style="font-weight: bold;">APOLLO L'AGENCE</span></li>
				</ul>
			</div>
			 
		</div>

		<div class="row" style="text-align: center; margin-top: 50px; margin-bottom: 50px;">
			<h3><?php echo __('Choisisez votre forfait. Soumettez votre offre.'); ?></h3>
			<h3><span style="font-weight: bold;">SICOTTE Recrutement</span><?php echo __(', marketing de recrutement'); ?></h3>
			<a href="mailto:marketing@sicotterecrutement.ca"><h3>marketing@sicotterecrutement.ca</h3></a>
			<h4><?php echo __('Pour plus de renseignements'); ?></br><?php echo __('Contactez-nous '); ?><span style="font-weight: bold;">514 360-1304</span></h4>
		</div>

	</div>		

	<div style="height: 2px; background-color: #848484; width: 12%; margin-left: 44%; margin-top: 2%;"></div>

    <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

    <div class="clearfix"></div>

    <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

    <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
    <?php echo do_shortcode('[footerSicotte]'); ?>

</div>

<style type="text/css">
	
.po-page {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg');
}

body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p, 
body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
    color: #F4545F;
}


#nblikefacebook {
	zoom: 100% !important;
}

li {
	list-style: initial;
	list-style-type: square;
}

.col-md-6 > ul > li > div > div >span {
	margin-right: 10px;
	font-size: 25px;
	color: #f4545f;
}

.footer-container-bottom {
	display: none;
}

</style>

<?php get_footer(); ?>