<?php
/*
Template Name: Contact Page
*/


get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");	?>
    
    <div class="po-page">

        <div class="row po-full-width">
        <div class="photoContact col-md-12" style="background-image: url('http://sicotterecrutement.agenceapollo.webfactional.com/wp-content/uploads/2014/12/HeaderAcceuilSicotte.jpg'); background-size: cover; background-repeat: no-repeat; height: 520px;"></div>
        <div class="portfolio-page col-sm-12 column-12" style="background-image: none !important;">
            <div class="liquidcontainerpage">
                <div class="titleHead col-sm-12" style="margin-top: 4%;">
                    <div class="titleTitle">
                        <h1 style="color: #848484;"><?php echo __("NOUS CONTACTER"); ?></h1>
                    </div>
                    <div class="reseauxTitle">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                        <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                        <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                    </div>
                    
                </div>
            </div>
        </div>
    	</div>
            

        <div class="container">
                
            <div class="col-md-12" style="width: 108%; right: 45px;">
                <div class="texteContact" style="background-color: #f0f0f0; margin-bottom: 2%;">
                    <p style="text-align: center;"><?php echo __("SICOTTE Recrutement se situent au 129 rue de la Commune Est, H2Y 1J1, Montréal. Vous pouvez joindre nos services par téléphone au "); ?><a href="tel:+15143601304">514-360-1304</a>, <?php echo __("par courriel à l'adresse"); ?> <a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a>, <?php echo __("ou encore en remplissant le formulaire de contact ci-dessous."); ?></p>
                </div>
            </div>

            <div class="row" style="margin-bottom: 10%;">
                <div class="formulaireContact col-md-6" style="background-color: #f0f0f0; right: 15px;">
                    <?php $url = $_SERVER["REQUEST_URI"];
                    $suburl =  substr($url, 1, 2); 
                    if ($suburl == "en") {
                        echo do_shortcode('[contact-form-7 id="811" title="Formulaire Contact General_copy"]');
                    } else {
                        echo do_shortcode('[contact-form-7 id="333" title="Formulaire Contact General"]');
                    }
                    ?> 
                </div>
                <div class="carteContact col-md-6" style="left: 15px;">
                    <?php echo do_shortcode('[wpgmza id="1"]'); ?>
                </div>
            </div>

        </div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
			
<style type="text/css">
    
.iResTitle {
    font-size: 26px;
    color: #f4545f;
    margin-left: 20px;
    margin-right: 20px;
    margin-bottom: 1%;
    margin-top: 1%;
}

.reseauxTitle {
    margin-bottom: 1%;
}   

.po-page {
    background-image: url('http://sicotterecrutement.agenceapollo.webfactional.com/wp-content/themes/nibiru/images/texturefond.jpg');
}

.formulaireContact, .texteContact {
    padding-top: 2%;
    padding-right: 2%;
    padding-left: 2%;
    padding-bottom: 2%;
}

#wpcf7-f333-o1 > form > p:nth-child(6) > input {
    color: #848484;
    border-color: #848484;
}

#wpcf7-f333-o1 > form > p:nth-child(6) > input:hover {
    color: white;
}

#wpcf7-f333-o1 > form > p:nth-child(5) > span > textarea, #wpcf7-f333-o1 > form > p:nth-child(2) > span > input, #wpcf7-f333-o1 > form > p:nth-child(3) > span > input, #wpcf7-f333-o1 > form > p:nth-child(4) > span > input {
    color: #848484;
}

#wpgmza_map {
    height: 535px !important; 
}

.footer-container, .footer-container-bottom {
    visibility: hidden;
    height: 0px;
}

.carteContact {
    padding-right: 0px;
    padding-left: 0px;
}

body > div.container.po-container-section > div {
    position: fixed;
}

body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
    color: #F4545F;
}

.wpcf7 input[type='text'], .wpcf7 input[type='email'], .wpcf7 textarea {
	color: gray !important;
}

#nblikefacebook {
    zoom: 100% !important;
}

@media (max-width: 766px) {
    #wpgmza_map {
        width: 90% !important;
    }
    .formulaireContact {
        right: 0px !important;
        margin-bottom: 15px;
    }
    body > div.po-nav.nav-fixed-padding > div > div.container > div.col-md-12 {
        right: 0px !important;
        width: 100% !important;
        margin-left: auto;
        margin-right: auto;
    }
    body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
        font-size: 75%;
    }
    body > div.po-nav.nav-fixed-padding > div > div.container > div.row {
        width: 95%;
        margin-left: auto;
        margin-right: auto;
    }
    #wpcf7-f333-p330-o1 > form > p:nth-child(6) {
        text-align: center;
    }
    body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width > div.photoContact.col-md-12 {
        height: 90px !important;
    }
}

</style>

<?php get_footer(); ?>

