<?php
/*
Author: Aziz Zahar
*/

// Tableau de correspondance entre les conferenciers et les articles medias associes

$correspTableau = array( 
	148 => 0, // Eric Sicotte
	144 => 1, // Janick Perreault
	1337 => 2, // Lauren Tatner
	1498 => 3, // Glenn Miller
	1336 => 0, // Eric Sicotte ENG
	1495 => 1, // Janick Perreault ENG
	1338 => 2, // Lauren Tatner ENG
	1508 => 3 // Glenn Miller ENG
	);

$racineLien  = get_template_directory_uri();
$racineLien .= '/images/ScreenArticles/';

$confMedia = array(	

	// Eric Sicotte -> 0
	array(
		array (
			'«Petite entrevue des grands réseauteurs» avec Eric Sicotte - Montréal 5à7', 		// titre
			'le 16 juillet 2015', 																// date
			'http://sicotterecrutement.ca/wp-content/uploads/2015/07/montreal-5-a-7.png',		// image 
			'https://www.youtube.com/watch?v=EGl0KQHlVfU'), 									// lien
		array (
			'«L’étiquette dans les 5 à 7 d’affaires» avec Eric Sicotte - APOLLO1.TV', 		// titre
			'le 2 mars 2015', 																// date
			''.$racineLien.'1.jpg',	// image 
			'http://apollo1.tv/conference-reseautage-affaires-eric-sicotte/'), 				// lien
		array (
			'Conférence "L\'étiquette dans les 5 à 7 d\'affaires" - Groupe Réso -  APOLLO1.tv', 
			'le 26 février 2015', 
			''.$racineLien.'2.jpg', 
			'http://apollo1.tv/conference-eric-sicotte-letiquette-dans-les-5-7-daffaires-26-fevrier-2015/' ),
		array (
			'Conférence "Le pouvoir du savoir-être et le marketing de soi" - Université de Montréal - APOLLO1.tv', 
			'le 02 décembre 2014', 
			''.$racineLien.'3.png', 
			'http://apollo1.tv/eric-sicotte-en-conference-au-pavillon-marie-victorin-de-luniversite-de-montreal/' ),
		array (
			'Lancement médiatique "Emission SoFy" - ICI Télé', 
			'le 15 octobre 2014', 
			''.$racineLien.'4.png', 
			'http://www.agenceapollo.ca/lancement_mediatique_emission_sofy/' ),
		array (
			'"Le marketing de soi ou l’art et la manière de se présenter pour obtenir l’emploi de ses rêves" - Époque Times -  10ème année, numéro 13', 
			'23 juin au 06 juillet 2014', 
			''.$racineLien.'5.jpg', 
			'http://www.epochtimes.fr/fr/pdf/ET_Montreal_2014_06_23.pdf' ),
		array (
			'Conférence "L’art et la manière de se présenter" avec Paule Genest, PGPR - Palais des Congrès de Montréal -APOLLO1.tv', 
			'le samedi 10 mai 2014', 
			''.$racineLien.'6.png', 
			'http://apollo1.tv/paule-genest-arp-et-eric-sicotte-en-conference-au-palais-des-congres-de-montreal-dans-le-cadre-du-siiq/' ),
		array (
			'L’art et la manière de se présenter', 
			'le 9 Mai 2014', 
			''.$racineLien.'7.png', 
			'http://www.salonimmigration.com/conferences/lart-et-la-maniere-de-se-presenter/' ),
		array (
			'Eric Sicotte to speak at KNOW-HOW, the annual conference of the Canadian Society for Civil Engineering - APOLLO1.tv', 
			'le 27 mai 2013', 
			''.$racineLien.'8.png', 
			'http://apollo1.tv/eric-sicotte-to-speak-at-know-how-the-annual-conference-of-the-canadian-society-for-civil-engineering/' ),
		array (
			'Entrevue "SoolFoodChilling" avec Keithy Antoine', 
			'le 26 septembre 2013', 
			''.$racineLien.'9.png',
			'http://loungeurbain.com/evenement/entrevue-eric-sicotte-dapollo-lagence/', ),
		array (
			'Conference "Le pouvoir infini des compétences" - Ordre des Ingénieurs Civils - APOLLO1.tv', 
			'le 28 mai 2013', 
			''.$racineLien.'10.png',
			'http://apollo1.tv/eric-sicotte-to-speak-at-know-how-the-annual-conference-of-the-canadian-society-for-civil-engineering/' ),
		array (
			'Conférence "Propulsez votre carrière par l’engagement professionnel" - Association des Clefs d\'Or du Canada - APOLLO1.tv', 
			'le 6 mai 2013', 
			''.$racineLien.'11.png',
			'http://apollo1.tv/eric-sicotte-conferencier-invite-au-9ieme-congres-des-clefs-dor-du-canada-a-quebec/' ),
		array (
			'Conférence sectorielle sur la communication et le marketing de la CCMM - Grande Bibliothèque - APOLLO1.tv', 
			'le 26 février 2013', 
			''.$racineLien.'12.png',
			'http://apollo1.tv/eric-sicotte-et-apollo-lagence-a-la-chambre-de-commerce-du-montreal-metropolitain-le-26-fevrier/' ),
		array (
			'Emission SoFy, Épisode 1, Bell Local, ', 
			'2013', 
			''.$racineLien.'13.png',
			'https://www.youtube.com/watch?v=I_OXN8K03L4' ),
		array (
			'Émission SoFy, Épisode 2, Bell Local, ', 
			'2013', 
			''.$racineLien.'14.png',
			'https://www.youtube.com/watch?v=DajeQR0ThIo' ),
		array (
			'Émission SoFy, Épisode 4, Bell Local, ', 
			'2013', 
			''.$racineLien.'15.png',
			'https://www.youtube.com/watch?v=hO42ITZ75EM' ),
		array (
			'"Les 25 règles d\'or pour les entreprises par Eric Sicotte" - Mila Ajauro - MilasPage', 
			'le 28 février 2012', 
			''.$racineLien.'16.png',
			'http://milaspage.com/10-eric-sicotte-golden-rules-for-start-ups/' ),
		array (
			'Conférence MEEX 4 "Les 25 règles d\'or pour les entreprises" - Studio KAI Design', 
			'le 21 février 2012', 
			''.$racineLien.'17.jpg', 
			'http://www.lametropole.com/article/affaires/la-reussite/25-r%C3%A8gles-d-or-pour-les-entreprises' ),
		array (
			'"Montréal : tout sauf le Paris de l\'Amérique du Nord" - FuckYeahQuebec', 
			'le 4 février 2012', 
			''.$racineLien.'18.png',
			'http://fuckyeahquebec.tumblr.com/post/17044451952/montreal-tout-sauf-le-paris-de-lamerique-du' )
	),

	//Janick Perreault -> 1
	array( 
		array (
			'Commission Bazzo-Dumont - La place des syndicats selon l\'ADQ - 98.5 Fm',
			'le 24 septembre 2010',
			'',
			'http://www.985fm.ca/audioplayer.php?mp3=80146'),
		array (
			'Délit de fuite: Me Janick Perreault - TVA Nouvelles', 		// titre
			'le 21 fevrier 2012', 																// date
			'',	// image 
			'http://tvanouvelles.ca/video/1464747304001'),
		/*
		array (
			'JANICK PERREAULT, Femme en Affaires 2014 - APOLLO L\'Agence',
			'Decembre 2014',
			'',
			'http://agenceapollo.ca/femmes-en-affaires/femmes-en-affaires-2014/janick-perreault/'),
		*/
		array (
			'Un remède contre la procédurite - Lapresse.ca',
			'2 mai 2013',
			'',
			'http://affaires.lapresse.ca/opinions/chroniques/stephanie-grammond/201305/02/01-4646717-un-remede-contre-la-procedurite.php'),
		array (
			'Un webinaire de Me Perreault à ne pas manquer! - Moelle épinière et motricité Québec',
			'7 fevrier 2014',
			'',
			'http://www.moelleepiniere.com/tag/janick-perreault/'),
		array (
			'SAAQ: baisse de 24 % des réclamations depuis 2010 - Huffington Post Québec',
			'25 aout 2014',
			'',
			'http://quebec.huffingtonpost.ca/2014/08/25/saaq-baisse-reclamations-accidents-de-la-route-et-hors-territoire_n_5706249.html'),
		
		array (
			'Me Janick Perreault – Femmes en Affaires 2014 - APOLLO L’AGENCE',
			'5 decembre 2014',
			'',
			'http://apollo1.tv/me-janick-perreault-femmes-en-affaires-apollo-lagence-2014/'),
	),

	//Lauren Tatner -> 2
	array( 
		array (
			'Former Centaur director offers Montreal lawyers acting lessons - Montreal Gazette',
			'1 mai 2012',
			'',
			'http://montrealgazette.com/business/former-centaur-director-offers-montreal-lawyers-acting-lessons?__lsa=a9bd-c1a1'),
		array (
			'Formation continue: des cours de théâtre reconnus par le Barreau! - Droit Inc Quebec',
			'29 juin 2012',
			'',
			'http://www.droit-inc.com/article7938-Formation-continue-des-cours-de-theatre-reconnus-par-le-Barreau'), 
	),

	//Glenn Miller -> 3
	array( 
		array (
			'L’heureuse double vie de Glenn B. Miller - La Metropole',
			'8 juillet 2014',
			''.$racineLien.'glenn-1.JPG',
			'http://www.lametropole.com/article/affaires/la-reussite/l-heureuse-double-vie-de-glenn-b-miller'),
		array (
			'Visionnaire et conférencier expert sur les arts - Gallerie Lisabel',
			'',
			''.$racineLien.'glenn-2.JPG',
			'http://www.lisabel.ca/nouveaux-concepts/'),
		array (
			'Offrez-vous une solution de financement alternatif personnalisée avec MBG Finance - MBG Finance',
			'',
			''.$racineLien.'glenn-3.JPG',
			'http://mbgfinance.com/financement-alternatif/'),
	)

);

?>