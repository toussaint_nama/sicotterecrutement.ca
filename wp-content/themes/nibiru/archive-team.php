<?php
/**
 * The template for displaying the Team page.
 */

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");
	?>
    
    <div class="po-page">

        <div class="entry-header po-portfolio-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                <h3 class="entry-title"><!--<?php echo get_theme_mod( 'team_title','Meet the team'); ?>-->Nos Conférenciers</h3>
                </div>
                <div class="col-sm-6 visible-lg hidden-xs">
                    <div class="text-right" style="margin-top:26px;"><?php if (function_exists('po_breadcrumbs')) po_breadcrumbs(); ?></div>
                </div>
                
                </div>
            </div>
        </div><!-- .entry-header -->

        <div class="title" style="text-align: center;">
            <div class="titleTitle">
                <h3>NOS CONFÉRENCIERS</h3>
            </div>
            <div class="reseauxTitle">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
            </div>
            <div class="separatorTitle" style="text-align: center; background-color: #848484; height: 2px; width: 10%; margin-left: 45%;"></div>
        </div>

        <div class="container">
            <div class="row" style="padding-top:40px;">
                <div class="col-sm-6 col-sm-offset-3">
                    <h4 class="text-center"><?php echo get_theme_mod( 'team_subtitle'); ?></h4>
                </div>
            </div>
        </div>

        <div>
            <?php echo do_shortcode('[teamsic category="conferencier" number="8" columnwidth="3" order="DESC" orderby="date"]'); ?>
        </div>

        <div class="reseauBottom">
            <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-youtube"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-facebook" size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-linkedin"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" icon-viadeo"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-pinterest"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-twitter"  size="20"][/column] ' ); ?>
        </div>

</div>

<?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="http://sicotterecrutement.agenceapollo.webfactional.com//wp-content/uploads/2014/11/fond_contact.jpg"]

                            [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title="Appelez-nous"]Notre équipe est prête à vous répondre: [text align="center" ]au <a href="tel:+15143601304">514-360-1304</a> les Lundi-Vendredi, 9am-6pm [/text] [/iconbox][/column]

                            [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title="Ecrivez-nous"] Envoyez nous un courriel à:  [text align="center" size="15"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                            [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title="Venez nous voir"] Notre équipe vous accueil durant nos horaires de bureau: [text align="center" ]Lundi-Vendredi, 9am-6pm, au 129 rue de la Commune Est, H2Y 1J1, Montréal[/text] [/iconbox][/column]

                            [/section]'); ?>

</div>

            
<?php get_footer(); ?>

<style type="text/css">
    
    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    .title {
        margin-bottom: 3%;
    }

    body > div.po-nav.nav-fixed-padding > div > div.entry-header.po-portfolio-header {
        visibility: hidden;
    }

    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .reseauBottom {
        min-height: 210px;
        zoom: 200%;
    }


</style>
