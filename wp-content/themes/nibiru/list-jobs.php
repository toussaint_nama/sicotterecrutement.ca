<?php
/*
Template Name: ListJobs
Author: Aziz Zahar
*/

include 'CorrespLogo.php';

// Recuperation de la langue de la page
    $url = $_SERVER["REQUEST_URI"];
    $lang = substr($url, 1, 2);

// Recuperation de toutes donnees des offres dans des tableaux
    $jobIDArray[] = "";
    $nomJob[] = "";
    $villeJob[] = "";
    $dateJob[] = "";
    $typeJob[] = "";
    $statutJob[] = "";
    $catJob[] = "";
    $catJobEn[] = "";
    $checkColor[] = "";
    $linkJob[] = "";
    $postName[] = "";
    $args = array(      'orderby'          => 'post_date',
                        'order'            => 'DESC',
                        'post_type'        => 'rsjp_job_postings',
                        'post_status'      => 'publish',
                        'posts_per_page'    => '-1' );
    $jobsUnsorted = get_posts($args);
    $partOpened[] = "";
    $partClosed[] = "";
    foreach ($jobsUnsorted as $job) {
        $statu = get_post_meta($job->ID, 'jp_statut', true);
        if (($statu == "Combl&eacute") || ($statu == "Comblé") || ($statu == "Comble") || ($statu == "En Entrevue") || ($statu == "Non Disponible") || ($statu == "")) {
            array_push($partClosed, $job);
        } else {
            array_push($partOpened, $job);
        }
    }
    $jobs = array_merge($partOpened, $partClosed);
    $countPosts = count($jobs);
    $nbpages = intval(($countPosts/10)+1);
    $i = 1;
    foreach ($jobs as $job) {
        $jobID = $job->ID;
        $jobIDArray[$i] = $jobID;
        $nomJob[$i] = $job->post_title;
        $villeJob[$i] = get_post_meta($jobID, 'jp_lieu', true);
        $dateJob[$i] = substr($job->post_date,0,10);
        $typeJob[$i] = get_post_meta($jobID, 'jp_type', true);
        $statutJob[$i] = get_post_meta($jobID, 'jp_statut', true);
        $category = get_the_category($jobID);
        $nbCatJob[$i] = count($category);
        $marginContentJob[$i] = 0;
        if ( count($category) >=2 ) {
            $sizeCatJob[$i] = 100/$nbCatJob[$i];
            $catNameArray = array();
            $catNameEnArray = array();
            $correspLogoArray = array();
            for ($j=0; $j < $nbCatJob[$i]; $j++) { 
                if ( $lang == "en") {
                    $catNameArray[] = $correspTitleEn[$category[$j]->name];
                    $correspLogoArray[] = $correspLogo[$correspTitleEn[$category[$j]->name]];
                    $catNameEnArray[] = $category[$j]->name;
                } else {
                    $catNameArray[] = $category[$j]->name;
                    $correspLogoArray[] = $correspLogo[($category[$j]->name)];
                }
            }
            $catJobEn[$i] = $catNameEnArray;
            $catJob[$i] = $catNameArray;
            $logoJob[$i] = $correspLogoArray;
            $marginContentJob[$i] = ($nbCatJob[$i]-1)*5.5;
        } else {
            if ( $lang == "en") {
                $catJob[$i] = $correspTitleEn[$category[0]->name];
                $catJobEn[$i] = $category[0]->name;
            } else {
                $catJob[$i] = $category[0]->name;
            }
            $logoJob[$i] = $correspLogo[$catJob[$i]]; 
        }
        
        $linkJob[$i] = $job->guid;
        $postName[$i] = $job->post_name;
        if (($statutJob[$i] == "Combl&eacute") || ($statutJob[$i] == "Comblé") || ($statutJob[$i] == "Comble")) {
            $checkColor[$i] = "#f4545f";
        } elseif ($statutJob[$i] == "En Entrevue") {
            $checkColor[$i] = "#ffa500";
        } elseif ($statutJob[$i] == "Non Disponible") {
            $checkColor[$i] = "lightgrey";
        } else { $checkColor[$i] = "#1abf52"; }
        $i++;
    }

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");	?>
    
    <div class="po-page">

        <div class="row po-full-width">
        <div class="portfolio-page col-sm-12 column-12" style="background-image: none !important;">
            <div class="liquidcontainerpage">
                <div class="titleHead col-sm-10" style="margin-top: 4%;">
                    <div class="titleTitle">
                        <h1 style="color: #848484;"><?php echo __("Toutes nos offres d'emplois"); ?></h1>
                    </div>
                    <div class="reseauxTitle">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                        <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                        <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                    </div>
                    
                </div>
            </div>
        </div>
    	</div>
            

        <div class="container">
                

            <div class="row" style="margin-bottom: 10%;">

                    <div class="content col-sm-9" style="padding-bottom: 4%;">
                        
                        <!-- Page 0: 10 premiers resultats -->
                        <div class="page page0">
                        <?php for ($i=1; $i < 100; $i++) { 
                        	if ( $lang == "en" ) {
                        		$l = "http://sicotterecrutement.ca/en/job-postings/".$postName[$i]."/";
                        	} else {
                        		$l = $linkJob[$i];
                        	}
                            if (!is_null($jobIDArray[$i])) { 
                            	if ($statutJob[$i] === "En Recrutement") { ?>

	                            <a href="<?php echo $l; ?>">
		                            <div class="jobOffre jobOffre-<?php echo $i; ?> col-md-12" id="<?php echo $jobIDArray[$i]; ?>">
		                                <div class="jobLogo col-md-2 row" style="margin: 0px;">
		                                    <?php if ($nbCatJob[$i] >= 2) { 
		                                        for ($j=0; $j < $nbCatJob[$i]; $j++) { ?>
		                                            <div class="col-md-<?php echo $sizeCatJob[$i]; ?>">
		                                                <span class="icon-<?php echo $logoJob[$i][$j]; ?> fa-3x iconCat" style="color: #f4545f;"></span>
		                                                <?php if ( $lang == "en" ) { ?>
		                                                    <h5 style="color: #848484; text-align: center; margin-left: -40%;"><?php echo $catJobEn[$i][$j]; ?></h5>
		                                                <?php } else { ?>
		                                                    <h5 style="color: #848484; text-align: center; margin-left: -40%;"><?php echo $catJob[$i][$j]; ?></h5>
		                                                <?php } ?>
		                                            </div>    
		                                        <?php }
		                                    } else { ?>
		                                        <span class="icon-<?php echo $logoJob[$i]; ?> fa-3x iconCat" style="color: #f4545f;"></span>
		                                        <?php if ( $lang == "en" ) { ?>
		                                            <h5 style="color: #848484; text-align: center; margin-left: -40%;"><?php echo $catJobEn[$i]; ?></h5>
		                                        <?php } else { ?>
		                                            <h5 style="color: #848484; text-align: center; margin-left: -40%;"><?php echo $catJob[$i]; ?></h5>
		                                        <?php } ?>
		                                    <?php } ?>
		                                </div>
		                                <div class="jobContent col-md-10" style="margin-top: <?php echo $marginContentJob[$i]; ?>%;">
		                                    <h3 style="width:100%; color: #848484; margin-bottom: 20px;"><b style="color: #333333"><?php echo $nomJob[$i]; ?></b></h3>
		                                    <div class="jobContentLieu col-md-4">
		                                        <span class="icon-location iconDet" style="color: #848484; float: left;"></span>
		                                        <h5 style="color: #848484; text-align: center;"><?php echo $villeJob[$i]; ?></h5>
		                                    </div>
		                                    <!-- <div class="jobContentDate col-md-3">
		                                        <span class="icon-calendrier iconDet" style="color: #848484; float: left;"></span>
		                                        <h5 style="color: #848484; text-align: center;"><?php echo $dateJob[$i]; ?></h5>
		                                    </div>  -->
		                                    <div class="jobContentType col-md-4">
		                                        <span class="icon-type_emploi iconDet" style="color: #848484; float: left;"></span>
		                                        <?php if ( $lang == "en" ) { 
		                                            $s = $typeJob[$i];?>
		                                            <h5 style="color: #848484; text-align: center;"><?php echo $correspType[$s]; ?></h5>
		                                        <?php } else { ?>
		                                            <h5 style="color: #848484; text-align: center;"><?php echo $typeJob[$i]; ?></h5>
		                                        <?php } ?>
		                                    </div>
		                                    <div class="jobContentStatut col-md-4">
		                                        <span class="icon-valide iconDet" style="color: <?php echo $checkColor[$i]; ?>; float: left;"></span>
		                                        <?php if ( $lang == "en" ) { 
		                                            $s = $statutJob[$i];?>
		                                            <h5 style="color: <?php echo $checkColor[$i]; ?>; text-align: center;"><?php echo $correspStatut[$s]; ?></h5>
		                                        <?php } else { ?>
		                                            <h5 style="color: <?php echo $checkColor[$i]; ?>; text-align: center;"><?php echo $statutJob[$i]; ?></h5>
		                                        <?php } ?>
		                                    </div>
		                                </div>
		                                <div class="jobSeparator col-md-10"></div>
		                            </div>
	                            </a>
                        <?php } } } ?>

                        <!-- Pagination Custom -->
                        <?php // if ( $nbpages > 1 ) { ?>
                        <!-- <div class="col-md-9">
                            <div class="po-page-portfolio-paginate">
                                <div class="col-md-12">
                                    <div class="col-md-2 bout precedent" style="width: 8%; font-weight: bold;"><a href="#">&#x2329;&#x2329;</a></div>
                                    <?php for ($i=1; $i < ($nbpages+1); $i++) { ?>
                                        <div class="col-md-1 bout bout<?php echo $i; ?>" style="width: 7%;"><a class="numPage<?php echo $i; ?>" href="#" style="color: #848484;"><?php echo $i; ?></a></div>
                                    <?php } ?>
                                    <div class="col-md-2 bout suivant" style="width: 8%; font-weight: bold;"><a href="#">&#x232A;&#x232A;</a></div>
                                </div>
                            </div>
                        </div> -->
                        <?php // } ?>

                        </div>

                        

                    </div>

                    

                <div class="col-sm-3">

                    <?php get_sidebar(); ?>

                    <div class="textSide col-md-12">
                        <div style="margin-top: 15px; margin-bottom: 15px;">
                            <h4 style="font-weight: bold;"><?php echo __("Pas d'offres pour vous?"); ?></h4>
                            <h6><?php echo __("Aucune offre ne correspond à votre profil, faites nous"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("parvenir votre CV"); ?></a><?php echo __(' et remplissez '); ?><a href="http://sicotterecrutement.ca/fiche-candidat/" style="color: blue;"><?php echo __('la fiche de renseignement candidats'); ?></a></h6>
                        </div>
                        <div>
                            <h4 style="font-weight: bold;"><?php echo __("Infos Sicotte Recrutement"); ?></h4>
                            <h6><?php echo __("Vous avez besoin de plus d'informations?"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("Ecrivez-nous un courriel"); ?></a></h6>
                        </div>
                    </div>

                    <a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>"><div class="boutonToutSide col-md-12" style="width: 70%;"><h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Voir toutes les catégories"); ?></h5></div></a>

                </div>	

            </div>

        </div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>

<!-- Scripts de pagination -->			
<script type="text/javascript">

jQuery(".bout1").css("background-color", "#f4545f" );
jQuery(".numPage1").css("color", "white !important" );

<?php for ($i=1; $i < $nbpages; $i++) { ?>
    jQuery(".page<?php echo $i; ?>").hide();
<?php } ?>

jQuery(".suivant").click( function() {
    <?php for ($i=0; $i < ($nbpages-1); $i++) { ?>
        if ( jQuery(".page<?php echo $i; ?>").css("display") == "block" ) {
            jQuery(".page<?php echo $i; ?>").css("display", "none");
            jQuery(".page<?php echo ($i+1); ?>").css("display", "block");
            jQuery(".bout<?php echo ($i+2); ?>").css("background-color", "#f4545f" );
            jQuery(".numPage<?php echo ($i+2); ?>").css("color", "white !important" );
            jQuery(".bout<?php echo ($i+1); ?>").css("background-color", "rgb(245,245,245)" );
            jQuery(".numPage<?php echo ($i+1); ?>").css("color", "#848484" );
        };
    <?php } ?>
});

jQuery(".precedent").click( function() {
    <?php for ($i=1; $i < $nbpages; $i++) { ?>
        if ( jQuery(".page<?php echo $i; ?>").css("display") == "block" ) {
            jQuery(".page<?php echo $i; ?>").css("display", "none");
            jQuery(".page<?php echo ($i-1); ?>").css("display", "block");
            jQuery(".bout<?php echo $i; ?>").css("background-color", "#f4545f" );
            jQuery(".numPage<?php echo $i; ?>").css("color", "white !important" );
            jQuery(".bout<?php echo ($i+1); ?>").css("background-color", "rgb(245,245,245)" );
            jQuery(".numPage<?php echo ($i+1); ?>").css("color", "#848484" );
        };
    <?php } ?>
});

<?php for ($i=0; $i < $nbpages; $i++) { ?>
    jQuery(".bout<?php echo ($i+1); ?>").click( function() {
        <?php for ($j=0; $j < $nbpages; $j++) { 
            if ( $j == $i ) { ?>
                jQuery(".page<?php echo $j ?>").show();
                jQuery(".bout<?php echo ($j+1); ?>").css("background-color", "#f4545f" );
                jQuery(".numPage<?php echo ($j+1); ?>").css("color", "white !important" );
            <?php } else { ?>
                jQuery(".page<?php echo $j ?>").hide();
                jQuery(".bout<?php echo ($j+1); ?>").css("background-color", "rgb(245,245,245)" );
                jQuery(".numPage<?php echo ($j+1); ?>").css("color", "#848484" );
            <?php }; 
        } ?>
    });
<?php } ?>

</script>

<style type="text/css">
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    #search-2 > form > input { background-color: white; }

    body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width > div > div { height: 240px; }

    .content {
        background-color: rgb(245,245,245);
        height: 100%;
    }

    .boutonToutSide {
        border: 1px solid;
        border-color: #f4545f;
        background-color: rgb(245,245,245);
        margin-left: 15%;
        margin-top: 40px;
    }

    .iconDet {
        zoom: 150%;
        margin-top: 3px;
    }

    .iconCat { margin-left: 15px; }
    .jobLogo { margin-top: 20px; }
    .jobSeparator { 
        width: 80%;
        margin-left: 10%;
        height: 1px;
        background-color: rgba(132, 132, 132, 0.46);
        margin-top: 5%;
    }
    .jobOffre {
        margin-top: 4%;
    }

    .jobContent {

    }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    .bout {
        background-color: rgb(245,245,245);
        border: 1px solid;
        border-color: #848484;
        text-align: center;
        margin-right: 0.5%;
        border-radius: 3px;
    }

    .precedent, .suivant {
        background-color: #848484;
        color: #ec0f1e;
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    .icon-ventes.fa-3x, .icon-opportunites_affaire.fa-3x { margin-left: 7px; }
    .icon-sante.fa-2x, .icon-evenementiel.fa-2x { margin-left: 19px; }
    .icon-bancaires.fa-3x { margin-left: 10px; }
    .icon-finance.fa-2x, .icon-production_manutention.fa-2x, .icon-vitivinicole.fa-2x, .icon-elite.fa-3x { margin-left: 20px; }
    .icon-assurance.fa-2x, .icon-marketing.fa-2x { margin-left: 18px; }
    .icon-assurance.fa-3x, .icon-services.fa-3x { margin-left: 11px; }
    .icon-consultants.fa-2x, .icon-savoir_faire_technologique.fa-2x, .icon-services.fa-2x, .icon-architecture.fa-3x { margin-left: 17px; }
    .icon-juridique.fa-3x, .icon-transport.fa-2x, .icon-marketing.fa-3x, .icon-finance.fa-3x, .icon-evenementiel.fa-3x  { margin-left: 13px; }
    .icon-mode.fa-3x { margin-left: 22px; }
    .icon-etudiants_diplomes.fa-2x { margin-left: 14px; }
    .icon-franchises.fa-3x { margin-left: 2px; }
    .icon-cinema.fa-3x, .icon-savoir_faire_technologique.fa-3x { margin-left: 12px; }
    .icon-mode.fa-2x { margin-left: 25px; }
    .icon-transport.fa-3x { margin-left: 5px; }
    .icon-interim.fa-3x { margin-left: 9px; }
    .icon-etudiants_diplomes.fa-3x { margin-left: 8px; }

    @media (max-width: 766px) {
        .jobLogo > div {
            width: 50%;
            float: left;
        }
        .jobLogo > div > h5 {
            font-size: 10px;
        }
        .jobOffre {
            padding-top: 10px;
        }
        .jobSeparator {
            margin-top: 20%;
        }
        body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
            font-size: 18px;
        }
        body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width {
            height: 140px;
        }
        .content.col-sm-9 {
            padding-bottom: 14% !important;
            margin-bottom: 15px;
        }
        .bout {
            float: left;
        }
    }

    #nblikefacebook {
        zoom: 100% !important;
    }

</style>

<?php get_footer(); ?>