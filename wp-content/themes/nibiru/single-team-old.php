<?php

// Fonction  de recuperation du titre de l'article à partir du lien
function getTitle($Url){
    if ( $Url != "") {
        $str = file_get_contents($Url);
        if(strlen($str)>0){
            preg_match("/\<title\>(.*)\<\/title\>/",$str,$title);
            return $title[1];
        }
    }
}

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

session_start();

echo do_shortcode("[nav_bar_noslide slider='none']");
	?>




<!-- Style place au debut pour accelerer le chargement de la page et son affichage -->
<style type="text/css">
    
.row {
    margin-left: 5%;
    margin-right: 5%;
}

.po-page {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg');
}

.textContent {
    -webkit-column-count: 3; /* Chrome, Safari, Opera */
    -moz-column-count: 3; /* Firefox */
    column-count: 3;
    -webkit-column-gap: 40px; /* Chrome, Safari, Opera */
    -moz-column-gap: 40px; /* Firefox */
    column-gap: 40px;
    text-align: justify;
    min-height: 270px;
    margin-top: 1%;
    overflow: auto;
}

.articleContent {
    text-align: justify;
    min-height: 270px;
    margin-top: 1%;
}

.textTitle {
    margin-top: 2%;
    margin-left: 1%;
}

.boutonConf {
    border: 1px solid;
    border-color: #848484;
    color: #f4545f;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    text-align: center;
    zoom: 150%;
    width: 58%;
    margin-left: 21%;
}

.boutonConf:hover {
    color: white;
    background-color: #f4545f;
    border-color: #848484;
}

.textBottom {
    text-align: center;
    margin-top: 22%;
    margin-bottom: 2%;
}

.rowUp, .rowDownm, .rowMid {
    margin-top: 2%;
}

.rowDown {
    margin-bottom: 5%;
}

.rowMid {
    text-align: -webkit-center;
    padding-bottom: 10px;
}

body > div.po-nav.nav-fixed-padding > div.po-page > div.row > div.col-md-12.rowUp > div.photo.col-md-4 > a > img {
    width: 95%;
    height: 95%;
}

.icon-box { zoom: 200%; }

.domaineText {
    font-size: 9px;
    font-weight: bold;
    color: #4d4d4d;
}

.articleText {
    color: #3f94e0;
    font-size: 20px;
    line-height: 40px;
}

body > div.container.po-container-section > div {
    position: fixed;
}

.footer-container, .footer-container-bottom {
    position: fixed;
}

body > div.po-nav.nav-fixed-padding > div > div.row > div.col-md-12.rowUp > div.sidebar.col-md-4 > a > div {
    width: 80%;
    margin-left: 10%;
}

#primary-sidebar {
    max-width: 80%;
    margin-left: 10%;
}

body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p, 
body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
    color: #F4545F;
}

.iResTitle {
    font-size: 26px;
    color: #f4545f;
    margin-left: 20px;
    margin-right: 20px;
    margin-bottom: 1%;
    margin-top: 1%;
}

.reseauxTitle {
    margin-bottom: 1%;
}

/*body > div.entry-content {
    display: none !important;
}*/

body > div.po-nav.nav-fixed-padding > div > div.title > h1 {
    margin-top: 0px !important;
}

#myCarousel > ol > li.active {
    background-color: #f4545f;
}
#myCarousel > ol > li {
    background-color: darkgray;
}

.photo {
    background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>");
    background-size: cover;
    background-position-x: 50%;
    background-position-y: 50%;
    min-height: 360px;
}

.boutonConf > a:hover {
	color: white;
}

.boutonRondPanier {
	height: 90px;
	width: 90px;
	text-align: center;
	border: 1px solid;
	border-radius: 50%;
	background-color: #f0f0f0;
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red-round.png');
	background-size: contain;
	border-color: #f4545f;
	display: inline-block;
}

.boutonRondPanier:hover {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-white-round.png');
	background-color: #f4545f;
}

.boutonRondEnlever {
	height: 90px;
	width: 90px;
	text-align: center;
	border: 1px solid;
	border-radius: 50%;
	background-color: #f0f0f0;
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-remove-red-round.png');
	background-size: contain;
	border-color: #f4545f;
	display: inline-block;
}

.boutonRondEnlever:hover {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-remove-white-round.png');
	background-color: #f4545f;
}

#nblikefacebook {
    zoom: 100% !important;
}

</style>



    
    <div class="po-page">
        
        <div class="title" style="text-align: center; min-height: 215px;">
            <h1 style="padding-top: 4%; color: #848484;"><?php the_title(); ?></h1>
            <div class="reseauxTitle">
                <a href='http://www.facebook.com/sharer.php?s=100&p[title]=<?php the_title(); ?>&p[summary]=<?php the_content() ?>&p[url]=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>&p[images][0]=<?php the_post_thumbnail("full"); ?>' ><span class="fa fa-facebook iResTitle"></span></a>
                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>&text=<?php the_excerpt(); ?>"><span class="fa fa-twitter iResTitle"></span></a>
                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
            </div>
        </div>

        <div class="row">

    		<div class="col-md-12 rowUp">
    			
                <div class="photo col-md-4">
	                <?php
	    			$video = get_post_meta( get_the_ID(), 'jp_videoTeam', true );
	    			if ( $video != "") { ?>
                        <a class="bouton-play"><img src="<?php echo get_template_directory_uri(); ?>/images/play-bouton.png" style="height: 50px; width: 50px; float: right; margin-top: 10px;"></a>
	    				<iframe width="500" height="360" src="https://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen style="width: 100%; height: 360px; display: none;"></iframe>
	    			<?php } else { ?>
						<!--<a href="<?php echo get_post_type_archive_link( 'team' ); ?>"><?php the_post_thumbnail('full'); ?></a>-->
	    			<?php  } ?> 
                    <!--
                    <a>
                        <div class="boutonConf">
                            <span style=""><?php echo __("Demander plus d'informations"); ?></span>
                        </div>
                    </a> 
                    -->  
                </div>
                <div class="domaines col-md-4">
                    <!--
                    <h3 style="margin-bottom: 6%;"><?php echo __("Domaines d'expertise"); ?></h3>
                    <?php
                    $domaine = "";
                    for ($i=1; $i < 5; $i++) { 
                        $domaine = get_post_meta( get_the_ID(), 'jp_domaine'.$i, true );
                        if ($domaine != "") { ?>
                            <p class="domaineText"><?php echo $domaine; ?></p>
                            <div style="height: 1px; background-color: #f4545f; width: 100%;"></div>
                            <br/><?php 
                        }    
                    } 
                    ?>
                    -->
                    <?php 
                    $domTitre = get_post_meta( get_the_ID(), 'jp_domaineTitre', true );
                    $domDet = get_post_meta( get_the_ID(), 'jp_domaineDetail', true );
                    $domCount = count($domTitre);
                    for ($i=0; $i < $domCount; $i++) { ?>
                        <span class="fa fa-angle-down dropD-<?php echo $i; ?>" style="float: left; margin-left: 10px; margin-right: 10px; font-size: 21px; margin-top: 12px; color: #f4545f;"></span><h4><?php echo $domTitre[$i]; ?></h4>
                        <div class="det-<?php echo $i; ?>" style="background-color: white; margin: 5px; display: none; padding-left: 5px;">
                            <p><?php echo $domDet[$i]; ?></p>
                        </div>
                        <div style="height: 1px; background-color: #f4545f; width: 100%;"></div>
                    <?php }
                    ?>
                </div>
                <div class="sidebar col-md-4">
                    <!--
                    <div id="secondary">
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<li id="search-2" class="widget widget_search">
								<form role="search" method="get" class="search-sidebar" action="http://sicotterecrutement.agenceapollo.webfactional.com/">
								<i class="fa fa-search search-icon-form-sidebar"></i>
								<input type="search" class="search-field-sidebar" value="" name="s" style="background-color: white;">	
							</form></li>
						</div>
					</div>
                    -->
                    <div class="temoignages">
                        <div style="background-color: #f0f0f0; width: 50%;float: left; height: 35px;"><h3 style="margin-top: 5px; margin-left: 5px;">TÉMOIGNAGES</h3></div>
                        <a><div style="background-color: #f0f0f0; width: 50%; float: right; height: 35px;"><h5 style="float: right; margin-right: 5px; font-size: 12px;">TÉMOIGNAGE SUIVANT</h5></div></a>
                        <div style="background-color: rgba(228, 228, 228, 0.56); margin-bottom: 15px; padding-top: 50px; padding-left: 5px; padding-right: 5px; text-align: justify; font-family: 'architects_daughterregular';">
                            <?php $temoignages = get_post_meta(get_the_ID(), 'jp_temoignage', true); 
                            $temCount = count($temoignages); ?>
                            <div class="temoignage-0">
                                <?php echo $temoignages[0]; ?>
                            </div>
                            <?php for ($i=1; $i < $temCount; $i++) { ?>
                                <div class="temoignage-<?php echo $i; ?> hidden">
                                    <?php echo $temoignages[$i]; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
	                
	                    <div class="boutonConf" style="width: 55%; margin-left: auto; margin-right: auto;">
	                    	<?php 
	                    	$confId = get_the_ID();
	                    	$classe = "boutonRondPanier";
	                    	$titreBouton = "Ajouter à mes conférenciers";
	                    	if ( isset($_SESSION["produits"]) ) {
	                    		$conferenciers = $_SESSION["produits"];
	                    		$found = false;
	                    		$url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	                    		foreach ( $conferenciers as $conferencier ) {
	                    			if ( $conferencier == $confId ) { 
	                    				$found =true; ?>
	                    			<?php }
	                    		}
	                    		if ( $found == true ) { 
	                    			$classe = "boutonRondEnlever";
	                    			$titreBouton = "Retirer de mes conférenciers";?>
	                    			<a href="<?php echo get_template_directory_uri(); ?>/panier-update.php?remove=<?php echo $confId; ?>&returnUrl=<?php echo $url; ?>">
	                    			<span style="display: block;"><?php echo __("Retirer de ma liste"); ?></span>
	                    			</a>
	                    		<?php } else { ?>
	                    			<a href="#"  onclick="document.forms['formAdd'].submit();">
	                    			<span style="margin-top: 5px; display: block;"><?php echo __("Ajouter à ma liste"); ?></span>
                            		<img class="img-responsive" style="height: 25px; margin-top: -25px; margin-bottom: 5px; margin-left: 5px;" src="<?php echo get_template_directory_uri(); ?>/images/cart-brain-red.png">
                            		</a>
	                    		<?php } 
	                    	} else { ?>
	                    		<a href="#"  onclick="document.forms['formAdd'].submit();">
	                    		<span style="margin-top: 5px; display: block;"><?php echo __("Ajouter à ma liste"); ?></span>
                            	<img class="img-responsive" style="height: 25px; margin-top: -25px; margin-bottom: 5px; margin-left: 5px;" src="<?php echo get_template_directory_uri(); ?>/images/cart-brain-red.png">
                            	</a>
	                    	<?php }	
	                    	?>
	
	                    </div>

	                    
	                    <div class="<?php echo $classe; ?>" title="<?php echo $titreBouton; ?>"></div>

	                    
	
                    <a>
                        <div class="boutonConf" style="width: 55%; margin-left: auto; margin-right: auto; margin-top: 15px;">
                            <span style=""><?php echo __("Demander plus d'informations"); ?></span>
                        </div>
                    </a>
                    <?php 
                    $confId = get_the_ID();
                    $url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                    ?>
                    <form name="formAdd" type="post" action="<?php echo get_template_directory_uri(); ?>/panier-update.php">  
                    	<input type="hidden" name="type" value="add">
                    	<input type="hidden" name="id" value="<?php echo $confId; ?>">
                    	<input type="hidden" name="returnUrl" value="<?php echo $url; ?>">
                    </form>
                </div>
            </div>

            <?php 
            $temoignages = get_post_meta(get_the_ID(), 'jp_temoignage', true);
            $temCount = count($temoignages);
            if ($temCount >= 1) { ?>
            <div class="col-md-12 rowMid rowTenoignage" style="background-color: #f0f0f0; margin-bottom: 2%;">
                
                <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 80% !important;">
                  <!-- Indicators -->
                  <ol class="carousel-indicators" style="bottom: -22px;">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <?php if ($temCount >= 2) {
                        for ($i=1; $i < $temCount; $i++) { ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
                        <?php }
                    } ?>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <h4 style="font-family: 'special_eliteregular';"><?php echo $temoignages[0]; ?></h4>
                    </div>

                    <?php if ($temCount >= 2) {
                        for ($i=1; $i < $temCount; $i++) { ?>
                            <div class="item">
                              <h4 style="font-family: 'special_eliteregular';"><?php echo $temoignages[$i]; ?></h4>
                            </div>
                        <?php }
                    } ?>
                    
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="color: #f4545f; margin-left: -15%; padding-top: 30px;">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="color: #f4545f; margin-right: -15%; padding-top: 30px;">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>    
            <?php }
            ?>
            

            <div class="col-md-12 rowDown" style="background-color: #f0f0f0;">
            	<div class="textTitle">
                    <h3 style="color: #f4545f;"><?php the_title(); ?></h3>
                </div>
                <div class="textContent col-md-12">
                    <?php the_content(); ?><br/>
                </div>
                <div class="textTitle">
            		<h3 style="color: #f4545f;"><?php echo __("Conférences réalisées"); ?></h3>
            		<h3 style="zoom: 80%;"><?php
            		$confs = get_post_meta( get_the_ID(), 'jp_conf', true );
            		echo $confs;
            		?></h3>
            	</div>
                <br/>
                <div class="textTitle">
                	<h3 style="color: #f4545f;"><?php echo __("Médias"); ?></h3>
                </div>
                <div class="articleContent col-md-12" style="margin-top: 1%;">
                	<?php
                	/*
                    $article = "";
                    for ($i=1; $i < 16; $i++) { 
                        $article = get_post_meta( get_the_ID(), 'jp_article'.$i, true );
                        $titreArticle = get_post_meta ( get_the_ID(), 'jp_article'.$i.'-titre', true);
                        $titre = "";
                        if ( $titreArticle == "" ) {
                            $titre = getTitle($article);
                        } else {
                            $titre = $titreArticle;
                        }
                        if ($article != "") { ?>
                            <a class="articleText" href="<?php echo $article; ?>"><?php echo $titre; ?></a>
                            <br/><?php 
                        }                        
                    }
                    */
                    $articleA = get_post_meta( get_the_ID(), 'jp_article16', true );
                    $titreArticleA = get_post_meta ( get_the_ID(), 'jp_article16-titre', true );
                    $imageArticle = get_post_meta ( get_the_ID(), 'jp_article16-image', true );
                    for ($i=0; $i < count($articleA); $i++) { 
                        $lien = $articleA[$i];
                        $titre = $titreArticleA[$i];
                        $image = $imageArticle[$i];
                        $heightDiv = ""; 
                        if ($image != "/") {
                        	$heightDiv = "height: 45px;";
                        } ?>
                        <div style="width: 100%; <?php echo $heightDiv; ?>">
                        <?php if ( $image != "/" ) { ?>
	                        <div style="height: 0px; float left;">
	                        	<img class="img-responsive" src="<?php echo $image; ?>" style="float: left; max-height: 45px;">
	                        </div>
                        <?php } ?>
                        <a class="articleText" href="<?php echo $lien; ?>" style="margin-top: 45px; margin-left: 10px;"><?php echo $titre ?></a>
                        </div>
                        <br>
                        <?php 
                    }
                    ?>
                </div>
                <div class="col-md-12" style="margin-top: 2%; margin-bottom: 2%;">
                	<a href="mailto:info@sicotterecrutement.ca">
                        <div class="boutonConf" style="width: 30%; margin-left: 35%;">
                            <?php echo __("Intéressé par ce conférencier?"); ?>
                        </div>
                    </a>
                </div>
                <div class="textBottom">
                    <h4><?php echo __("Pour de plus amples informations merci de communiquer avec Mlle Nolwenn Guillon"); ?>,<br/> au <a href="tel:+15143601304">514-360-1304</a> <?php echo __("et/ou"); ?> <a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a></h4>
                </div>
            </div>

        </div>

        <div style="height: 2px; background-color: #848484; width: 12%; margin-left: 44%; margin-top: 2%;"></div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>

    </div>
<script type="text/javascript">
	
<?php 
$ccc = count(get_post_meta( get_the_ID(), 'jp_domaineTitre', true ));
for ($i=0; $i < $ccc; $i++) { ?>
	
	jQuery('.dropD-<?php echo $i; ?>').click(function() {
		jQuery('.det-<?php echo $i; ?>').toggle();
		if (jQuery('.dropD-<?php echo $i; ?>').hasClass("fa-angle-down")) {jQuery('.dropD-<?php echo $i; ?>').removeClass( "fa-angle-down" ).addClass("fa-angle-up");} else {jQuery('.dropD-<?php echo $i; ?>').removeClass( "fa-angle-up" ).addClass("fa-angle-down");};
	});

<?php } ?>

jQuery('.bouton-play').click(function() {
    jQuery('iframe').toggle();
    jQuery('.bouton-play').hide();
    jQuery('.photo').css("background-image", "none");
});

</script>		
<?php get_footer(); ?>

