<?php
/*
Template Name: Babillard Page
*/


get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");	?>
    
    <div class="po-page">

        <div class="container col-md-12">
                
            <?php echo do_shortcode('[boutonsbabillard]

                                     [babillard]'); ?>

        </div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.linkedin.com/pub/sicotte-recrutement" target="_blank"> [iconboxsic</a><a href="https://www.linkedin.com/pub/sicotte-recrutement" target="_blank"> icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="http://sicotterecrutement.agenceapollo.webfactional.com//wp-content/uploads/2014/11/fond_contact.jpg"]

                                [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title="Appelez-nous"]Notre équipe est prête à vous répondre: [text align="center" ]au <a href="tel:+15143601304">514-360-1304</a> les Lundi-Vendredi, 9am-6pm [/text] [/iconbox][/column]

                                [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title="Ecrivez-nous"] Envoyez-nous un courriel à:  [text align="center" size="15"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title="Venez nous rencontrer"] Notre équipe vous accueil durant nos horaires de bureau: [text align="center" ]Lundi-Vendredi, 9am-6pm, au 129 rue de la Commune Est, H2Y 1J1, Montréal[/text] [/iconbox][/column]

                                 [/section]'); ?>
			
<style type="text/css">
    
    .container {
        margin-top: 2%;
    }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    .icon-box { zoom: 200%; }

    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

</style>

<?php get_footer(); ?>

