<?php
/*
Template Name: Club de lecture Page
*/

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");   ?>
    
    <div class="po-page">

        <div class="title" style="text-align: center; min-height: 215px;">

            <div style="padding-top: 4%;">
                <span class="icon-photos logTitle" style="font-size: 45px;"></span>
                <span class="icon-livre logTitle" style="font-size: 45px;"></span>
            </div>
            <h1 style="color: #848484;"><?php echo __("Club de lecture"); ?></h1>
            <div class="reseauxTitle" style="padding-bottom: 3%;">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
            </div>
        </div>

            <div class="row">

                <?php global $page_mb; ?>

                <div class="page col-md-9" style="margin-bottom: 5%; margin-left: 10%; margin-right: 10%;">
                 
                    <div class="content col-md-9" style="padding: 60px;">

                            <div class="livre" style="min-height: 355px;">
                                <div class="couverture couverture1" style="float: left;"></div>
                                <div class="textLivre" style="float: right;">
                                    <h2 class="titleBook">Power - <span class="author">Robert Green</span></h2>
                                    <p class="resume">Le sentiment de n'avoir aucun pouvoir sur les gens et les événements est difficilement supportable: l'impuissance rend malheureux. Personne ne réclame moins de pouvoir, tout le monde ne veut d'avantage.</p>
                                </div>
                            </div>

                            <div class="separator"></div>

                            <div class="livre" style="min-height: 438px;">
                                <div class="textLivre" style="float: left;">
                                    <h2 class="titleBook">The Power of Focus - <span class="author">Jack Canfield, Mark Hansen, Les Hewitt</span></h2>
                                    <p class="resume">La raison numéro 1 qui empêche les gens d'obtenir ce qu'ils veulent, c'est le manque de focus. Les gens qui font le point et se concentrent sur ce qu'ils veulent, vont de l'avant, contrairement à ceux qui ne le font pas. Dans Thhe Power Of Focus vous découvrirez les stratégies de focus spécifiques utilisées par les hommes et les femmes les plus performants du monde. Découvrez comment.</p>
                                </div>
                                <div class="couverture couverture2" style="float: right;"></div>
                            </div>

                            <div class="separator"></div>

                            <div class="livre" style="min-height: 442px;">
                                <div class="couverture couverture3" style="float: left;"></div>
                                <div class="textLivre" style="float: right;">
                                    <h2 class="titleBook">Steve Jobs - <span class="author">Walter Isaacson</span></h2>
                                    <p class="resume">Suggéré par les créateur d'Apple et à partir de plus de 40 entretiens menés sur plus de deux ans d'interviews d'une centaine de membres de sa famille, amis, rivaux, concurrents et collègues, le livre retrace l'incroyable vie et l'extraordinnaire personnalité d'un génie, perfectionniste et hyperactif, qui a révolutionné les ordinateurs, les films d'animation, la musique , les téléphones, les tablettes tactiles et l'edition numérique. Steve Jobs est desormais l'icône absolue de l'inventivité.</p>
                                </div>
                            </div>

                            <div class="separator"></div>

                            <div class="livre" style="min-height: 404px;">
                                <div class="textLivre" style="float: left;">
                                    <h2 class="titleBook">Tribes - <span class="author">Seth Godin</span></h2>
                                    <p class="resume">Qui va nous mener? Le web peut faire des choses étonnantes, mais il ne peut pas assurer le leadership. Cela doit encore venir de gens comme vous qui ont la passion de quelque chose. L'explosion dans les tribus signifie que toute personne qui veut faire la difference a maintenant les outils au bout des doigts.</p>
                                </div>
                                <div class="couverture couverture4" style="float: right;"></div>
                            </div>

                            <div class="separator"></div>

                            <div class="livre" style="min-height: 400px;">
                                <div class="couverture couverture5" style="float: left;"></div>
                                <div class="textLivre" style="float: right;">
                                    <h2 class="titleBook">Entrepreneurial StrengthsFinder - <span class="author">Jim Clinfton</span></h2>
                                    <p class="resume">Entrepreunarial StrengthsFinder tente de comprendre ce qui pousse les entrepreneurs à se lancer, maintenir et faire croître une entreprise. Quelles sont les types de personnalité et les comportements qui mènent une personne à prendre le risque  de créer, et de réussir? Peut-on apprendre à être un entrepreneur, ou est-ce une qualité innée?</p>
                                </div>
                            </div>

                            <div class="separator"></div>

                            <div class="livre" style="min-height: 440px;">
                                <div class="textLivre" style="float: left;">
                                    <h2 class="titleBook">Ctrl Alt Delete - <span class="author">Mitch Joel</span></h2>
                                    <p class="resume">Pas encore de résumé</p>
                                </div>
                                <div class="couverture couverture6" style="float: right;"></div>
                            </div>

                            <div class="separator"></div>

                            <?php echo the_content(); ?>

                            <h4 style="font-weight: bolder; text-align: center; margin-top: 6%;"><?php echo __("Partager sur les réseaux sociaux"); ?></h4>

                            <div class="reseauxTitle" style="text-align: center;">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                            </div>
                        
                    </div>

                    <!-- Sidebar -->
                    <div class="col-md-3">

                        <?php get_sidebar(); ?>

                        <a href="<?php echo ("".get_site_url()."/offres-demploi/"); ?>">
                        <div class="boutonToutSide col-md-12" style="width: 100%; margin-top: -40px;">
                            <h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Voir toutes nos offres"); ?></h5>
                        </div>
                        </a>

                        <a href="<?php echo ("".get_site_url()."/?post_type=portfolio"); ?>">
                        <div class="boutonToutSide col-md-12" style="width: 100%; margin-top: 15px;">
                            <h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Voir toutes nos Catégories"); ?></h5>
                        </div>
                        </a>

                    </div>  

                </div>


            </div>

            <!-- Bouton reseaux sociaux -->
        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-youtube"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-facebook" size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-linkedin"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" icon-viadeo"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-pinterest"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-twitter"  size="20"][/column] ' ); ?>

        <div class="clearfix"></div>
        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title="'.__("Appelez-nous").'"]'.__("Notre équipe est prête à vous répondre").': [text align="center" ]<br/>'.__("au").' <a href="tel:+15143601304">514-360-1304</a> '.__("les Lundi-Vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title="'.__("Ecrivez-nous").'"] '.__("Envoyez nous un courriel à").':  [text align="center" size="15"]<br/><a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title="'.__("Venez nous voir").'"] '.__("Notre équipe vous accueille durant nos horaires de bureau").': [text align="center" ]<br/>'.__("Lundi-Vendredi, 9am-6pm, au 129 rue de la Commune Est, H2Y 1J1, Montréal").' [/text] [/iconbox][/column]

                                    [/section]'); ?>  

        </div>
        
            
<style type="text/css">
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .logTitle {
        color: #f4545f;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    #search-2 > form > input { background-color: white; }

    .content {
        background-color: #f0f0f0;
    }

    .icon-box { zoom: 200%; }

    .boutonToutSide {
        border: 1px solid;
        border-color: #f4545f;
        background-color: rgb(245,245,245);
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    body > div.container.po-container-section {
        visibility: hidden;
    }

    hr { border-color: #848484; }

    .separator {
        border-top: 1px solid;
        border-color: #D1CCCC;
        margin-top: 3%;
        margin-bottom: 3%;
    }

    body > div.container.po-container-section > div {
        position: fixed;
    }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    .couverture {
        width: 30%;
    }

    .textLivre {
        width: 66%;
    }

    .titleBook {
        color: black;
    }

    .author {
        color: #848484 !important;
    }
    
    .livre {
        width: 100%;
    }

    .livre, .textLivre, .couverture, .separator {
        position: inherit;
    }

    .couverture1 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/Power.jpg");
        background-size: cover;
        min-height: 355px;
        margin-right: 4%;
    }

    .couverture2 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/thepoweroffocus.jpg");
        background-size: cover;
        min-height: 438px;
        margin-left: 4%;
    }

    .couverture3 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/stevejobs.jpg");
        background-size: cover;
        min-height: 442px;
        margin-right: 4%;
    }

    .couverture4 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/tribes.jpg");
        background-size: cover;
        min-height: 404px;
        margin-left: 4%;
    }

    .couverture5 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/EntrepreneurialStrengthsFinder.jpg");
        background-size: cover;
        min-height: 400px;
        margin-right: 4%;
    }

    .couverture6 {
        background-image: url("<?php get_site_url(); ?>/wp-content/themes/nibiru/images/ClubLecture/ctrl-alt-delete.jpg");
        background-size: cover;
        min-height: 440px;
        margin-right: 4%;
    }

    p {
        font-weight: bold;
        color: #848484;
        text-align: justify;
    }

</style>

<?php get_footer(); ?>

