<?php
/**
 * The template for displaying the Portfolio page.
 */

include 'CorrespLogo.php';

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");
	?>
    
    <div class="po-page">
        <div class="entry-header po-portfolio-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                <h3 class="entry-title"><?php echo __("TOUS LES SECTEURS"); ?></h3>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="text-right" style="margin-top:26px;"><?php if (function_exists('po_breadcrumbs')) po_breadcrumbs(); ?></div>
                </div>
                
                </div>
            </div>
        </div>

        <div class="title" style="text-align: center;">
        	<div class="titleTitle">
        		<h3><?php echo __("CATÉGORIES"); ?></h3>
        	</div>
        	<div class="reseauxTitle">
        		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
        		<a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
        		<a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
        		<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
        	</div>
        	<div class="separatorTitle" style="text-align: center; background-color: #848484; height: 2px; width: 10%; margin-left: 45%;"></div>
        	<div class="boutonsAncre">
        		<?php 
        		$num = 1;
        		while ( have_posts() ) : the_post(); ?>
        			<a href="<?php the_permalink(); ?>">
        			<div class="boutonAncre" style="width: 12.5%;"><?php the_title(); ?></div>
        			</a>
        		<?php endwhile; ?>
        	</div>
        </div>
 
		<div id="container" class="row po-full-width"><!--<![endif]-->
        <!--[if IE]><div id="" class="row po-full-width"><![endif]-->
        	<?php $hovUrl[] = ""; $numSec = 1; 
			while ( have_posts() ) : the_post(); 
					
				global $post, $portfolio_mb;
				$portfolio_mb->the_meta();
				$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class="cat-sep"></span> ', '' );
				$cat = get_the_title();
				$catId = $correspCatId[$cat];
				$countOpen = "";
				$openOffers = new WP_Query( "post_type=rsjp_job_postings&cat=".$catId."&meta_key=jp_statut&meta_value=En Recrutement&order=ASC" );
				$countOpen = $openOffers->found_posts;
				?>
	
				<div class="portfolio-item portfolio-item-<?php echo $numSec; ?> filter-item column-8 item <?php echo strip_tags($terms); ?> categorie-item"><!--<![endif]-->
					<a class="blocSecteur blocSecteur-<?php echo $numSec; ?>" href="<?php the_permalink(); ?>" id="blocSecteur-<?php echo $numSec; ?>">
						<div class="portfolio-details portfolio-details-<?php echo $numSec; ?>">
							<div class="item-description">
							<div class="description-cell">
								<?php if ($countOpen >= 1) { ?>
									<div id="vignetteJob" style="width: 26px; height: 26px; background-image: url('<?php echo get_template_directory_uri(); ?>/images/vignette.png'); float: right; background-size: contain; color: white; margin-top: -50%; margin-right: 20%;"><?php echo $countOpen; ?></div>
								<?php } ?>
								<h5 class="item-title"><?php the_title(); ?></h5>
							</div>
							</div>
						</div>
						<div class="liquid-container">
							<?php 
							if ( '' == $portfolio_mb->get_the_value('image') ) {
								the_post_thumbnail('full'); 
							} else { ?>
							<img class="img-responsive" src="<?php echo $portfolio_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
							<?php }
							?>	
						</div>
					</a>
					<?php 
					$ub = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					$mid = substr($ub, 0, -4);
					$lastchar = substr($mid, -1);
					if ($lastchar == '1') {
						$m = substr($mid, 0, -1);
						$mid = $m;
					}
					$hovUrl[$numSec] = $mid."_hover.png";
					$numSec++;
					?>
					<style type="text/css">
					</style>
				</div>

			<?php endwhile; ?>
            
            
        <div class="col-sm-12">
        <div class="po-page-portfolio-paginate">
        <div style="text-align: center;">
   			<?php po_paginate(); ?>
        </div>
        </div>
        </div>         
        </div>
                
        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
									[column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
									[column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
									[column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic icon=" icon-viadeo"  size="20"]</a>[/column]
									[column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic icon=" fa-pinterest"  size="20"]</a>[/column]
									[column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>
        </div>

<style type="text/css">
	
	.liquid-container {
		height: 200px !important;
	}

	#container {
		margin-top: 75px;
	}

	.blocSecteur {
		padding-bottom: 6px !important;
	}

	.po-page {
		background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
	}

	.filter-item {
		width: 12.5%;
	}

	.iResTitle {
		font-size: 26px;
		color: #f4545f;
		margin-left: 20px;
		margin-right: 20px;
		margin-bottom: 1%;
		margin-top: 1%;
	}

	.reseauxTitle {
		margin-bottom: 1%;
	}

	.title {
		margin-top: 1%;
		margin-bottom: 6%;
	}

	<?php for ($i=1; $i < 61; $i++) { ?>

		.blocSecteur-<?php echo $i; ?>:hover, .portfolio-item-<?php echo $i; ?>:hover, .portfolio-details-<?php echo $i; ?>:hover { 
			background-image: url("<?php echo $hovUrl[$i]; ?>"); 
			background-size: cover; 
			background-position: 50% 50%; 
		}

	<?php } ?>

	#menu-item-318 > a {
		color: #ea7373 !important;
	}

	.footer-container, .footer-container-bottom {
    	position: fixed;
	}

	body > div.po-nav.nav-fixed-padding > div.po-page > div.entry-header.po-portfolio-header {
		visibility: hidden;
	}

	body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
	body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
		color: #F4545F;
	}

	.boutonsAncre {
		width: 80%;
		margin-left: 10%;
		padding-top: 3%;
		padding-bottom: 80px;
	}

	.boutonAncre {
		float: left;
		margin-top: 10px;
		height: 25px;
		width: 12.5% !important;
		line-height: 13px;
	}

	@media (max-width: 766px) {
		body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
			font-size: 18px;
		}
		.item-title {
			font-size: 10px;
		}
		body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
			font-size: 22px !important;
		}
		.boutonAncre {
			width: 33% !important;
		}
		#container {
			margin-top: 245px;
		}
	}

	#nblikefacebook {
		zoom: 100% !important;
	}

</style>
            
<?php get_footer(); ?>
