<?php 

session_start();

if ( isset($_REQUEST['type']) && $_REQUEST['type']=='add' )  {
	
	$confId = filter_var($_REQUEST["id"], FILTER_SANITIZE_STRING);
	$returnUrl = base64_decode($_REQUEST["returnUrl"]);

	if ( isset($_SESSION["produits"]) ) {
		
		$verif = false;
		$conferenciers = array();

		foreach ($_SESSION["produits"] as $conf) {
			if ( $conf == $confId ) {
				$conferenciers[] = $conf;
				$verif = true;
			} else {
				$conferenciers[] = $conf;
			}
		}

		if ( $verif == false ) {
			$conferenciers[] = $confId;
			$_SESSION["produits"] = $conferenciers;
		} else {
			$_SESSION["produits"] = $conferenciers;
		}

	} else {

		$conferenciers[] = $confId;
		$_SESSION["produits"] = $conferenciers;

	}

	header('Location: '.$returnUrl);

}

if ( isset($_REQUEST["emptycart"]) && $_REQUEST["emptycart"]==1 ) {

	$returnUrl = base64_decode($_REQUEST["returnUrl"]);
	session_destroy();
	header('Location: '.$returnUrl);

}

if ( isset($_REQUEST["remove"]) && isset($_SESSION["produits"]) && isset($_REQUEST["returnUrl"]) ) {
	
	$returnUrl = base64_decode($_REQUEST["returnUrl"]);
	$removeId = $_REQUEST["remove"]; 

	$conferenciers = $_SESSION["produits"];
	$retour = array();
	
	foreach ( $conferenciers as $confId ) {
		if ( $confId != $removeId ) {
			$retour[] = $confId;
		}
	}

	$_SESSION["produits"] = $retour;
	header('Location: '.$returnUrl);

}

?>