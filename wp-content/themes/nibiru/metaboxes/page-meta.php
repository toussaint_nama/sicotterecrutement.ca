<div class="my_meta_control">

	<label>Hide breadcrumbs</label><br/>
	<?php $mb->the_field('cb_single'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="hide"<?php $mb->the_checkbox_state('hide'); ?> />
    
    <label>Remove top padding</label><br/>
	<?php $mb->the_field('top_padding'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="hide"<?php $mb->the_checkbox_state('hide'); ?> />
    
    <label>Hide sidebar</label><br/>
	<?php $mb->the_field('hide_sidebar'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="hide"<?php $mb->the_checkbox_state('hide'); ?> />
   
   
</div>