<?php
/**
 * The template for displaying all portfolio items.
 */

include 'CorrespLogo.php';

global $wpdb;

//Recuperation des donnees concernant l'offre d'emploi
$ID = get_the_ID();
$jobVille = get_post_meta(get_the_ID(), 'jp_lieu', true);
$jobSalaire = get_post_meta(get_the_ID(), 'jp_salaire', true);
$jobExp = get_post_meta(get_the_ID(), 'jp_experience', true);
$jobType = get_post_meta(get_the_ID(), 'jp_type', true);
$jobStatut = get_post_meta(get_the_ID(), 'jp_statut', true);
$jobLangue = get_post_meta(get_the_ID(), 'jp_langue', true);
$category = get_the_category(); 
$jobCat = $category[0]->cat_name;
$catCount = count($category);
$jobCatArray[] = "";
$catLinkArray[] = "";
for ($i=0; $i < $catCount; $i++) { 
	$jobCatArray[$i] = $category[$i]->cat_name;
	$catSlugArray = $category[$i]->slug;
	$args = array(
	  'name'        => $catSlugArray,
	  'post_type'   => 'portfolio'
	);
	$portfolio = get_posts($args);
	$portItem = $portfolio[0];
	if (!is_null($portItem->ID)) {
		$catLinkArray[$i] = get_post_permalink($portItem->ID);
	}
}
if ($ID == 1060) {
    $jobCat = "Opportunités d'Affaires";
}

//Recuperation de la langue
$url = $_SERVER["REQUEST_URI"];
$lang = substr($url, 1, 2);

// Recuperation des donnees du formulaire
$action      = $_POST['action'];
$fname       = esc_html( $_POST['fname'] );
$lname       = esc_html( $_POST['lname'] );
$address     = esc_html( $_POST['address'] );
$address2    = "";
$city        = esc_html( $_POST['city'] );
$state       = $_POST['state'];
$zip         = esc_html( $_POST['zip'] );
$pnumber     = esc_html( $_POST['pnumber'] );
$pnumbertype = "";
$snumber     = esc_html( $_POST['snumber'] );
$snumbertype = "";
$email       = esc_html( $_POST['email'] );
$job         = get_the_title();
$attachment  = array($_FILES['attachment']);
$cover       = "";
$resume      = "";
$fromPosting = "";

$fields  = array( 'fname' => $fname, 'lname' => $lname, 'address' => $address, 'address2' => $address2, 'city' => $city, 'state' => $state,
                  'zip' => $zip, 'pnumber' => $pnumber, 'pnumbertype' => $pnumbertype, 'snumber' => $snumber, 'snumbertype' => $snumbertype, 
                  'email' => $email, 'attachment' => $attachment, 'job' => $job, 'cover' => $cover, 'resume' => $resume );
$pubDate = date('Y-m-d H:i:s');

//Verification du formulaire
$formError = "";
if (($action == 'add') && ($fname != "") && ($lname != "") && ($address != "") && ($city != "") && ($state != "") && ($zip != "") && ($pnumber != "") && ($email != "")) {
    $formError = false;
    echo '<script language="javascript">';
    echo 'alert("Merci d\'avoir postulé")';
    echo '</script>';
} else { $formError = true; }

if( $action == 'add' && $formError == false ) {
    
    // Enregistrement du document attache
    $attachFinal = uploadAttachments( $attachment, 'attachment' );
    
    if ( $attachFinal != 'Error' ){
        
        //Requete d'insertion de la candidature
        $insertQuery = $wpdb->query('INSERT INTO `wp_rsjp_submissions` VALUES (NULL,
                                                                        "' . $fname . '",
                                                                        "' . $lname . '",
                                                                        "' . $address . '",
                                                                        "' . $address2 . '",
                                                                        "' . $city . '",
                                                                        "' . $state . '",
                                                                        "' . $zip . '",
                                                                        "' . $pnumber . '",
                                                                        "' . $pnumbertype . '",
                                                                        "' . $snumber . '",
                                                                        "' . $snumbertype . '",
                                                                        "' . $email . '",
                                                                        "' . $job . '",
                                                                        "' . $attachFinal . '",
                                                                        "' . $cover . '",
                                                                        "' . $resume . '",
                                                                        "' . $pubDate . '")' );
        
        if ( $insertQuery ){
            
            $resumeSubmit = "submitted";
            $cvFile = "http://sicotterecrutement.ca/wp-content/uploads/rsjp/attachments/".$attachFinal."";
            
            // Get the info of the inserted entry so the admin can click on the link, also builds array for replacing the shortcodes
            $upload = $wpdb->get_row( 'SELECT * FROM `wp_rsjp_submissions` WHERE email = "' . $email . '" ORDER BY pubdate DESC LIMIT 1' );
            
            // Send email to the admin
            $admin_to      = 'info@sicotterecrutement.ca';
            $admin_subject = 'Nouvelle candidature déposée';
            $admin_message = '<html>
                                <head>
                                    <title>Nouvelle candidature déposée</title>
                                </head>
                                <body>
                                    <p>' . $fname . ' ' . $lname . ' a uploadé son CV dans notre base de donnée.</p>
                                    <p>La candidature concerne l\'offre : ' . $job . '.</p>
                                    <p><a href="' . admin_url() . 'admin.php?page=rsjp-submissions&id=' . $upload->id . '"><b>Cliquez Ici</b></a> pour consulter les détails de la candidature et <a href="'.$cvFile.'">Ici</a> pour consulter directement le CV.</p>
                                    <br/>
                                </body>
                            </html>';
            
            $admin_headers  = 'MIME-Version: 1.0' . "\r\n";
            $admin_headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $admin_headers .= 'From: "' . $fname . '"<' . $email . '>' . "\r\n";
            wp_mail( $admin_to, $admin_subject, $admin_message, $admin_headers );
          
            // Send email to the user, if enabled
            if ( get_option( 'resume_send_email_to_user' )  == 'Enabled' ) {
                $to      = $email; 
                $subject = get_option( 'resume_user_email_subject' );
                $message = '<html>
                                <head>
                                    <title>' . get_option( 'resume_user_email_subject' ) . '</title>
                                </head>
                                <body>
                                    ' . replaceShortCode( get_option( 'resume_user_email_copy' ), $upload ) . '
                                </body>
                            </html>';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $headers .= 'From: "SICOTTE Recrutement"<info@sicotterecrutement.ca>' . "\r\n";
                wp_mail( $to, $subject, $message, $headers );
            }
        }
    } else {
        $formError = true;
        echo '<script language="javascript">';
        echo 'alert("L extension du fichier fournis ne correspond pas")';
        echo '</script>';
    }
    $upload = $wpdb->get_row( 'SELECT * FROM `wp_rsjp_submissions` ORDER BY pubdate DESC LIMIT 1' );
} 

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");	?>
    
    <div class="po-page">

        <?php 
		global $page_mb; 
        ?>
        
        <div class="row po-full-width">
        <div class="portfolio-page col-sm-12 column-12" style="background-image: none !important;">
            <div class="liquidcontainerpage" style="height: 22%;">
                <div class="logoTitle" style="text-align: right; position: absolute; margin-top: 2%; margin-bottom: -2%; width: 18%;">
                    <?php 
                    for ($i=0; $i < $catCount; $i++) { ?>
                    	<div id="div-categorie<?php echo $i;?>" style="text-align: center; margin-bottom: 10%;">
	                    	<?php 
	                    	$s = $jobCatArray[$i];
	                    	if ( $lang == "en" ) {
	                    	 	$or = $correspTitleEn[$s];
	                    	 	$logo = $correspLogo[$or];
	                    	} else {
	                    		$logo = $correspLogo[$s];
	                    	} ?>
	                    	<a id="a-categorie<?php echo $i;?>" href="<?php echo $catLinkArray[$i]; ?>">
	                        	<span id="job-categorie<?php echo $i;?>" class="icon-<?php echo $logo; ?>" style="font-size: 3em; color: #f4545f;"></span>
	                        </a>
	                        <h2 id="h2-categorie<?php echo $i;?>" style="color: #808080; <?php if($catCount > 1){ echo "margin-left: 50%"; } ?>; margin-top: 0px; <?php if($catCount == 1){ echo "white-space: nowrap;"; } ?>"><?php echo $s; ?></h2>
	                    </div>
                    <?php } ?>
                </div>
                <div class="titleHead col-sm-10">
                    <div class="titleTitle col-md-12">
                        <h1 style="color: #333333; width: 55%;"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    	</div>
            
		<?php while ( have_posts() ) : the_post(); ?>
        <?php global $page_mb;
				if ( 'hide' == $page_mb->get_the_value( 'top_padding' ) ) {
				?>	
            	<div class="container">
                <?php 
            	} else { ?>
                <div class="container page-padding"
                <?php 
				}
				?>

            <div class="row" style="margin-bottom: 10%;">

                <?php global $page_mb; ?>

                <div class="content col-sm-9" style="padding-top: 15px;">
                    <div class="jobContent col-md-9" style="text-align: justify; word-break: break-word;">
                        <?php echo the_content(); ?>
                    </div>

                    <div class="jobDetailDiv col-md-3" style="float: right;">

                        <!-- Bloc details de l'offre -->
                        <div class="detailJob" style="background-color: #eaeaea; float: right; margin-bottom: 4%;">
                            <div class="row">
                                <div class="col-md-2" style="margin-top: 8%;">
                                    <span class="fa fa-map-marker fa-2x iconDet" style="color: #848484;"></span>
                                </div>
                                <div class="col-md-10">
                                    <h6 style="color: #848484; margin-left: 7px;"><?php echo __("Ville"); ?>:</h6>
                                    <?php if ($jobVille == "") {
                                        $jobVille = "NA";
                                    } ?>
                                    <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $jobVille; ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2" style="margin-top: 8%;">
                                    <span class="fa fa-dollar fa-2x iconDet" style="color: #848484;"></span>
                                </div>
                                <div class="col-md-10">
                                    <h6 style="color: #848484; margin-left: 7px;"><?php echo __("Salaire"); ?>:</h6>
                                    <?php if ($jobSalaire == "") {
                                        $jobSalaire = "NA";
                                    } ?>
                                    <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $jobSalaire; ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2" style="margin-top: 8%;">
                                    <span class="fa fa-comment fa-2x iconDet" style="color: #848484;"></span>
                                </div>
                                <div class="col-md-10">
                                    <h6 style="color: #848484; margin-left: 7px;"><?php echo __("Langue"); ?>:</h6>
                                    <?php if ($jobLangue == "") {
                                        $jobLangue = "Français";
                                    } ?>
                                    <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $jobLangue; ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2" style="margin-top: 8%;">
                                    <span class="fa fa-mortar-board fa-2x iconDet" style="color: #848484;"></span>
                                </div>
                                <div class="col-md-10">
                                    <h6 style="color: #848484; margin-left: 7px;"><?php echo __("Années d'experience"); ?>:</h6>
                                    <?php if ($jobExp == "") {
                                        $jobExp = "NA";
                                    } ?>
                                    <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $jobExp; ?> <?php echo __("ans ou plus"); ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2" style="margin-top: 8%;">
                                    <span class="icon-type_emploi fa-2x iconDet" style="color: #848484;"></span>
                                </div>
                                <div class="col-md-10">
                                    <h6 style="color: #848484; margin-left: 7px;"><?php echo __("Type d'emploi"); ?>:</h6>
                                    <?php if ($jobType == "") {
                                        $jobType = "NA";
                                    } 
                                    if ($lang == "en" ) { ?>
                                        <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $correspType[$jobType]; ?></h5>
                                    <?php } else { ?>
                                        <h5 style="color: #333333; margin-top: -5%; margin-left: 7px;"><?php echo $jobType; ?></h5>
                                    <?php }?>
                                </div>
                            </div class="row">
                        </div>

                        <!-- Bloc de partage sur les reseaux sociaux -->
                        <div class="reseauxDet" style="background-color: #eaeaea; float: right; margin-top: 2%; margin-bottom: 4%;">
                            <div style="width: 100%; text-align: center;">
                                <h6 style="font-weight: bold;"><?php echo __("partager sur les réseaux sociaux"); ?></h6>
                            </div>
                            <div style="width: 100%; margin-bottom: 25%;">
                                <div class="col-md-3" style="width: 25%;">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook fa-2x logoReseau"></span></a>
                                </div>
                                <div class="col-md-3" style="width: 25%;">
                                    <a href="http://twitter.com/intent/tweet?text=<?php the_title(); ?>&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter fa-2x logoReseau"></span></a>
                                </div>
                                <div class="col-md-3" style="width: 25%; margin-top: -4px;">
                                    <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo fa-2x logoReseau"></span></a>
                                </div>
                                <div class="col-md-3" style="width: 25%; margin-top: -3px;">
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin fa-2x logoReseau"></span></a>
                                </div>
                            </div>
                        </div>

                        <!-- Bouton pour candidater -->
                        <div class="reseauxDet" style="background-color: #f4545f; float: right; margin-top: 2%; margin-bottom: 2%; width: 100%;">
                            <?php if (($jobStatut == "En Recrutement") || ($jobStatut == "En Entrevue")) { ?>
                                <a href="#resumeform"><h5 style="color: white; text-align: center;"><?php echo __("Postuler pour cet emploi"); ?></h5></a>
                            <?php } elseif ($jobStatut == "Non Disponible") { ?>
                                <a><h5 style="color: white; text-align: center;"><?php echo __("Non Disponible"); ?></h5></a>
                            <?php } else { ?>
                                <a><h5 style="color: white; text-align: center;"><?php echo __("Comblé"); ?></h5></a>
                            <?php } ?>
                        </div>

                    </div>
                    
                    <div class="col-md-12" id="resumeform" style="margin-top: 2%; margin-bottom: 2%;">
                        <h4 style="color: #848484; text-align: center;"><?php echo __("Pour plus de renseignements merci de communiquer avec SICOTTE Recrutement au"); ?> <span style="color: #666666;">514.360.1304</span> <?php echo __("et/ou"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: #f4545f;">info@sicotterecrutement.ca</a></h4>
                    </div>

                    <!-- Formulaire de candidature -->
                    <?php if (($jobStatut == "En Recrutement") || ($jobStatut == "En Entrevue")) { ?>
                    <div class="col-md-12" style="background-color: #f5f5f5; margin-bottom: 2%;">
                        <h2 style="font-weight: 500; color: #666666; margin-bottom: 5%;"><?php echo __("Postuler pour cet emploi"); ?></h2>
                        <form id='formSubmission' method='POST' action="" enctype="multipart/form-data">
                        <table width="100%" cellpadding="10" cellspacing="5">
                            <tr>
                                <td style="color: #f4545f;">* <?php echo __("Requis"); ?></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Prénom"); ?></td>
                                <td><input class="td tdcont" type='text' name='fname' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Nom"); ?></td>
                                <td><input class="td tdcont" type='text' name='lname' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Adresse"); ?></td>
                                <td><input class="td tdcont" type='text' name='address' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Ville"); ?></td>
                                <td><input class="td tdcont" type='text' name='city' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Etat"); ?></td>
                                <td><select class="td tdcont" name="state" id="state">
                                    <option value="Alberta">Alberta AB</option>
                                    <option value="ColombieBritanique">Colombie-Britanique BC</option>
                                    <option value="IleDuPrinceEdouard">Ile-Du-Prince-Edouard PE</option>
                                    <option value="Manitoba">Manitoba MB</option>
                                    <option value="NouveauBrunswick">Nouveau-Brunswick NB</option>
                                    <option value="NouvelleEcosse">Nouvelle-Ecosse NS</option>
                                    <option value="Ontario">Ontario ON</option>
                                    <option value="Quebec">Quebec QC</option>
                                    <option value="Saskatchewan">Saskatchewan SK</option>
                                    <option value="TerreNeuveEtLabrador">Terre-Neuve et Labrador NL</option>
                                    <option value="Nunavut">Nunavut NU</option>
                                    <option value="TerritoiresDuNordOuest">Territoires du Nord-Ouest NT</option>
                                    <option value="Yukon">Yukon YT</option>
                                </select><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Code Postal"); ?></td>
                                <td><input class="td tdcont" type='text' name='zip' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Numéro Principal"); ?></td>
                                <td><input class="td tdcont" type='text' name='pnumber' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Second Numéro"); ?></td>
                                <td><input class="td tdcont" type='text' name='snumber' size='30'/></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Adresse E-Mail"); ?></td>
                                <td><input class="td tdcont" type='text' name='email' size='30'/><b style="color: #f4545f;">   *</b></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"><?php echo __("Pièce(s) Jointe"); ?></td>
                                <td><input class="td tdcont" type="file" name="attachment[]" id="attachment" class="multi" accept=".pdf, .doc, .docx"/></td>
                            </tr>
                            <tr>
                                <td class="td tdtit"></td>
                                <td class="td tdcont" style="zoom: 75%;">.pdf, .doc ou .docx</td>
                            </tr>
                            <input type='hidden' name='action' value='add' />
                            <tr>
                                <td class="td tdtit"></td>
                                <td><p><input class="td tdcont" type='submit' value='<?php echo __("Postuler"); ?>' name='submit' style="color: white; background-color: #f4545f; margin-top: 30px; border: none;" /></p></td>
                            </tr>
                        </table>
                        </form>
                    </div>
                    <?php } ?>
                </div>

                <!-- Sidebar -->
                <div class="col-sm-3">
                    <?php get_sidebar(); ?>

                    <div class="titleSide">
                        <h4 style="text-aligne: left; font-weight: bold; margin-bottom: 5%;"><?php echo __("Offres similaires"); ?></h4>
                    </div>

                    <div class="offresSide">

                        <div class="offresListe">
                            <?php 
                            $args = array(  'posts_per_page'   => 5,
                                            'orderby'          => 'post_date',
                                            'order'            => 'DESC',
                                            'post_type'        => 'rsjp_job_postings',
                                            'category_name'    => $jobCat,
                                            'post_status'      => 'publish');
                            $offres = get_posts( $args );
                            $offreId = "";
                            $offreNom = "";
                            $offreLieu = "";
                            $offreContrat = "";
                            $offreStatut = "";
                            $offreLink = "";
                            $checkColor = "";
                            foreach ($offres as $offre) {
                                $offreId = $offre->ID;
                                $offreNom = $offre->post_title; 
                                $offreLink = $offre->guid;
                                $offreLieu = get_post_meta($offreId, 'jp_lieu', true);
                                $offreContrat = get_post_meta($offreId, 'jp_type', true);
                                $offreStatut = get_post_meta($offreId, 'jp_statut', true);
                                if ($offreStatut == "Combl&eacute") {
                                    $checkColor = "#f4545f";
                                } else { $checkColor = "#1abf52"; }
                                ?>
                                <div class="offre offre-<?php echo $offreId; ?>" style="width: 100%;">
                                    <a href="<?php echo $offreLink; ?>">
                                    <div class="col-md-3" style="margin-top: 10%;">
                                        <span class="icon-valide fa-2x" style="color: <?php echo $checkColor; ?>"></span>
                                    </div>
                                    <div class="col-md-9">
                                        <h5 style="width: 100%; color: #4d4d4d; font-weight: bold;"><?php echo $offreNom; ?></h5>
                                        <h6 class="detOffreSide"><?php echo __("lieu"); ?>: <b style="color: #4d4d4d;"><?php echo $offreLieu; ?></b></h6>
                                        <h6 class="detOffreSide"><?php echo __("contrat"); ?>: <b style="color: #4d4d4d;"><?php echo $offreContrat; ?></b></h6>
                                        <h6 class="detOffreSide" style="color: <?php echo $checkColor; ?>"><?php echo $offreStatut; ?></h6>
                                    </div>
                                    </a>
                                    <div class="col-md-12" style="height: 1px; background-color: black; width: 80%; text-align: center; margin-left: 10%; margin-bottom: 10px; margin-top: 5px;"></div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                    </div>

                    <a href="<?php echo ("".get_site_url()."/offres-demploi/"); ?>"><div class="boutonToutSide col-md-12" style="width: 70%;"><h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Toutes nos offres"); ?></h5></div></a>

                    <div class="textSide col-md-12" style="margin-top: 40px;">
                        <div style="margin-top: 15px; margin-bottom: 15px;">
                            <h4 style="font-weight: bold;"><?php echo __("Pas d'offres pour vous?"); ?></h4>
                            <h6><?php echo __("Aucune offre ne correspond à votre profil, faites nous"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("parvenir votre CV"); ?></a></h6>
                        </div>
                        <div>
                            <h4 style="font-weight: bold;">Infos SICOTTE Recrutement</h4>
                            <h6><?php echo __("Vous avez besoin de plus d'informations?"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("Ecrivez-nous un courriel"); ?></a></h6>
                        </div>
                    </div>

                </div>	

            </div>

        </div>

        <div style="background-color: rgb(245,245,245); width: 240px; margin-left: auto; margin-right: auto;">
            <?php echo do_shortcode("[button style='top' color='light' link='".get_post_type_archive_link( 'portfolio' )."' icon='fa-folder-open-o' width='240']". __('Voir toutes nos industries','pixelobject')."[/button]"); ?>           
        </div>

        <!-- Bouton reseaux sociaux -->
        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>
        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>

        <?php 
        // get the custom post type's taxonomy terms
          
        $custom_taxterms = wp_get_object_terms( $post->ID, 'portfolio_categories', array('fields' => 'ids') );
        // arguments
        $args = array(
        'post_type' => 'portfolio',
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'portfolio_categories',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array ($post->ID),
        );
        $related_items = new WP_Query( $args );

        // Reset Post Data
        wp_reset_postdata();
        ?>
           
		<?php endwhile; // end of the loop. ?>

<script type="text/javascript">

    jQuery(".actuContent").hide();
    jQuery(".eventContent").hide();

    jQuery(".indusButton").click(function(){
        jQuery(".indusContent").show();
        jQuery(".actuContent").hide();
        jQuery(".eventContent").hide();
        jQuery(".indusButton").css("background-color", "rgb(238,238,238)");
        jQuery(".actuButton").css("background-color", "rgb(245,245,245)");
        jQuery(".eventButton").css("background-color", "rgb(245,245,245)");
    });
    jQuery(".actuButton").click(function(){
        jQuery(".indusContent").hide();
        jQuery(".actuContent").show();
        jQuery(".eventContent").hide();
        jQuery(".actuButton").css("background-color", "rgb(238,238,238)");
        jQuery(".indusButton").css("background-color", "rgb(245,245,245)");
        jQuery(".eventButton").css("background-color", "rgb(245,245,245)");
    });
    jQuery(".eventButton").click(function(){
        jQuery(".indusContent").hide();
        jQuery(".actuContent").hide();
        jQuery(".eventContent").show();
        jQuery(".eventButton").css("background-color", "rgb(238,238,238)");
        jQuery(".actuButton").css("background-color", "rgb(245,245,245)");
        jQuery(".indusButton").css("background-color", "rgb(245,245,245)");
    });

    var largeur = parseInt($(".container.page-padding").css("margin-left").replace("px", ""));	
    if (largeur > 100) { largeur = 100; }
    //$("#job-categorie0").css("font-size", largeur);
    if (parseInt($("#job-categorie0").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie0").css("margin-left", "50%"); $("#h2-categorie0").css("margin-left", "50%");} 
    //$("#job-categorie1").css("font-size", largeur);
    if (parseInt($("#job-categorie1").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie1").css("margin-left", "50%"); $("#h2-categorie1").css("margin-left", "50%");}
    //$("#job-categorie2").css("font-size", largeur);
    if (parseInt($("#job-categorie2").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie2").css("margin-left", "50%"); $("#h2-categorie2").css("margin-left", "50%");}
    //$("#job-categorie3").css("font-size", largeur);
    if (parseInt($("#job-categorie3").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie3").css("margin-left", "50%"); $("#h2-categorie3").css("margin-left", "50%");}
    //$("#job-categorie4").css("font-size", largeur);
    if (parseInt($("#job-categorie4").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie4").css("margin-left", "50%"); $("#h2-categorie4").css("margin-left", "50%");}
    //$("#job-categorie5").css("font-size", largeur);
    if (parseInt($("#job-categorie5").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie5").css("margin-left", "50%"); $("#h2-categorie5").css("margin-left", "50%");}
    //$("#job-categorie6").css("font-size", largeur);
    if (parseInt($("#job-categorie6").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie6").css("margin-left", "50%"); $("#h2-categorie6").css("margin-left", "50%");}
    //$("#job-categorie7").css("font-size", largeur);
if (parseInt($("#job-categorie7").css("width").replace("px","")) < (largeur / 2)) {$("#job-categorie7").css("margin-left", "50%"); $("#h2-categorie7").css("margin-left", "50%");}

</script>
			
<style type="text/css">

	.po-nav.nav-fixed-padding {
		background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
	}
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    .buttonTitle {
        width: 120px;
        margin-left: -3px !important;
        margin-right: -3px !important;
        padding-left: 1.5%;
        padding-right: 1.5%;
        background-color: rgb(245,245,245);
        border-radius: 4px;
        border: 1px solid;
        border-color: rgb(231,231,231);
    }

    .buttonTitle:hover {
        background-color: rgb(238,238,238);
    }

    .titleHead {
        margin-left: 18%;
        margin-top: 4%;
        margin-bottom: -2%;
    }

    #search-2 > form > input { background-color: white; }

    body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width > div > div { height: 240px; }

    .content {
        background-color: #f0f0f0;
    }

    .logoReseau { color: #f4545f; }

    .tdtit { width: 28%; color: #848484; }
    .tdcont { width: 62%; }
    td, tr { padding-top: 9px; }
    input, select { background-color: transparent; }
    input { border: 1px solid; border-color: #848484; }

    body > div.container.po-container-section { display: none; }

    .detOffreSide {
        margin-top: 2px;
        margin-bottom: 2px;
        color: #848484;
    }

    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(5) > b,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(8) > strong,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(11) > strong,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(15) > strong { 
        color: #f4545f; }

    .boutonToutSide {
        border: 1px solid;
        border-color: #f4545f;
        background-color: rgb(245,245,245);
        margin-left: 15%;
        margin-top: 40px;
    }

    body > div.po-nav.nav-fixed-padding > div > div:nth-child(3) {
    	width: 240px !important;
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    li {
    	list-style-type: initial;
    	list-style-position: initial;
		list-style-image: initial;
    }

    @media (max-width: 766px) {
        body > div.po-nav.nav-fixed-padding > div > div:nth-child(3) {
            margin-left: auto !important;
            margin-right: auto;
        }
        body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
            font-size: 18px;
        }
        body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
            font-size: 22px !important;
        }
        .detailJob, .reseauxDet {
            float: none !important;
        }
        .detailJob > div > .col-md-2 {
            width: 20%;
            float: left;
        }
        .detailJob > div > .col-md-10 {
            width: 80%;
            float: left;
        }
        /*
        .detailJob > div {
            display: inline-block;
        }
        */
        .reseauxDet > div > .col-md-3 {
            float: left;
        }
        body > div.po-nav.nav-fixed-padding > div:nth-child(3) > div > div > div > a {
    		width: 100% !important;
    	}
    	body > div.po-nav.nav-fixed-padding > div:nth-child(3) > div > div > div > a > span {
    		font-size: 12px; 
    	}
    	body > div.po-nav.nav-fixed-padding {
    		background-image: url("http://sicotterecrutement.ca/wp-content/themes/nibiru/images/texturefond.jpg");
    	}
    	div.logoTitle {
    		width: 100% !important;
    		display: inline-block;
    		position: relative !important;
    	}
    	div.logoTitle > div {
    		width: 33% !important;
    		float: left;
    		margin-bottom: 0px !important;
    	}
    	.titleHead {
    		margin-left: auto !important;
    		margin-right: auto !important;
    	}
    	.titleTitle > h1 {
    		width: 100% !important;
    	}
    }

    @media (max-width: 1700px) {
    	div.logoTitle > div > h2 {
            font-size: 100%;
        }
    }

    @media (max-width: 1500px) {
    	div.logoTitle > div {
            width: 13%;
        }
        div.logoTitle {
            padding-left: 15px;
        }
    }

    @media (max-width: 1400px) {
    	div.logoTitle > div {
            width: 9%;
        }
        div.logoTitle > div > span {
            font-size: 366% !important;
        }
    }

    #nblikefacebook {
        zoom: 100% !important;
    }

</style>

<?php get_footer(); ?>
