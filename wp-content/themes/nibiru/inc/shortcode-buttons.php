<?php

/* SECTION SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_section_button( $buttons ) {
   array_push( $buttons, "|", "section" );
   return $buttons;
}

function po_add_section_plugin( $plugin_array ) {
   $plugin_array['section'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_section_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_section_plugin' );
      add_filter( 'mce_buttons', 'po_register_section_button' );
   }

}

add_action('init', 'po_section_button');


/* COLUMN SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_column_button( $buttons ) {
   array_push( $buttons, "|", "column" );
   return $buttons;
}

function po_add_column_plugin( $plugin_array ) {
   $plugin_array['column'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_column_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_column_plugin' );
      add_filter( 'mce_buttons', 'po_register_column_button' );
   }

}

add_action('init', 'po_column_button');


/* NEWROW SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_newrow_button( $buttons ) {
   array_push( $buttons, "|", "newrow" );
   return $buttons;
}

function po_add_newrow_plugin( $plugin_array ) {
   $plugin_array['newrow'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_newrow_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_newrow_plugin' );
      add_filter( 'mce_buttons', 'po_register_newrow_button' );
   }

}

add_action('init', 'po_newrow_button');



/* HEADER SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_header_button( $buttons ) {
   array_push( $buttons, "|", "header" );
   return $buttons;
}

function po_add_header_plugin( $plugin_array ) {
   $plugin_array['header'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_header_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_header_plugin' );
      add_filter( 'mce_buttons', 'po_register_header_button' );
   }

}

add_action('init', 'po_header_button');


/* TEXT SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_text_button( $buttons ) {
   array_push( $buttons, "|", "text" );
   return $buttons;
}

function po_add_text_plugin( $plugin_array ) {
   $plugin_array['text'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_text_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_text_plugin' );
      add_filter( 'mce_buttons', 'po_register_text_button' );
   }

}

add_action('init', 'po_text_button');


/* MEDIA SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_pomedia_button( $buttons ) {
   array_push( $buttons, "|", "pomedia" );
   return $buttons;
}

function po_add_pomedia_plugin( $plugin_array ) {
   $plugin_array['pomedia'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_pomedia_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_pomedia_plugin' );
      add_filter( 'mce_buttons', 'po_register_pomedia_button' );
   }

}

add_action('init', 'po_pomedia_button');


/* BUTTON SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_po_button_button( $buttons ) {
   array_push( $buttons, "|", "po_button" );
   return $buttons;
}

function po_add_po_button_plugin( $plugin_array ) {
   $plugin_array['po_button'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_po_button_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_po_button_plugin' );
      add_filter( 'mce_buttons', 'po_register_po_button_button' );
   }

}

add_action('init', 'po_po_button_button');


/* ICONBOX SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_iconbox_button( $buttons ) {
   array_push( $buttons, "|", "iconbox" );
   return $buttons;
}

function po_add_iconbox_plugin( $plugin_array ) {
   $plugin_array['iconbox'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_iconbox_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_iconbox_plugin' );
      add_filter( 'mce_buttons', 'po_register_iconbox_button' );
   }

}

add_action('init', 'po_iconbox_button');

/* COUNT SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_count_button( $buttons ) {
   array_push( $buttons, "|", "count" );
   return $buttons;
}

function po_add_count_plugin( $plugin_array ) {
   $plugin_array['count'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_count_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_count_plugin' );
      add_filter( 'mce_buttons', 'po_register_count_button' );
   }

}

add_action('init', 'po_count_button');

/* PROGRESS BAR SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_progress_button( $buttons ) {
   array_push( $buttons, "|", "progress" );
   return $buttons;
}

function po_add_progress_plugin( $plugin_array ) {
   $plugin_array['progress'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_progress_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_progress_plugin' );
      add_filter( 'mce_buttons', 'po_register_progress_button' );
   }

}

add_action('init', 'po_progress_button');

/* PORTFOLIO SHOWCASE SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_portfolios_button( $buttons ) {
   array_push( $buttons, "|", "portfolios" );
   return $buttons;
}

function po_add_portfolios_plugin( $plugin_array ) {
   $plugin_array['portfolios'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_portfolios_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_portfolios_plugin' );
      add_filter( 'mce_buttons', 'po_register_portfolios_button' );
   }

}

add_action('init', 'po_portfolios_button');


/* PORTFOLIO DETAILS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_portfoliod_button( $buttons ) {
   array_push( $buttons, "|", "portfoliod" );
   return $buttons;
}

function po_add_portfoliod_plugin( $plugin_array ) {
   $plugin_array['portfoliod'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_portfoliod_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_portfoliod_plugin' );
      add_filter( 'mce_buttons', 'po_register_portfoliod_button' );
   }

}

add_action('init', 'po_portfoliod_button');


/* CAROUSEL SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_carousel_button( $buttons ) {
   array_push( $buttons, "|", "carousel" );
   return $buttons;
}

function po_add_carousel_plugin( $plugin_array ) {
   $plugin_array['carousel'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_carousel_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_carousel_plugin' );
      add_filter( 'mce_buttons', 'po_register_carousel_button' );
   }

}

add_action('init', 'po_carousel_button');


/* GOOGLEMAPS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_googlemaps_button( $buttons ) {
   array_push( $buttons, "|", "googlemaps" );
   return $buttons;
}

function po_add_googlemaps_plugin( $plugin_array ) {
   $plugin_array['googlemaps'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_googlemaps_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_googlemaps_plugin' );
      add_filter( 'mce_buttons', 'po_register_googlemaps_button' );
   }

}

add_action('init', 'po_googlemaps_button');


/* CLIENTS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_clients_button( $buttons ) {
   array_push( $buttons, "|", "clients" );
   return $buttons;
}

function po_add_clients_plugin( $plugin_array ) {
   $plugin_array['clients'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_clients_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_clients_plugin' );
      add_filter( 'mce_buttons', 'po_register_clients_button' );
   }

}

add_action('init', 'po_clients_button');



/* TEAM SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_team_button( $buttons ) {
   array_push( $buttons, "|", "team" );
   return $buttons;
}

function po_add_team_plugin( $plugin_array ) {
   $plugin_array['team'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_team_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_team_plugin' );
      add_filter( 'mce_buttons', 'po_register_team_button' );
   }

}

add_action('init', 'po_team_button');


/* TESTIMONIALS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_testimonials_button( $buttons ) {
   array_push( $buttons, "|", "testimonials" );
   return $buttons;
}

function po_add_testimonials_plugin( $plugin_array ) {
   $plugin_array['testimonials'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_testimonials_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_testimonials_plugin' );
      add_filter( 'mce_buttons', 'po_register_testimonials_button' );
   }

}

add_action('init', 'po_testimonials_button');


/* SOCIAL SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_social_button( $buttons ) {
   array_push( $buttons, "|", "social" );
   return $buttons;
}

function po_add_social_plugin( $plugin_array ) {
   $plugin_array['social'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_social_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_social_plugin' );
      add_filter( 'mce_buttons', 'po_register_social_button' );
   }

}

add_action('init', 'po_social_button');


/* SOCIAL PERSON SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_social_person_button( $buttons ) {
   array_push( $buttons, "|", "social_person" );
   return $buttons;
}

function po_add_social_person_plugin( $plugin_array ) {
   $plugin_array['social_person'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_social_person_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_social_person_plugin' );
      add_filter( 'mce_buttons', 'po_register_social_person_button' );
   }

}

add_action('init', 'po_social_person_button');


/* TWEETS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_tweets_button( $buttons ) {
   array_push( $buttons, "|", "tweets" );
   return $buttons;
}

function po_add_tweets_plugin( $plugin_array ) {
   $plugin_array['tweets'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_tweets_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_tweets_plugin' );
      add_filter( 'mce_buttons', 'po_register_tweets_button' );
   }

}

add_action('init', 'po_tweets_button');


/* RECENT POSTS SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_recent_posts_button( $buttons ) {
   array_push( $buttons, "|", "recent_posts" );
   return $buttons;
}

function po_add_recent_posts_plugin( $plugin_array ) {
   $plugin_array['recent_posts'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_recent_posts_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_recent_posts_plugin' );
      add_filter( 'mce_buttons', 'po_register_recent_posts_button' );
   }

}

add_action('init', 'po_recent_posts_button');


/* FOOTER PORTFOLIO SHORTCODE BUTTON
------------------------------------------------------- */  

function po_register_footer_portfolio_button( $buttons ) {
   array_push( $buttons, "|", "footer_portfolio" );
   return $buttons;
}

function po_add_footer_portfolio_plugin( $plugin_array ) {
   $plugin_array['footer_portfolio'] = get_template_directory_uri() . '/js/shortcode-buttons/shortcode-buttons.js';
   return $plugin_array;
}

function po_footer_portfolio_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'po_add_footer_portfolio_plugin' );
      add_filter( 'mce_buttons', 'po_register_footer_portfolio_button' );
   }

}

add_action('init', 'po_footer_portfolio_button');