<?php

/* GALLERY CATEGORY TAXONOMY
------------------------------------------------------- */  

function po_gallery_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Gallery Categories', 'taxonomy general name', 'po_gallery_cat' ),
		'singular_name'     => _x( 'Gallery Category', 'taxonomy singular name', 'po_gallery_cat' ),
		'search_items'      => __( 'Search Gallery Categories', 'po_gallery_cat' ),
		'all_items'         => __( 'All Gallery Categories', 'po_gallery_cat' ),
		'parent_item'       => __( 'Parent Gallery Category', 'po_gallery_cat' ),
		'parent_item_colon' => __( 'Parent Gallery Category:', 'po_gallery_cat' ),
		'edit_item'         => __( 'Edit Gallery Category', 'po_gallery_cat' ),
		'update_item'       => __( 'Update Gallery Category', 'po_gallery_cat' ),
		'add_new_item'      => __( 'Add New Gallery Category', 'po_gallery_cat' ),
		'new_item_name'     => __( 'New Gallery Category Name', 'po_gallery_cat' ),
		'menu_name'         => __( 'Gallery Categories', 'po_gallery_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'gallery_categories', array( 'gallery' ), $args );
}
add_action( 'init', 'po_gallery_cat' );


/* GALLERY POST TYPE
------------------------------------------------------- */  

function po_galleries() {

	$labels = array(
		'name'                => _x( 'Galleries', 'Post Type General Name', 'po_gallery' ),
		'singular_name'       => _x( 'Galleries', 'Post Type Singular Name', 'po_gallery' ),
		'menu_name'           => __( 'Galleries', 'po_gallery' ),
		'parent_item_colon'   => __( 'Parent Gallery:', 'po_gallery' ),
		'all_items'           => __( 'Galleries', 'po_gallery' ),
		'view_item'           => __( 'View Gallery', 'po_gallery' ),
		'add_new_item'        => __( 'Add New', 'po_gallery' ),
		'add_new'             => __( 'Add New', 'po_gallery' ),
		'edit_item'           => __( 'Edit Gallery', 'po_gallery' ),
		'update_item'         => __( 'Update Gallery', 'po_gallery' ),
		'search_items'        => __( 'Search Galleries', 'po_gallery' ),
		'not_found'           => __( 'No Galleries found', 'po_gallery' ),
		'not_found_in_trash'  => __( 'No Galleries found in Trash', 'po_gallery' ),
	);
	$args = array(
		'label'               => __( 'Gallery', 'po_gallery' ),
		'description'         => __( 'Gallery', 'po_gallery' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		'taxonomies'          => array( 'gallery_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-images-alt2',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'gallery', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_galleries', 0 );


/* SLIDER TITLES CATEGORY TAXONOMY
------------------------------------------------------- */  

function po_slider_title_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Slider Title Categories', 'taxonomy general name', 'po_slider_title_cat' ),
		'singular_name'     => _x( 'Slider Title Category', 'taxonomy singular name', 'po_slider_title_cat' ),
		'search_items'      => __( 'Search Slider Title Categories', 'po_slider_title_cat' ),
		'all_items'         => __( 'All Slider Title Categories', 'po_slider_title_cat' ),
		'parent_item'       => __( 'Parent Slider Title Category', 'po_slider_title_cat' ),
		'parent_item_colon' => __( 'Parent Slider Title Category:', 'po_slider_title_cat' ),
		'edit_item'         => __( 'Edit Slider Title Category', 'po_slider_title_cat' ),
		'update_item'       => __( 'Update Slider Title Category', 'po_slider_title_cat' ),
		'add_new_item'      => __( 'Add New Slider Title Category', 'po_slider_title_cat' ),
		'new_item_name'     => __( 'New Slider Title Category Name', 'po_slider_title_cat' ),
		'menu_name'         => __( 'ST Categories', 'po_slider_title_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'slider-title-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'slider_title_categories', array( 'slider_titles' ), $args );
}
add_action( 'init', 'po_slider_title_cat' );



/* SLIDER TITLES POST TYPE
------------------------------------------------------- */  

function po_slider_titles() {

	$labels = array(
		'name'                => _x( 'Slider Titles', 'Post Type General Name', 'po_slider_title' ),
		'singular_name'       => _x( 'Slider Titles', 'Post Type Singular Name', 'po_slider_title' ),
		'menu_name'           => __( 'Slider Titles', 'po_slider_title' ),
		'parent_item_colon'   => __( 'Parent Slider Titles:', 'po_slider_title' ),
		'all_items'           => __( 'Slider Titles', 'po_slider_title' ),
		'view_item'           => __( 'View Slider Titles', 'po_slider_title' ),
		'add_new_item'        => __( 'Add New', 'po_slider_title' ),
		'add_new'             => __( 'Add New', 'po_slider_title' ),
		'edit_item'           => __( 'Edit Slider Titles', 'po_slider_title' ),
		'update_item'         => __( 'Update Slider Titles', 'po_slider_title' ),
		'search_items'        => __( 'Search Slider Titles', 'po_slider_title' ),
		'not_found'           => __( 'No Slider Titles found', 'po_slider_title' ),
		'not_found_in_trash'  => __( 'No Slider Titles found in Trash', 'po_slider_title' ),
	);
	$args = array(
		'label'               => __( 'Slider Titles', 'po_slider_title' ),
		'description'         => __( 'Slider Titles', 'po_slider_title' ),
		'labels'              => $labels,
		'supports'            => array( 'title'),
		'taxonomies'          => array( 'slider_title_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-editor-paste-text',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'slider_titles', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_slider_titles', 0 );

/* PORTFOLIO CATEGORIES TAXONOMY
------------------------------------------------------- */  

function po_portfolio_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Portfolio Categories', 'taxonomy general name', 'po_portfolio_cat' ),
		'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name', 'po_portfolio_cat' ),
		'search_items'      => __( 'Search Portfolio Categories', 'po_portfolio_cat' ),
		'all_items'         => __( 'All Portfolio Categories', 'po_portfolio_cat' ),
		'parent_item'       => __( 'Parent Portfolio Category', 'po_portfolio_cat' ),
		'parent_item_colon' => __( 'Parent Portfolio Category:', 'po_portfolio_cat' ),
		'edit_item'         => __( 'Edit Portfolio Category', 'po_portfolio_cat' ),
		'update_item'       => __( 'Update Portfolio Category', 'po_portfolio_cat' ),
		'add_new_item'      => __( 'Add New', 'po_portfolio_cat' ),
		'new_item_name'     => __( 'New Portfolio Category Name', 'po_portfolio_cat' ),
		'menu_name'         => __( 'Portfolio Categories', 'po_portfolio_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'portfolio-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'portfolio_categories', array( 'portfolio' ), $args );
}
add_action( 'init', 'po_portfolio_cat' );



/* PORTFOLIO POST TYPE
------------------------------------------------------- */  

function po_portfolios() {
	
	$portfolioslug = get_theme_mod( 'portfolio_slug');

	$labels = array(
		'name'                => _x( 'Portfolio', 'Post Type General Name', 'po_portfolio' ),
		'singular_name'       => _x( 'Portfolio', 'Post Type Singular Name', 'po_portfolio' ),
		'menu_name'           => __( 'Portfolio', 'po_portfolio' ),
		'parent_item_colon'   => __( 'Parent Portfolio:', 'po_portfolio' ),
		'all_items'           => __( 'Portfolios', 'po_portfolio' ),
		'view_item'           => __( 'View Portfolio Item', 'po_portfolio' ),
		'add_new_item'        => __( 'Add New', 'po_portfolio' ),
		'add_new'             => __( 'Add New', 'po_portfolio' ),
		'edit_item'           => __( 'Edit Portfolio Item', 'po_portfolio' ),
		'update_item'         => __( 'Update Portfolio Item', 'po_portfolio' ),
		'search_items'        => __( 'Search portfolio Items', 'po_portfolio' ),
		'not_found'           => __( 'No portfolio items found', 'po_portfolio' ),
		'not_found_in_trash'  => __( 'No portfolios items found in Trash', 'po_portfolio' ),
	);
	$args = array(
		'label'               => __( 'portfolio', 'po_portfolio' ),
		'description'         => __( 'Portfolio', 'po_portfolio' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'portfolio_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-format-image',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'           => array( 'slug' => $portfolioslug ,'with_front' => false ),
	);
	register_post_type( 'portfolio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_portfolios', 0 );


/* CAROUSEL GROUPS TAXONOMY
------------------------------------------------------- */  

function po_carousel_groups() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Carousel Categories', 'taxonomy general name', 'po_carousels_cat' ),
		'singular_name'     => _x( 'Carousel Category', 'taxonomy singular name', 'po_carousels_cat' ),
		'search_items'      => __( 'Search Carousel Categories', 'po_carousels_cat' ),
		'all_items'         => __( 'All Carousel Categories', 'po_carousels_cat' ),
		'parent_item'       => __( 'Parent Carousel Category', 'po_carousels_cat' ),
		'parent_item_colon' => __( 'Parent Carousel Category:', 'po_carousels_cat' ),
		'edit_item'         => __( 'Edit Carousel Category', 'po_carousels_cat' ),
		'update_item'       => __( 'Update Carousel Category', 'po_carousels_cat' ),
		'add_new_item'      => __( 'Add New Carousel Category', 'po_carousels_cat' ),
		'new_item_name'     => __( 'New Carousel Category Name', 'po_carousels_cat' ),
		'menu_name'         => __( 'Carousel Categories', 'po_carousels_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'carousel-group' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'carousel_groups', array( 'carousel' ), $args );
}
add_action( 'init', 'po_carousel_groups' );



/* CAROUSEL POST TYPE
------------------------------------------------------- */  

function po_carousel() {

	$labels = array(
		'name'                => _x( 'Carousel', 'Post Type General Name', 'po_carousels' ),
		'singular_name'       => _x( 'Carousel', 'Post Type Singular Name', 'po_carousels' ),
		'menu_name'           => __( 'Carousel', 'po_carousels' ),
		'parent_item_colon'   => __( 'Parent Carousel:', 'po_carousels' ),
		'all_items'           => __( 'Carousels', 'po_carousels' ),
		'view_item'           => __( 'View Carousel Item', 'po_carousels' ),
		'add_new_item'        => __( 'Add New', 'po_carousels' ),
		'add_new'             => __( 'Add New', 'po_carousels' ),
		'edit_item'           => __( 'Edit Carousel Item', 'po_carousels' ),
		'update_item'         => __( 'Update Carousel Item', 'po_carousels' ),
		'search_items'        => __( 'Search Carousel Items', 'po_carousels' ),
		'not_found'           => __( 'No Carousel items found', 'po_carousels' ),
		'not_found_in_trash'  => __( 'No Carousel items found in Trash', 'po_carousels' ),
	);
	$args = array(
		'label'               => __( 'Carousel', 'po_carousels' ),
		'description'         => __( 'Carousel', 'po_carousels' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		'taxonomies'          => array( 'carousel_groups' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-slides',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'carousel', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_carousel', 0 );


/* CLIENT CATEGORIES TAXONOMY
------------------------------------------------------- */  

function po_client_show_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Client Categories', 'taxonomy general name', 'po_client_show_cat' ),
		'singular_name'     => _x( 'Client Category', 'taxonomy singular name', 'po_client_show_cat' ),
		'search_items'      => __( 'Search Client Categories', 'po_client_show_cat' ),
		'all_items'         => __( 'All Client Categories', 'po_client_show_cat' ),
		'parent_item'       => __( 'Parent Client Category', 'po_client_show_cat' ),
		'parent_item_colon' => __( 'Parent Client Category:', 'po_client_show_cat' ),
		'edit_item'         => __( 'Edit Client Category', 'po_client_show_cat' ),
		'update_item'       => __( 'Update Client Category', 'po_client_show_cat' ),
		'add_new_item'      => __( 'Add New Client Category', 'po_client_show_cat' ),
		'new_item_name'     => __( 'New Client Category Name', 'po_client_show_cat' ),
		'menu_name'         => __( 'Client Categories', 'po_client_show_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'client-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'client_categories', array( 'clients' ), $args );
}
add_action( 'init', 'po_client_show_cat' );



/* CLIENT POST TYPE
------------------------------------------------------- */  

function po_client_shows() {

	$labels = array(
		'name'                => _x( 'Clients', 'Post Type General Name', 'po_client_show' ),
		'singular_name'       => _x( 'Clients', 'Post Type Singular Name', 'po_client_show' ),
		'menu_name'           => __( 'Clients', 'po_client_show' ),
		'parent_item_colon'   => __( 'Parent Client:', 'po_client_show' ),
		'all_items'           => __( 'Clients', 'po_client_show' ),
		'view_item'           => __( 'View Clients', 'po_client_show' ),
		'add_new_item'        => __( 'Add New', 'po_client_show' ),
		'add_new'             => __( 'Add New', 'po_client_show' ),
		'edit_item'           => __( 'Edit Client', 'po_client_show' ),
		'update_item'         => __( 'Update Client', 'po_client_show' ),
		'search_items'        => __( 'Search Client', 'po_client_show' ),
		'not_found'           => __( 'No cliens found', 'po_client_show' ),
		'not_found_in_trash'  => __( 'No clients found in Trash', 'po_client_show' ),
	);
	$args = array(
		'label'               => __( 'Clients', 'po_client_show' ),
		'description'         => __( 'Clients', 'po_client_show' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		'taxonomies'          => array( 'client_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-businessman',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'clients', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_client_shows', 0 );

/* TEAM CATEGORIES TAXONOMY
------------------------------------------------------- */  

function po_team_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Team Categories', 'taxonomy general name', 'po_team_cat' ),
		'singular_name'     => _x( 'Team Category', 'taxonomy singular name', 'po_team_cat' ),
		'search_items'      => __( 'Search Team Categories', 'po_team_cat' ),
		'all_items'         => __( 'All Team Categories', 'po_team_cat' ),
		'parent_item'       => __( 'Parent Team Category', 'po_team_cat' ),
		'parent_item_colon' => __( 'Parent Team Category:', 'po_team_cat' ),
		'edit_item'         => __( 'Edit Team Category', 'po_team_cat' ),
		'update_item'       => __( 'Update Team Category', 'po_team_cat' ),
		'add_new_item'      => __( 'Add New Team Category', 'po_team_cat' ),
		'new_item_name'     => __( 'New Team Category Name', 'po_team_cat' ),
		'menu_name'         => __( 'Team Categories', 'po_team_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'team-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'team_categories', array( 'team' ), $args );
}
add_action( 'init', 'po_team_cat' );



/* TEAM POST TYPE
------------------------------------------------------- */  

function po_teams() {
	
	$teamslug = get_theme_mod( 'team_slug');

	$labels = array(
		'name'                => _x( 'Team', 'Post Type General Name', 'po_team' ),
		'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'po_team' ),
		'menu_name'           => __( 'Team', 'po_team' ),
		'parent_item_colon'   => __( 'Parent Team:', 'po_team' ),
		'all_items'           => __( 'Team', 'po_team' ),
		'view_item'           => __( 'View Team Member', 'po_team' ),
		'add_new_item'        => __( 'Add New', 'po_team' ),
		'add_new'             => __( 'Add New', 'po_team' ),
		'edit_item'           => __( 'Edit Team Member', 'po_team' ),
		'update_item'         => __( 'Update Team Member', 'po_team' ),
		'search_items'        => __( 'Search Team Members', 'po_team' ),
		'not_found'           => __( 'No Team Members found', 'po_team' ),
		'not_found_in_trash'  => __( 'No teams members found in Trash', 'po_team' ),
	);
	$args = array(
		'label'               => __( 'Team', 'po_team' ),
		'description'         => __( 'Team', 'po_team' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'team_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'           => array( 'slug' => $teamslug ,'with_front' => false ),
	);
	register_post_type( 'team', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_teams', 0 );

/* TESTIMONIALS CATEGORY TAXONOMY
------------------------------------------------------- */  

function po_testimonial_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Testimonial Categories', 'taxonomy general name', 'po_testimonial_cat' ),
		'singular_name'     => _x( 'Testimonial Category', 'taxonomy singular name', 'po_testimonial_cat' ),
		'search_items'      => __( 'Search Testimonial Categories', 'po_testimonial_cat' ),
		'all_items'         => __( 'All Testimonial Categories', 'po_testimonial_cat' ),
		'parent_item'       => __( 'Parent Testimonial Category', 'po_testimonial_cat' ),
		'parent_item_colon' => __( 'Parent Testimonial Category:', 'po_testimonial_cat' ),
		'edit_item'         => __( 'Edit Testimonial Category', 'po_testimonial_cat' ),
		'update_item'       => __( 'Update Testimonial Category', 'po_testimonial_cat' ),
		'add_new_item'      => __( 'Add New Testimonial Category', 'po_testimonial_cat' ),
		'new_item_name'     => __( 'New Testimonial Category Name', 'po_testimonial_cat' ),
		'menu_name'         => __( 'Testimonial Categories', 'po_testimonial_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'testimonial-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'testimonial_categories', array( 'testimonials' ), $args );
}
add_action( 'init', 'po_testimonial_cat' );



/* TESTIMONIALS POST TYPE
------------------------------------------------------- */  

function po_testimonials() {

	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'po_testimonial' ),
		'singular_name'       => _x( 'Testimonials', 'Post Type Singular Name', 'po_testimonial' ),
		'menu_name'           => __( 'Testimonials', 'po_testimonial' ),
		'parent_item_colon'   => __( 'Parent Testimonials:', 'po_testimonial' ),
		'all_items'           => __( 'Testimonials', 'po_testimonial' ),
		'view_item'           => __( 'View Testimonials', 'po_testimonial' ),
		'add_new_item'        => __( 'Add New', 'po_testimonial' ),
		'add_new'             => __( 'Add New', 'po_testimonial' ),
		'edit_item'           => __( 'Edit Testimonials', 'po_testimonial' ),
		'update_item'         => __( 'Update Testimonials', 'po_testimonial' ),
		'search_items'        => __( 'Search Testimonials', 'po_testimonial' ),
		'not_found'           => __( 'No Testimonials found', 'po_testimonial' ),
		'not_found_in_trash'  => __( 'No testimonials found in Trash', 'po_testimonial' ),
	);
	$args = array(
		'label'               => __( 'Testimonials', 'po_testimonial' ),
		'description'         => __( 'Testimonials', 'po_testimonial' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'testimonial_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-editor-quote',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'testimonials', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_testimonials', 0 );


/* FOOTER COLUMN TAXONOMY
------------------------------------------------------- */  

function po_footer_column_cat() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Footer Column Categories', 'footer column general name', 'po_footer_column_cat' ),
		'singular_name'     => _x( 'Footer Column Category', 'footer column singular name', 'po_footer_column_cat' ),
		'search_items'      => __( 'Search Footer Column Categories', 'po_footer_column_cat' ),
		'all_items'         => __( 'All Footer Column Categories', 'po_footer_column_cat' ),
		'parent_item'       => __( 'Parent Footer Column Category', 'po_footer_column_cat' ),
		'parent_item_colon' => __( 'Parent Footer Column Category:', 'po_footer_column_cat' ),
		'edit_item'         => __( 'Edit Footer Column Columnonial Category', 'po_footer_column_cat' ),
		'update_item'       => __( 'Update Footer Column Category', 'po_footer_column_cat' ),
		'add_new_item'      => __( 'Add New Footer Column Category', 'po_footer_column_cat' ),
		'new_item_name'     => __( 'New Footer Column Category Name', 'po_footer_column_cat' ),
		'menu_name'         => __( 'Footer Column Categories', 'po_footer_column_cat' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'footer-column-category' ),
	);
	
	// create a new taxonomy
	register_taxonomy( 'footer_column_categories', array( 'footer_column' ), $args );
}
add_action( 'init', 'po_footer_column_cat' );



/* FOOTER COLUMN POST TYPE
------------------------------------------------------- */  

function po_footer_column() {

	$labels = array(
		'name'                => _x( 'Footer Columns', 'Post Type General Name', 'po_footer_column' ),
		'singular_name'       => _x( 'Footer Column', 'Post Type Singular Name', 'po_footer_column' ),
		'menu_name'           => __( 'Footer Columns', 'po_footer_column' ),
		'parent_item_colon'   => __( 'Parent Footer Column:', 'po_footer_column' ),
		'all_items'           => __( 'Footer Columns', 'po_footer_column' ),
		'view_item'           => __( 'View Footer Column', 'po_footer_column' ),
		'add_new_item'        => __( 'Add New', 'po_footer_column' ),
		'add_new'             => __( 'Add New', 'po_footer_column' ),
		'edit_item'           => __( 'Edit Footer Column', 'po_footer_column' ),
		'update_item'         => __( 'Update Footer Column', 'po_footer_column' ),
		'search_items'        => __( 'Search Footer Columns', 'po_footer_column' ),
		'not_found'           => __( 'No Footer Column found', 'po_footer_column' ),
		'not_found_in_trash'  => __( 'No Footer Column found in Trash', 'po_footer_column' ),
	);
	$args = array(
		'label'               => __( 'Footer Columns', 'po_footer_column' ),
		'description'         => __( 'Footer Columns', 'po_footer_column' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'footer_column_categories' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 27,
		'menu_icon'           => 'dashicons-align-center',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'footer_column', $args );

}

// Hook into the 'init' action
add_action( 'init', 'po_footer_column', 0 );


?>