<?php

/* PO-SLIDER SHORTCODES
------------------------------------------------------- */  

function po_slider_details( $atts , $content = null ) {
global $slider_mb;
$slider_mb->the_meta();
	extract( shortcode_atts(
		array(
			'animation' => 'fade-in',  
			'detailsdelay' => '1500', 
			'logo' => '', 
			'logodelay' => '1500', 
			'logoanimation' => '', 
		), $atts )
	);

	$output="";
	
	if ( 'show' == $slider_mb->get_the_value( 'slider_logo' ) ) {
	
		$output .= '<div class="po-slider-logo">';
		$output .= '<a href="'.get_theme_mod( 'logo_url').'"><img class="logo center-block po-'.$logoanimation.'" src="'.$logo.'" alt="'.__('Logo', 'pixelobject').'"></a>';
		$output .= '</div>';
		$output .= '<div class="po-slider-details" data-details-animation="po-'.$animation.'" data-details-delay="'.$detailsdelay.'">';
		$output .= '<div class="container po-slider-buttons">';
		$output .= '<div class="row">';
		$output .= do_shortcode($content);
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
	
	}
	
	else {
		$output .= '<div class="po-slider-details" data-details-animation="po-'.$animation.'" data-details-delay="'.$detailsdelay.'">';
		$output .= '<div class="container po-slider-buttons">';
		$output .= '<div class="row">';
		$output .= do_shortcode($content);
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';
	}
	
	return $output;

}
add_shortcode( 'slider_details', 'po_slider_details' );

function po_slider_details_nologo( $atts , $content = null ) {

	extract( shortcode_atts(
		array(
			'animation' => 'fade-in',  
			'detailsdelay' => '1500', 
			'logo' => '', 
			'logoDelay' => '1500', 
			'logoanimation' => '', 
		), $atts )
	);

	$output="";
	$output .= '<div class="po-slider-details" data-details-animation="po-'.$animation.'" data-details-delay="'.$detailsdelay.'">';
	$output .= '<div class="container po-slider-buttons">';
	$output .= '<div class="row">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	
	return $output;

}
add_shortcode( 'slider_details_nologo', 'po_slider_details_nologo' );

function po_slider_column( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'width' => '3',  
			'offset' => '',
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="col-md-'.$width.' col-sm-offset-'.$offset.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;
	
}
add_shortcode('slider_column', 'po_slider_column');

function po_slider_button( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'type' => '',
			'style' => 'outline',
			'color' => 'white',
			'size' => 'lg',
			'link' => '#',
		), $atts )
	);
	
	$output="";
	
	if ($type == "anchor") {
		
		$output .= '<button type="button" id="learn-more-button" class="slider-btn po-slider-anchor btn '.$style.'-button '.$color.' btn-'.$size.' btn-block btn-icon-ani center-block">';
		$output .= do_shortcode($content);
		$output .= '</button>';
	
	} 
	
	else if ($type == "anchor-video") {
		
		$output .= '<button type="button" id="learn-more-button" class="po-slider-anchor btn '.$style.'-button '.$color.' btn-'.$size.' btn-block btn-icon-ani center-block" style="width:100%">';
		$output .= do_shortcode($content);
		$output .= '</button>';
	
	} 
	
	else if ($type == "video") {
		
		$output .= '<button class="slider-btn-video btn '.$style.'-button '.$color.' btn-'.$size.' btn-block btn-icon-ani center-block" data-toggle="modal" data-target="#myModal">';
		$output .= do_shortcode($content);
		$output .= '</button>';
	
	}
	
	else if ($type == "video-noanchor") {
		
		$output .= '<button class="slider-btn-video-noanchor btn '.$style.'-button '.$color.' btn-'.$size.' btn-block btn-icon-ani center-block" data-toggle="modal" data-target="#myModal">';
		$output .= do_shortcode($content);
		$output .= '</button>';
	
	}
	
	else {
		$output = '<a href="'.$link.'" type="button" class="btn '.$style.'-button '.$color.' btn-'.$size.' btn-block">';
		$output .= do_shortcode($content);
		$output .= '</a>';
	}
	
	return $output;
	
}
add_shortcode('slider_button', 'po_slider_button');

function po_slider_text( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'delay' => '2500',
			'animation' => 'fade-in',
			'type' => '',
		), $atts )
	);
	
	$output="";
	
	if ($type=="static"){
		$output .= '<div class="po-slider-text-container-static" data-text-delay="'.$delay.'">';
		$output .= '<div class="po-slider-text-static center-block" data-text-animation="po-'.$animation.'">';
		$output .= do_shortcode($content);
		$output .= '</div>';
		$output .= '</div>';
	}
	else {
		$output .= '<div class="po-slider-text-container" data-text-delay="'.$delay.'">';
		$output .= '<div class="po-slider-text center-block" data-text-animation="po-'.$animation.'">';
		$output .= do_shortcode($content);
		$output .= '</div>';
		$output .= '</div>';
	}
	
	return $output;
	
}
add_shortcode('slider_text', 'po_slider_text');


function po_slider_title($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'columnwidth' => '2',
			'number' => '6',
			'order' => 'DESC',
			'orderby' => 'date',
		), $atts ) 
	);
	
	$args = array( 'post_type' => 'slider_titles', 'slider_title_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post;
	$terms = wp_get_object_terms( $post->ID, 'slider_title_categories');
	?>
    
    <h1 class="po-slider-header text-center"><?php __(the_title()); ?></h1>
                     
	<?php endwhile; ?>
    <?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('slider_titles', 'po_slider_title');

function po_slider_gallery( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'shade' => 'green',
			'type' => '',
		), $atts )
	);
	
	$output="";
	
	if ($type == 'static') {
		$output .= '<div class="po-slider">';
		$output .= '<div class="po-slider-load" id="po-slider-load"></div>';
		$output .= '<div class="load-block"></div>';
		$output .= '<div class="background-greyscale '.$shade.'"></div>';
		$output .= '<ul class="po-slider-loop">';
		$output .= do_shortcode($content);
		$output .= '</ul>';
		$output .= '</div>';
	}
	else
	{
		$output .= '<div class="po-slider">';
		$output .= '<div class="po-slider-load" id="po-slider-load"></div>';
		$output .= '<div class="load-block"></div>';
		$output .= '<div class="background-greyscale '.$shade.'"></div>';
		$output .= '<ul class="po-slider-loop">';
		$output .= do_shortcode($content);
		$output .= '</ul>';
		$output .= '</div>';
	}
	
	return $output;
	
}
add_shortcode('slider_gallery', 'po_slider_gallery');

function po_slider_gallery_no_controls( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'shade' => 'green',
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="po-slider">';
	$output .= '<div class="po-slider-load" id="po-slider-load"></div>';
	$output .= '<div class="load-block"></div>';
	$output .= '<div class="background-greyscale '.$shade.'"></div>';
	$output .= '<ul class="po-slider-loop-no-control">';
	$output .= do_shortcode($content);
	$output .= '</ul>';
	$output .= '</div>';
	
	return $output;
	
}
add_shortcode('slider_gallery_no_controls', 'po_slider_gallery_no_controls');

function po_slider_images($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'number' => '10',
			'category' => '',
			'order' => 'DESC',
			'orderby' => 'date',
		), $atts ) 
	);
	
	$args = array( 'post_type' => 'gallery', 'gallery_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<li><?php the_post_thumbnail('full'); ?></li>
    <?php endwhile;
	
	wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('slider_images', 'po_slider_images');

function po_nav_bar($atts) {
	
	
	extract(shortcode_atts( 
		array (
			'slider' => 'display',
		), $atts ) 
	);
    
    if( get_theme_mod( 'nav_text_custom' ) == '1') { 
			
    ob_start();
	?>
    
	<div class="to-top"></div>
	<div class="po-slider-<?php echo $slider; ?> po-nav po-nav-slide">
    	
		<nav class="navbar po-navbar custom-color-nav-text po-navbar-slide" role="navigation" style="margin:0px;">
		  <!-- Mobile display -->
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		  </div>
		 
		  <!-- Collect the nav links for toggling -->
		  <?php // Loading WordPress Custom Menu
		  if (has_nav_menu('header-menu')) {
			 wp_nav_menu( array(
				'theme_location'  => 'header-menu',
				'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
				'menu_class'      => 'nav navbar-nav',
				'walker'          => new po_bootstrap_walker_menu()
			) );
		  }
		  ?>
		</nav><?php
	
	wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	} else {
	
	ob_start();
	?>
    
	<div class="to-top"></div>
	<div class="po-slider-<?php echo $slider; ?> po-nav po-nav-slide">
    	
		<nav class="navbar po-navbar po-navbar-slide" role="navigation" style="margin:0px;">
		  <!-- Mobile display -->
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		  </div>
		 
		  <!-- Collect the nav links for toggling -->
		  <?php // Loading WordPress Custom Menu
		  if (has_nav_menu('header-menu')) {
			 wp_nav_menu( array(
				'theme_location'  => 'header-menu',
				'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
				'menu_class'      => 'nav navbar-nav',
				'walker'          => new po_bootstrap_walker_menu()
			) );
		  }
		  ?>
		</nav><?php
	
	wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
	}
}
add_shortcode('nav_bar', 'po_nav_bar');


function po_nav_bar_noslide($atts) {
	
	
	
	extract(shortcode_atts( 
		array (
			'slider' => 'display',
		), $atts ) 
	);
    
    if( get_theme_mod( 'nav_text_custom' ) == '1') { 
    
    
    ob_start();
	?>
    
	<div class="to-top"></div>
	<div class="po-nav nav-fixed-padding">
    
    	
		<nav class="navbar po-navbar custom-color-nav-text navbar-fixed-top" role="navigation" style="margin:0px;">
		  <!-- Mobile display -->
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		  </div>
		 
		  <!-- Collect the nav links for toggling -->
		  <?php // Loading WordPress Custom Menu
		  if (has_nav_menu('header-menu')) {
			 wp_nav_menu( array(
				'theme_location'  => 'header-menu',
				'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
				'menu_class'      => 'nav navbar-nav',
				'walker'          => new po_bootstrap_walker_menu()
			) );
		  }
		  ?>
		</nav><?php
	
	wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	} else {
	
		 ob_start();
	?>
    
	<div class="to-top"></div>
	<div class="po-nav nav-fixed-padding">
    
    	
		<nav class="navbar po-navbar navbar-fixed-top" role="navigation" style="margin:0px;">
		  <!-- Mobile display -->
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		  </div>
		 
		  <!-- Collect the nav links for toggling -->
		  <?php // Loading WordPress Custom Menu
		  if (has_nav_menu('header-menu')) {
			 wp_nav_menu( array(
				'theme_location'  => 'header-menu',
				'container_class' => 'collapse navbar-collapse navbar-ex1-collapse',
				'menu_class'      => 'nav navbar-nav',
				'walker'          => new po_bootstrap_walker_menu()
			) );
		  }
		  ?>
		</nav><?php
	
	wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
	}
}
add_shortcode('nav_bar_noslide', 'po_nav_bar_noslide');



/* SECTION SHORTCODE
------------------------------------------------------- */

function po_section( $atts, $content = null ) {
	
	extract( shortcode_atts(
		array(
			'width' => '',
			'background' => 'white',
			'backgroundcolor' => '',
			'paddingtop' => '50',
			'paddingbottom' => '50',
			'border' => '',
			'imageurl' => '',
			'videourl' => '',
			'overlay' => '',
			'overlaycolor' => '',
			'ratio' => '0.5',
		), $atts )
	);
	
	$output="";
	
	if ($width == "full") {
		if ($background == "image-parallax") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		} 
		
		else if ($background == "image-fixed") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		}
		
		else if ($background == "image") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); background-size:cover;">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); background-size:cover; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		}
		
		else if ($background == "video") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-video" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="section-video-content"> ';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '<video autoplay loop class="po-background-video">';
			$output .= '<source src="'.$videourl.'.webmhd.webm" type="video/webm">';
			$output .= '<source src="'.$videourl.'.mp4" type="video/mp4">';
			$output .= '<source src="'.$videourl.'.ogv" type="video/ogg">';
			$output .= '</video></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			
		}
		
		else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="section-background-'.$background.' section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background-color:'.$backgroundcolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
		}
	} 
	else if ($width == "sidebar") {
		if ($background == "image-parallax") {
			if ($overlaycolor == "") {
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
			else {
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
		} 
		
		else if ($background == "image-fixed") {
			if ($overlaycolor == "") {
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
			else {
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
		}
		
		else if ($background == "image") {
			if ($overlaycolor == "") {
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); background-size:cover;">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
			else {
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); background-size:cover; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			}
		}
		
		else if ($background == "video") {
			$output .= '<div class="po-section section-video" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="section-video-content"> ';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
			$output .= '<video autoplay loop class="po-background-video">';
			$output .= '<source src="'.$videourl.'.webmhd.webm" type="video/webm">';
			$output .= '<source src="'.$videourl.'.mp4" type="video/mp4">';
			$output .= '<source src="'.$videourl.'.ogv" type="video/ogg">';
			$output .= '</video></div>';
			
		}
		
		else {
			$output .= '<div class="section-background-'.$background.' section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background-color:'.$backgroundcolor.';">';
			$output .= '<div class="row po-full-width">';
			$output .= do_shortcode($content);
			$output .= '</div></div>';
		}
	}
	else {
		if ($background == "image-parallax") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-parallax section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';" data-stellar-background-ratio="'.$ratio.'">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		} 
		
		else if ($background == "image-fixed") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image-fixed section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';	
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		}
		
		else if ($background == "image") {
			if ($overlaycolor == "") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.');">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
			else {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-image section-background-image section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background:url('.$imageurl.'); background-size:cover; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
			}
		}
		
		else if ($background == "video") {
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="po-section section-video" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; -webkit-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; -moz-box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.'; box-shadow: inset 0px 0px 1000px 500px '.$overlaycolor.';">';
			$output .= '<div class="section-video-content"> ';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);
			$output .= '</div></div></div>';
			$output .= '<video autoplay loop class="po-background-video">';
			$output .= '<source src="'.$videourl.'.webmhd.webm" type="video/webm">';
			$output .= '<source src="'.$videourl.'.mp4" type="video/mp4">';
			$output .= '<source src="'.$videourl.'.ogv" type="video/ogg">';
			$output .= '</video></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
		}
		
		else {
			
			$output .= '</div></div></div></div><div class="entry-content">';
			$output .= '<div class="section-background-'.$background.' section-'.$border.'" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px; background-color:'.$backgroundcolor.';">';
			$output .= '<div class="container">';
			$output .= '<div class="row">';
			$output .= do_shortcode($content);#
			$output .= '</div></div></div>';
			$output .= '</div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">';
		}
		
	}
	
	return $output;
	
}
add_shortcode('section', 'po_section');

/* SECTION NEST LEVEL 1, 2, 3 SHORTCODES
------------------------------------------------------- */

add_shortcode('section1', 'po_section');
add_shortcode('section2', 'po_section');


/* COLUMN SHORTCODE
------------------------------------------------------- */  

function po_column( $atts , $content = null ) {
	
	extract( shortcode_atts(
		array(  
			'width' => '4', 
			'offset' => '0', 
			'paddingtop' => '', 
			'paddingbottom' => '', 
			'paddingside' => '',
			'delay' => '', 
			'animation' => 'fade-in', 
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="po-column col-sm-'.$width.' col-sm-offset-'.$offset.'" style="padding-top:'.$paddingtop.'px; padding-left:'.$paddingside.'px; padding-right:'.$paddingside.'px; padding-bottom:'.$paddingbottom.'px;" data-delay="'.$delay.'" data-animation="'.$animation.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;

}
add_shortcode( 'column', 'po_column' );

function po_column4( $atts , $content = null ) {
	
	extract( shortcode_atts(
		array(  
			'width' => '3', 
			'offset' => '0', 
			'paddingtop' => '', 
			'paddingbottom' => '', 
			'paddingside' => '',
			'delay' => '', 
			'animation' => 'fade-in', 
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="column4 po-column col-sm-'.$width.' col-sm-offset-'.$offset.'" style="padding-top:'.$paddingtop.'px; padding-left:'.$paddingside.'px; padding-right:'.$paddingside.'px; padding-bottom:'.$paddingbottom.'px;" data-delay="'.$delay.'" data-animation="'.$animation.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;

}
add_shortcode( 'column4', 'po_column4' );

function po_column5( $atts , $content = null ) {
	
	extract( shortcode_atts(
		array(  
			'width' => '3', 
			'offset' => '0', 
			'paddingtop' => '', 
			'paddingbottom' => '', 
			'paddingside' => '',
			'delay' => '', 
			'animation' => 'fade-in', 
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="column5 po-column col-sm-'.$width.' col-sm-offset-'.$offset.'" style="width: 20%; padding-top:'.$paddingtop.'px; padding-left:'.$paddingside.'px; padding-right:'.$paddingside.'px; padding-bottom:'.$paddingbottom.'px;" data-delay="'.$delay.'" data-animation="'.$animation.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;

}
add_shortcode( 'column5', 'po_column5' );


/* NESTED COLUMNS
------------------------------------------------------- */  

function po_column_nested( $atts , $content = null ) {
	
	extract( shortcode_atts(
		array(  
			'width' => '4', 
			'offset' => '0', 
			'paddingtop' => '', 
			'paddingbottom' => '', 
			'paddingside' => '',
			'delay' => '', 
			'animation' => 'fade-in', 
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="po-column col-md-'.$width.' col-md-offset-'.$offset.'" style="padding-top:'.$paddingtop.'px; padding-left:'.$paddingside.'px; padding-right:'.$paddingside.'px; padding-bottom:'.$paddingbottom.'px;" data-delay="'.$delay.'" data-animation="'.$animation.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;

}
add_shortcode( 'column1', 'po_column_nested' );
add_shortcode( 'column2', 'po_column_nested' );


/*  ROW SEPARATOR
------------------------------------------------------- */  

function po_row_separate( $atts ) {
	
	extract( shortcode_atts(
		array(  
			'width' => '4'
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="clearfix hidden-xs"></div>';
	
	return $output;

}
add_shortcode( 'newrow', 'po_row_separate' );


/* HEADER SHORTCODE
------------------------------------------------------- */  

function po_header( $atts, $content = null ) {
	
	extract( shortcode_atts(
		array(
			'type' => '',
			'icon' => '',
			'title' => '',
			'paddingtop' => '',
			'paddingbottom' => '',
		), $atts )
	);
	
	$output="";
	
	if ($type == "icon") {
		$output .= '<div class="po-column po-header col-sm-12" data-delay="0" data-animation="pull-up">';
		$output .= '<div class="icon-boxless icon-boxless-green"><i class="fa '.$icon.' fa-4x"></i></div>';
		$output .= '</div>';
		$output .= '<div class="po-column po-header col-sm-12" data-delay="0" data-animation="fade-in">  ';
		$output .= '<h1 class="text-center">'.$title.'</h1>';
		$output .= '</div>';
		$output .= '<div class="po-column po-header-small col-sm-6 col-md-offset-3" style="margin-bottom:'.$paddingbottom.'px;" data-delay="800" data-animation="fade-in">';
		$output .= '<h4 class="text-center">';
		$output .= do_shortcode($content);
		$output .= '</h4></div>';
		$output .= '<div class="clearfix"></div>';
	} 
	
	else if ($type == "left") {
		$output .= '<div class="po-column po-header-top col-sm-12" data-delay="0" data-animation="fade-in" style="padding:0;">';
		$output .= '<h3>';
		$output .= do_shortcode($content);
		$output .= '</h3>';
		$output .= '<div class="header-line-left" style="margin-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px;"></div>';
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
	}
	
	else if ($type == "line-small") {
		$output .= '<div class="po-column po-header col-sm-12" data-delay="0" data-animation="fade-in">';
		$output .= '<h4>';
		$output .= do_shortcode($content);
		$output .= '</h4></div>';
		$output .= '<div class="po-column po-header-line-thin col-sm-12 col-md-offset-0" style="margin-bottom:'.$paddingbottom.'px;" data-delay="0" data-animation="fade-in">';
		$output .= '<div style="width:100%; border-top:2px solid #0C9;"></div>';
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
	}
	
	else if ($type == "large") {
		$output .= '<div class="po-column po-header-top col-sm-12" data-delay="0" data-animation="fade-in" style="padding:0;">';
		$output .= '<h1 class="header-large text-center">';
		$output .= do_shortcode($content);
		$output .= '</h1>';
		$output .= '<div class="header-line" style="margin-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px;"></div>';
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
	}
	
	else if ($type == "small") {
		$output .= '<div class="po-column po-header-top header-small-padding col-sm-12" style="padding:'.$paddingtop.'px 0 '.$paddingbottom.'px;" data-delay="0" data-animation="fade-in">';
		$output .= '<h6 class="header-small text-center">';
		$output .= do_shortcode($content);
		$output .= '</h6>';
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
	}
	
	else if ($type == "small-left") {
		$output .= '<div class="po-column po-header-top col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
		$output .= '<div class="header-small-padding" style="padding-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px;">';
		$output .= '<h6 class="header-small">';
		$output .= do_shortcode($content);
		$output .= '</h6>';
		$output .= '</div></div>';
		$output .= '<div class="clearfix"></div>';
	}
	
	else {
		$output .= '<div class="po-column po-header-top col-sm-12" data-delay="0" data-animation="fade-in" style="padding:0;">';
		$output .= '<h3 class="text-center">';
		$output .= do_shortcode($content);
		$output .= '</h3>';
		$output .= '<div class="header-line" style="margin-top:'.$paddingtop.'px; padding-bottom:'.$paddingbottom.'px;"></div>';
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
		
	}
	
	return $output;
	
}
add_shortcode('header', 'po_header');


/* TEXT BLOCK SHORTCODE
------------------------------------------------------- */  

function po_text( $atts, $content = null ) {
	
	extract( shortcode_atts(
		array(
			'align' => 'left',
			'size' => '',
			'weight' => '',
			'paddingtop' => '0',
			'paddingbottom' => '0',
			'paddingside' => '0',
		), $atts )
	);
	
	$output="";
	
	if ($align == "left") {
		$output .= '<p class="textblock" style="text-align:'.$align.'; padding-top:'.$paddingtop.'px; padding-right:10px; padding-bottom:'.$paddingbottom.'px; font-size: '.$size.'px; font-weight: '.$weight.';">';
		$output .= do_shortcode($content);
		$output .= '</p>';
	} 
	else if ($align == "right"){
		$output .= '<p class="textblock" style="text-align:'.$align.'; padding-top:'.$paddingtop.'px; padding-left:10px; padding-bottom:'.$paddingbottom.'px; font-size: '.$size.'px; font-weight: '.$weight.';">';
		$output .= do_shortcode($content);
		$output .= '</p>';
	} 
	else {
		$output .= '<p class="textblock" style="text-align:'.$align.'; padding-top:'.$paddingtop.'px; padding-left:5px; padding-right:5px; padding-bottom:'.$paddingbottom.'px; font-size: '.$size.'px; font-weight: '.$weight.';">';
		$output .= do_shortcode($content);
		$output .= '</p>';
	}
	
	
	return $output;
	
}
add_shortcode('text', 'po_text');


/* MEDIA SHORTCODE
------------------------------------------------------- */  

function po_media( $atts ) {
	
	extract( shortcode_atts(
		array(
			'thumburl' => '',   
			'imageurl' => '',  
			'type' => '',
			'height' => '200',
			'group' => '',
			'imagealt' => '',
		), $atts )
	);
	
	$output="";
	
	if ($type == "image") {
		if ($thumburl == ''){
			$output .= '<div class="po-column col-sm-12" data-delay="0" data-animation="fade-in" style="padding:0;" >';
			$output .= '<div class="grid cs-style-2 media-container" style="height:'.$height.'px;">';
			$output .= '<a class="view" href="'.$imageurl.'" rel="'.$group.'">';
			$output .= '<figure>';
			$output .= '<img class="img-responsive" src="'.$imageurl.'" alt="'.$imagealt.'" />';
			$output .= '<figcaption style="height:'.$height.'px;"></figcaption>';
			$output .= '</figure>';
			$output .= '</a>';
			$output .= '</div></div>';
			$output .= '<div class="clearfix"></div>';
		} else {
			$output .= '<div class="po-column col-sm-12" data-delay="0" data-animation="fade-in" style="padding:0;" >';
			$output .= '<div class="grid cs-style-2 media-container" style="height:'.$height.'px;">';
			$output .= '<a class="view" href="'.$imageurl.'" rel="'.$group.'">';
			$output .= '<figure>';
			$output .= '<img class="img-responsive" src="'.$thumburl.'" alt="'.$imagealt.'" />';
			$output .= '<figcaption style="height:'.$height.'px;"></figcaption>';
			$output .= '</figure>';
			$output .= '</a>';
			$output .= '</div></div>';
			$output .= '<div class="clearfix"></div>';
		}
	} 
	else if ($type == "video"){
		$output .= '</p>';
	} 
	else {
		$output .= '</p>';
	}
	
	
	return $output;
	
}
add_shortcode('media', 'po_media');


/* BUTTON SHORTCODES
------------------------------------------------------- */  

function po_button( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'type' => '',
			'style' => '',
			'width' => '',
			'color' => 'light',
			'icon' => 'fa-check',
			'size' => 'lg',
			'link' => '#',
			'position' => '',
		), $atts )
	);
	
	$output="";
	
		if ($style == "banner") {
			if ($position == "left") {
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="banner-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px;">';
				$output .= '<div class="btn-icon"><i class="fa '.$icon.' fa-lg"></i></div>';
				$output .= '<span>';
				$output .= do_shortcode($content);
				$output .= '</span>';
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
			else 
			{
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="banner-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px; margin:0 auto;">';
				$output .= '<div class="btn-icon"><i class="fa '.$icon.' fa-lg"></i></div>';
				$output .= '<span>';
				$output .= do_shortcode($content);
				$output .= '</span>';
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
		} else if ($style == "top") {
			if ($position == "left") {
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="btn outline-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px;">';
				$output .= '<div class="btn-icon"><i class="fa '.$icon.'"></i></div>';
				$output .= '<span>';
				$output .= do_shortcode($content);
				$output .= '</span>';
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
			else 
			{
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="btn outline-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px; margin:0 auto;">';
				$output .= '<div class="btn-icon"><i class="fa '.$icon.'"></i></div>';
				$output .= '<span>';
				$output .= do_shortcode($content);
				$output .= '</span>';
				$output .= '</a>';
				$output .= '</div></div></div>';
			}	
				
		} else if ($style == "left") {
			if ($position == "left") {
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="btn outline-button button-icon-left-manual '.$color.' btn-lg btn-block" style="width:'.$width.'px;">';
				$output .= '<span class="btn-icon-left"><i class="fa '.$icon.'"></i></span>';
				$output .= do_shortcode($content);
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
			else 
			{
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="btn outline-button button-icon-left-manual '.$color.' btn-lg btn-block" style="width:'.$width.'px; margin:0 auto;">';
				$output .= '<span class="btn-icon-left"><i class="fa '.$icon.'"></i></span>';
				$output .= do_shortcode($content);
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
		} else {
			if ($position == "left") {
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'" class="btn outline-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px;">';
				$output .= do_shortcode($content);
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
			else 
			{
				$output .= '<div style="padding:0">';
				$output .= '<div class="row po-full-width">';
				$output .= '<div class="po-column col-sm-12" style="padding:0;" data-delay="0" data-animation="fade-in">';
				$output .= '<a href="'.$link.'"  class="btn outline-button '.$color.' btn-'.$size.' btn-block btn-icon-ani" style="width:'.$width.'px; margin:0 auto;">';
				$output .= do_shortcode($content);
				$output .= '</a>';
				$output .= '</div></div></div>';
			}
		}
	
	return $output;
	
}
add_shortcode('button', 'po_button');



/* ICON BOX SHORTCODES
------------------------------------------------------- */  

function po_iconbox( $atts, $content = null ) {
	
	extract(shortcode_atts(
		array(
			'type' => '',
			'character' => '',
			'icon' => '',
			'title' => '',
			'paddingtop' => '',
			'paddingbottom' => '',
			'titlesize' => '',
			'titleweight' => '',
			'size' => '',
			'weight' => '',
		), $atts )
	);
	
	$output="";
	
	if ($type == "icon-float") {
		if ($character != "") {
			$output .= '<div class="po-icon-float icon-green hover-ani">';
			$output .= '<div class="icon-float icon-float-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<div class="icon-bg">';
			$output .= '<div class="float-header text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</div>';
			$output .= '<div class="text-center float-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</div>';
			$output .= '</div></div>';
		} else {
			$output .= '<div class="po-icon-float icon-green hover-ani">';
			$output .= '<div class="icon-float icon-float-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '<div class="icon-bg">';
			$output .= '<div class="float-header text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</div>';
			$output .= '<div class="text-center float-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</div>';
			$output .= '</div></div>';
		}
	}
	
	else if ($type == "icon-box-left") {
		if ($character != "") {
			$output .= '<div class="po-icon-box-left icon-green hover-ani">';
			$output .= '<div class="icon-section-left">';
			$output .= '<div class="icon-box-side icon-green">';
			$output .= '<div class="icon-char">'.$character.'</div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<div class="column-content-left">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-box-left icon-green hover-ani">';
			$output .= '<div class="icon-section-left">';
			$output .= '<div class="icon-box-side icon-green">';
			$output .= '<i class="fa '.$icon.' fa-2x"></i>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<div class="column-content-left">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-top") {
		if ($character != "") {
			$output .= '<div class="po-icon-boxless icon-green hover-ani">';
			$output .= '<div class="icon-boxless icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-boxless icon-green hover-ani">';
			$output .= '<div class="icon-boxless icon-boxless-green"><i class="fa '.$icon.' fa-4x"></i></div>';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-title") {
		if ($character != "") {
			$output .= '<div class="po-icon-small icon-green hover-ani">';
			$output .= '<div class="icon-section-left-small">';
			$output .= '<div class="po-icon-title icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '</div>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-small icon-green hover-ani">';
			$output .= '<div class="icon-section-left-small">';
			$output .= '<div class="po-icon-title icon-boxless-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '</div>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-left") {
		if ($character != "") {
			$output .= '<div class="po-icon-left icon-green hover-ani">';
			$output .= '<table><tbody><tr><td class="icon-td">';
			$output .= '<div class="po-icon icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '</td><td>';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</td></tr></tbody></table></div>';
		} else {
			$output .= '<div class="po-icon-left icon-green hover-ani">';
			$output .= '<table><tbody><tr><td class="icon-td">';
			$output .= '<div class="po-icon icon-boxless-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '</td><td>';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</td></tr></tbody></table></div>';
		}
	}
	
	else {
		if ($character != "") {
			$output .= '<div class="po-icon-box icon-green hover-ani">';
			$output .= '<div class="icon-box icon-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<div class="column-content">';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<div class="row">';
			$output .= '<div class="col-sm-6 col-sm-offset-3">';
			$output .= '<div class="icon-box-line"></div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
		} else {
			$output = '<div class="po-icon-box icon-green hover-ani">';
			$output .= '<div class="icon-box icon-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '<div class="column-content">';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<div class="row">';
			$output .= '<div class="col-sm-6 col-sm-offset-3">';
			$output .= '<div class="icon-box-line"></div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
		}
	}
	
	return $output;
	
}
add_shortcode('iconbox', 'po_iconbox');


// IconBox sans les lignes du dessous pour la page d'acceuil

function po_iconbox_sic( $atts, $content = null ) {

	extract(shortcode_atts(
		array(
			'type' => '',
			'character' => '',
			'icon' => '',
			'title' => '',
			'paddingtop' => '',
			'paddingbottom' => '',
			'titlesize' => '',
			'titleweight' => '',
			'size' => '',
			'weight' => '',
		), $atts )
	);

	$iconShort = substr($icon, 0, 6);
	
	$output="";
	
	if ($type == "icon-float") {
		if ($character != "") {
			$output .= '<div class="po-icon-float icon-green hover-ani">';
			$output .= '<div class="icon-float icon-float-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<div class="icon-bg">';
			$output .= '<div class="float-header text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</div>';
			$output .= '<div class="text-center float-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</div>';
			$output .= '</div></div>';
		} else {
			$output .= '<div class="po-icon-float icon-green hover-ani">';
			$output .= '<div class="icon-float icon-float-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '<div class="icon-bg">';
			$output .= '<div class="float-header text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</div>';
			$output .= '<div class="text-center float-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</div>';
			$output .= '</div></div>';
		}
	}
	
	else if ($type == "icon-box-left") {
		if ($character != "") {
			$output .= '<div class="po-icon-box-left icon-green hover-ani">';
			$output .= '<div class="icon-section-left">';
			$output .= '<div class="icon-box-side icon-green">';
			$output .= '<div class="icon-char">'.$character.'</div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<div class="column-content-left">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-box-left icon-green hover-ani">';
			$output .= '<div class="icon-section-left">';
			$output .= '<div class="icon-box-side icon-green">';
			$output .= '<i class="fa '.$icon.' fa-2x"></i>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<div class="column-content-left">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-top") {
		if ($character != "") {
			$output .= '<div class="po-icon-boxless icon-green hover-ani">';
			$output .= '<div class="icon-boxless icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-boxless icon-green hover-ani">';
			$output .= '<div class="icon-boxless icon-boxless-green"><i class="fa '.$icon.' fa-4x"></i></div>';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-title") {
		if ($character != "") {
			$output .= '<div class="po-icon-small icon-green hover-ani">';
			$output .= '<div class="icon-section-left-small">';
			$output .= '<div class="po-icon-title icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '</div>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		} else {
			$output .= '<div class="po-icon-small icon-green hover-ani">';
			$output .= '<div class="icon-section-left-small">';
			$output .= '<div class="po-icon-title icon-boxless-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '</div>';
			$output .= '<div class="details-section-right">';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '</div>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
		}
	}
	
	else if ($type == "icon-left") {
		if ($character != "") {
			$output .= '<div class="po-icon-left icon-green hover-ani">';
			$output .= '<table><tbody><tr><td class="icon-td">';
			$output .= '<div class="po-icon icon-boxless-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '</td><td>';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</td></tr></tbody></table></div>';
		} else {
			$output .= '<div class="po-icon-left icon-green hover-ani">';
			$output .= '<table><tbody><tr><td class="icon-td">';
			$output .= '<div class="po-icon icon-boxless-green"><i class="fa '.$icon.' fa-2x"></i></div>';
			$output .= '</td><td>';
			$output .= '<h4 style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<p class="box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</td></tr></tbody></table></div>';
		}
	}
	
	else {
		if ($character != "") {
			$output .= '<div class="po-icon-box icon-green hover-ani">';
			$output .= '<div class="icon-box icon-green"><div class="icon-char">'.$character.'</div></div>';
			$output .= '<div class="column-content">';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<div class="row">';
			$output .= '<div class="col-sm-6 col-sm-offset-3">';
			$output .= '<div class="icon-box-line"></div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
		}
		else if ($iconShort == " icon-") {
			$output = '<div class="po-icon-box icon-green hover-ani">';
			$output .= '<div class="icon-box icon-green"><i class="'.$icon.' fa-4x"></i></div>';
			if ( $icon == " icon-viadeo") {
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">465</div>';
			}
			$output .= '<div class="column-content">';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<div class="row">';
			$output .= '<div class="col-sm-6 col-sm-offset-3">';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
		} else {
			$output = '<div id="social-bouton-rond" class="po-icon-box icon-green hover-ani">';
			$output .= '<div class="icon-box icon-green"><i class="fa '.$icon.' fa-4x"></i></div>';
			if ( $icon == " fa-facebook") {
				$json = file_get_contents('https://graph.facebook.com/Sicotterecrutement/?fields=likes');
				$obj = json_decode($json);
				$nblikefb = $obj->likes;
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">1257</div>';
			}
			if ( $icon == " fa-twitter") {
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">678</div>';
			}
			if ( $icon == " fa-youtube") {
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">5</div>';
			}
			if ( $icon == " fa-linkedin") {
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">500+</div>';
			}
			if ( $icon == " fa-pinterest") {
				$output .= '<div id="nblikefacebook" style="border: 1px solid; border-radius: 10px; background-color: #EACD27; float: right; min-width: 63px; min-height: 20px; position: absolute; font-size: 16px; text-align: center; top: 60px; left: -10px;">41</div>';
			}
			$output .= '<div class="column-content">';
			$output .= '<h4 class="text-center" style="margin-top:'.$paddingtop.'px; margin-bottom:'.$paddingbottom.'px; font-size:'.$titlesize.'px; font-weight:'.$titleweight.';">'.$title.'</h4>';
			$output .= '<div class="row">';
			$output .= '<div class="col-sm-6 col-sm-offset-3">';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<p class="text-center box-text" style="font-size:'.$size.'px; font-weight:'.$weight.';">'.do_shortcode($content).'</p>';
			$output .= '</div>';
			$output .= '</div>';
		}
	}
	
	return $output;
	
}
add_shortcode('iconboxsic', 'po_iconbox_sic');


/* COUNT SHORTCODE
------------------------------------------------------- */  

function po_count( $atts , $content = null ) {
	
	extract( shortcode_atts(
		array(  
			'from' => '',
			'to' => '',
			'speed' => '3000',  
			'icon' => '',
			'icondelay' => '',
			'iconanimation' => '',
			'textanimation' => '', 
		), $atts )
	);
	
	$output="";
	
	$output .= '<div class="po-count hover-ani"> ';
	$output .= '<div class="po-column count-icon col-sm-12" data-delay="'.$icondelay.'" data-animation="'.$iconanimation.'">';
	$output .= '<div class="icon-boxless icon-boxless-green"><i class="fa '.$icon.' fa-2x"></i></div>';
	$output .= '</div>';
	$output .= '<h1 class="po-number text-center" data-from="'.$from.'" data-to="'.$to.'" data-speed="'.$speed.'" data-refresh-interval="10"></h1>';
	$output .= '<div class="row">';
	$output .= '<div class="col-sm-6 col-sm-offset-3">';
	$output .= '<div class="count-line"></div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="po-column po-column-mobile po-header-small col-sm-12" data-delay="'.$speed.'" data-animation="'.$textanimation.'">';
	$output .= '<h6 class="count-text text-center">';
	$output .= do_shortcode($content);
	$output .= '</h6>';
	$output .= '</div></div>';
	
	return $output;

}
add_shortcode( 'count', 'po_count' );


/* PROGRESS BAR SHORTCODE
------------------------------------------------------- */  

function po_progress_bar( $atts ) {
	
	extract( shortcode_atts(
		array(  
			'type' => '',
			'title' => '',
			'icon' => '',
			'value' => '0',
			'color' => '#20C596',
		), $atts )
	);
	
	$output="";
	
	if ($type == "circular") {
		$output .= '<div class="po-column-mobile-ani" data-delay="0" data-animation="fade-in">';
		$output .= '<div class="hover-ani">';
		$output .= '<div class="circular-cont center-block">';
		$output .= '<div style="position:absolute;">';
		$output .= '<div class="icon-circular"><i class="fa '.$icon.' fa-2x po-column-mobile-ani" data-delay="0" data-animation="fade-in"></i></div>';
		$output .= '</div>';
		$output .= '<input type="text" value="0" class="dial po-column-mobile-ani" data-width="150" data-thickness=.01 data-displayInput=false data-fgColor="'.get_theme_mod( 'accent').'" data-bgColor="#ddd" data-linecap=round data-readOnly=true data-delay="0" data-animation="fade-in" data-value="'.$value.'">';
		$output .= '</div>';
		$output .= '<div class="po-column po-header-line col-sm-6 col-sm-offset-3" data-delay="0" data-animation="fade-in">';
		$output .= '<div class="circular-line"></div>';
		$output .= '</div>';
		$output .= '<div class="po-column po-column-mobile-ani po-header-small col-sm-12" data-delay="900" data-animation="pull-up">';
		$output .= '<h4 class="circular-text text-center">'.$title.'</h4></div></div></div>';
	} 
	else {
		$output .= '<div class="parent">';
		$output .= '<div class="row">';
		$output .= '<div class="col-xs-6">';
		$output .= '<h6 class="progress-title">'.$title.'</h6>';
		$output .= '</div>';
		$output .= '<div class="col-xs-6">';
		$output .= '<div class="progress-value text-right">'.$value.'%</div>';
		$output .= '</div></div>';
		$output .= '<div class="progress">';
		$output .= '<div class="progress-bar" role="progressbar" style="background-color:'.get_theme_mod( 'accent').';" data-progress="'.$value.'"></div>';
		$output .= '</div></div>';
	}
	
	return $output;

}
add_shortcode( 'progress_bar', 'po_progress_bar' );




/* PORTFOLIO SHOWCASE SHORTCODE
------------------------------------------------------- */  

function po_portfolio_showcase($atts) {
	
	
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'columnwidth' => '4',
			'number' => '6',
			'order' => 'DESC',
			'orderby' => 'date',
			'hovercolor' => '',
		), $atts ) 
	);
	
	if ($hovercolor == "none") {
	
	ob_start();
	
	$args = array( 'post_type' => 'portfolio', 'portfolio_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post, $portfolio_mb;
	$portfolio_mb->the_meta();
	$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class="cat-sep"></span> ', '' );
	
	?>
    	<div class="portfolio-item col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>">
        <a href="<?php the_permalink(); ?>">
        <div class="portfolio-details-nocolor">
                	<div class="item-description">
                    <div class="description-cell">
                        <h5 class="item-title"><?php the_title(); ?></h5>
                        <div class="subtitle"><?php echo strip_tags($terms, '<span>'); ?></div>
                    </div>
                    </div>
                </div>
                <div class="liquid-container">
                	<?php 
					if ( '' == $portfolio_mb->get_the_value('image') ) {
						the_post_thumbnail('full'); 
                    } else { ?>
					<img class="img-responsive" src="<?php echo $portfolio_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
					<?php }
					?>	
                </div>
            </a>
        </div>
		<?php endwhile; ?>
		<div class="clear-float"></div>
        	
                    <div style="width:8%; margin:30px auto 30px; border-top:1px solid #e1e1e1;"></div>
              
                	
                     <?php    
	echo do_shortcode("
	
	[button style='top' color='light' link='".get_post_type_archive_link( 'portfolio' )."' icon='fa-folder-open-o' width='200']". __('View All Projects','pixelobject')."[/button]

"); ?>
                    
                 
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
	} else {
	
		ob_start();
	
		$args = array( 'post_type' => 'portfolio', 'portfolio_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		global $post, $portfolio_mb;
		$portfolio_mb->the_meta();
		$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class="cat-sep"></span> ', '' );
		
		?>
			<div class="portfolio-item col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>">
			<a href="<?php the_permalink(); ?>">
			<div class="portfolio-details">
						<div class="item-description">
						<div class="description-cell">
							<h5 class="item-title"><?php the_title(); ?></h5>
							<div class="subtitle"><?php echo strip_tags($terms, '<span>'); ?></div>
						</div>
						</div>
					</div>
					<div class="liquid-container">
						<?php 
						if ( '' == $portfolio_mb->get_the_value('image') ) {
							the_post_thumbnail('full'); 
						} else { ?>
						<img class="img-responsive" src="<?php echo $portfolio_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
						<?php }
						?>	
					</div>
				</a>
			</div>
			<?php endwhile; ?>
			<div class="clear-float"></div>
				
						<div style="width:8%; margin:30px auto 30px; border-top:1px solid #e1e1e1;"></div>
				  
						
						 <?php    
		echo do_shortcode("
		
		[button style='top' color='light' link='".get_post_type_archive_link( 'portfolio' )."' icon='fa-folder-open-o' width='200']". __('View All Projects','pixelobject')."[/button]
	
	"); ?>
						
					 
			
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		return $output;	
	
	}
	
}
add_shortcode('portfolio_showcase', 'po_portfolio_showcase');



/* NOUVEAU PORTFOLIO POUR LA MODIFICATION DU TEMPLATE ET DU SHORTCODE QUI EST AU DESSUS*/
function slider_sicotte( $atts ) {
    extract(shortcode_atts( 
		array (
			'category' => '',
			'columnwidth' => '3',
			'number' => '8',
			'order' => 'DESC',
			'orderby' => 'date',
			'hovercolor' => '',
		), $atts ) 
	);

	ob_start();
	
		$args = array( 'post_type' => 'portfolio', 'post_in' => array( 76, 77, 79, 82, 416, 374, 410, 376 ), 'portfolio_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
		$loop = new WP_Query( $args );

		echo do_shortcode("[button style='top' color='light' link='".get_post_type_archive_link( 'portfolio' )."' icon='fa-folder-open-o' width='200']". __('Toutes les catégories','pixelobject')."[/button]");
		?>
		<div class="clear-float"></div>				
		<div style="width:8%; margin:30px auto 30px;"></div>
		<?php

		while ( $loop->have_posts() ) : $loop->the_post(); 
		global $post, $portfolio_mb;
		$portfolio_mb->the_meta();
		$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class="cat-sep"></span> ', '' );
		
		?>
			<div class="portfolio-item col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" style="border: 1px solid; border-color: rgb(217,217,217)">
			<a href="<?php the_permalink(); ?>" style="padding-bottom: 0px !important;">
			<div class="portfolio-details portfolio-details-<?php echo($post->ID) ;?>">
						<div class="item-description">
						<div class="description-cell">
							<h5 class="item-title item-title-<?php echo($post->ID) ;?>" style="text-transform: none; font-family: 'Open Sans', sans-serif, Normal; font-size: 18px; font-weight: normal; color: rgb(128,128,128);"><?php the_title(); ?></h5>
						</div>
						</div>
					</div>
					<div class="liquid-container">
						<?php 
						if ( '' == $portfolio_mb->get_the_value('image') ) {
							the_post_thumbnail('full'); 
						} else { ?>
						<img class="img-responsive" src="<?php echo $portfolio_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
						<?php }
						?>	
					</div>
				</a>
			</div>

			<?php 
			$urlbase = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$hovbackurl= "";
			$firstPart = substr($urlbase, 0, -4);
			$lastchar = substr($firstPart, -1);
					if ($lastchar == '1') {
						$m = substr($firstPart, 0, -1);
						$firstPart = $m;
					}
			$hovbackurl = $firstPart."_hover.png";
			?>

			<style type="text/css">
				.portfolio-details-<?php echo($post->ID) ;?>:hover {
					background-image: url("<?php echo $hovbackurl; ?>") !important;
					background-position: 50% 50%; 
					background-size: cover;
				}

			</style>

			<script type="text/javascript">
				$(".portfolio-details-<?php echo($post->ID) ;?>").mouseover(function(){
					$(".item-title-<?php echo($post->ID) ;?>").css("color", "rgb(240,240,240) !important");
				});
				$(".portfolio-details-<?php echo($post->ID) ;?>").mouseout(function(){
					$(".item-title-<?php echo($post->ID) ;?>").css("color", "rgb(128,128,128) !important");
				});
			</script>

		<?php endwhile; ?>


				  
						
						
					 
			
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		return $output;	
} 


add_shortcode( 'portsicotte','slider_sicotte');



/* PORTFOLIO FOOTER SHORTCODE
------------------------------------------------------- */  

function po_portfolio_footer($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'columnwidth' => '4',
			'number' => '6',
			'order' => 'DESC',
			'orderby' => 'date',
		), $atts ) 
	);
	
	
	
	$args = array( 'post_type' => 'portfolio', 'portfolio_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post, $portfolio_mb;
	$portfolio_mb->the_meta();
	$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class"cat-sep"></span> ', '' );
	
	?>
    	<div class="portfolio-item-footer col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>">
        <a class="ssss" href="<?php the_permalink(); ?>" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
        <div class="portfolio-details">
                	<div class="item-description">
                    
                    </div>
                </div>
                <div class="liquid-container-footer">
            		<?php 
					if ( '' == $portfolio_mb->get_the_value('image') ) {
						the_post_thumbnail('full'); 
                    } else { ?>
					<img class="img-responsive" src="<?php echo $portfolio_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>" />
					<?php }
					?>	
                </div>
            </a>
        </div>
		<?php endwhile; ?>
		<div class="clear-float"></div>
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('portfolio_footer', 'po_portfolio_footer');


/* PORTFOLIO DETAILS SHORTCODE
------------------------------------------------------- */  

function po_portfolio_details($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'displaylink' => '',
			'link' => '',
			'linktext' => __('Visit Website','pixelobject'),
		), $atts ) 
	);
	
	global $post;
	$terms = get_the_term_list( $post->ID, 'portfolio_categories', '', ' <span class"cat-sep"></span> ', '' );
	
	?>
    	
            <div class="portfolio-link">
            <ul>
            <li style="display:<?php echo $displaylink; ?>;"><a href="<?php echo $link; ?>" target="_blank"><?php echo $linktext; ?></a></li>
            </ul>
            </div>
            <div class="portfolio-cat">
            	<ul>
        		<?php the_terms( $post->ID, 'portfolio_categories', '<li>', '</li><li>', '</li>' ); ?>
                </ul>
             </div>
        
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('portfolio_details', 'po_portfolio_details');


/* TWEETS SHORTCODE
------------------------------------------------------- */  

function po_tweets($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'number' => '2',
		), $atts ) 
	);
	
	global $post;
	
	?>
    	
            <div class="recent-tweets">
                <ul>
                <?php
                if (function_exists( 'getTweets' )) {
                $tweets = getTweets(get_theme_mod( 'twitter_username' ), $number, array('exclude_replies' => true, 'include_rts' => false, 'include_entities' => true));
                    foreach($tweets as $tweet){
                    $tweet_text = $tweet["text"];
                    $tweet_text = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '<a href="http://$1" target="_blank">http://$1</a>', $tweet_text); //replace links
                    $tweet_text = preg_replace('/@([a-z0-9_]+)/i', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $tweet_text); //replace users
                    echo '<li>'.$tweet_text.'</li>';
                    }
                }
                ?>
                </ul>
            	<a href="http://twitter.com/<?php echo get_theme_mod( 'twitter_username'); ?>"><?php echo get_theme_mod( 'twitter_username'); ?></a>
        	</div>
        
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('tweets', 'po_tweets');


/* RECENT POSTS SHORTCODE
------------------------------------------------------- */  

function po_recent_posts($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'number' => '4',
		), $atts ) 
	);
	
	global $post;
	
	?>
    	
            <div class="recent-posts"> 
                <ul> 	
                    <?php 
                    
                        $args = array( 'numberposts' => $number );
                        $recent_posts = wp_get_recent_posts( $args );
                        foreach( $recent_posts as $recent ){
                            echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
                        }
                    
                    ?>
                 </ul>
            </div> 
        
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('recent_posts', 'po_recent_posts');



/* CAROUSEL SHORTCODE
------------------------------------------------------- */  

function po_carousel_sc($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'columnwidth' => '4',
			'number' => '3',
		), $atts ) 
	);
	
	?> 
    <div class="po-column col-sm-12" data-delay="0" data-animation="fade-in" style="padding-top:0; padding-bottom:0;" >
    <div class="po-carouseleds">
    	<!--[if !IE]><!--><ul class="po-carousel grid cs-style-1"> <!--<![endif]-->
<!--[if IE]><ul class="po-carousel-ie grid cs-style-1"> <![endif]--> <?php
        $args = array( 'post_type' => 'carousel', 'carousel_groups' => $category, 'posts_per_page' => 24, 'order' => 'DESC', 'orderby' => 'date' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); 
        global $post, $carousel_mb;
		$carousel_mb->the_meta();
        $terms = get_the_term_list( $post->ID, 'carousel_groups', '', ' ', '' ); 
        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') );
        ?>
            <li>
                
                    <a class="view" href="<?php echo $url ?>" rel="<?php echo strip_tags($terms, '<span>'); ?>">
                    <figure>
                    	<?php 
						if ( '' == $carousel_mb->get_the_value('image') ) {
							?> <img class="img-responsive" src="<?php echo $url ?>" alt="<?php the_title(); ?>" /> <?php
						} else { ?>
						<img class="img-responsive" src="<?php echo $carousel_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>" />
						<?php }
						?>	
                        
                        <figcaption>
                            <h6><?php the_title(); ?></h6>
                        </figcaption>
                     </figure>
                    </a>
               
            </li>
        <?php endwhile; ?>
        </ul>
   	</div>
    </div>
	<div class="clearfix"></div>	
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('carousel', 'po_carousel_sc');



/* GOOGLE MAPS SHORTCODE
------------------------------------------------------- */  

function po_googlemap($atts) 
{
    extract(shortcode_atts(
		array(
			"id" => 'myMap', 
			"type" => 'road', 
			"latitude" => '51.501364', 
			"longitude" => '-0.141890', 
			"zoom" => '14',
			"marker" => '', 
			"message" => __('We are here!','pixelobject'), 
			"height" => '250',
			"color" => '',
		), $atts)
	);
     
    $mapType = '';
    if($type == "satellite") 
        $mapType = "SATELLITE";
    else if($type == "terrain")
        $mapType = "TERRAIN";  
    else if($type == "hybrid")
        $mapType = "HYBRID";
    else
        $mapType = "ROADMAP";  
         
    echo '<!-- Google Map -->
        <script type="text/javascript">  
        $(document).ready(function() {
          function initializeGoogleMap() {
			  
			  var styles = [
				{
				  stylers: [
					{ hue: "'.$color.'" },
					{ saturation: -20 }
				  ]
				},{
				  featureType: "road",
				  elementType: "geometry",
				  stylers: [
					{ lightness: 100 },
					{ visibility: "simplified" }
				  ]
				},{
				  featureType: "road",
				  elementType: "labels",
				  stylers: [
					{ visibility: "simplified" }
				  ]
				}
			  ];
			  
			  var styledMap = new google.maps.StyledMapType(styles,
				{name: "Styled Map"});
			  
              var myLatlng = new google.maps.LatLng('.$latitude.','.$longitude.');
              var myOptions = {
                center: myLatlng,  
                zoom: '.$zoom.',
				scrollwheel: false,
				navigationControl: false,
				mapTypeControl: false,
				scaleControl: false,
				draggable: false,
                mapTypeId: google.maps.MapTypeId.SATELLITE
              };
              var map = new google.maps.Map(document.getElementById("'.$id.'"), myOptions);
     
              var contentString = "'.$message.'";
              var infowindow = new google.maps.InfoWindow({
                  content: contentString
              });
               
              var marker = new google.maps.Marker({
                  position: myLatlng,
				  animation:google.maps.Animation.DROP,
				  icon:"'.$marker.'"
              });
               
              google.maps.event.addListener(marker, "click", function() {
                  infowindow.open(map,marker);
              });
               
              marker.setMap(map);
			  
			  map.mapTypes.set("'.$id.'", styledMap);
 			  map.setMapTypeId("'.$id.'");
             
          }
          initializeGoogleMap();
     
        });
        </script>';
     
        return '<div id="'.$id.'" style="width:100%; height:'.$height.'px;" class="googleMap"></div>';
} 

add_shortcode('googlemaps','po_googlemap');


/* TEAM SHORTCODE
------------------------------------------------------- */  

function po_team_sc($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'number' => '20',
			'columnwidth' => '2',
			'order' => 'DESC',
			'orderby' => 'date',
		), $atts ) 
	);
	
	
	
	$args = array( 'post_type' => 'team', 'team_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post, $team_mb;
	$team_mb->the_meta();
	$terms = wp_get_object_terms( $post->ID, 'team_categories');
	
	?>
    	<div class="po-column portfolio-item col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
        <a href="<?php the_permalink(); ?>">
        <div class="portfolio-details">
                	<div class="item-description">
                    <div class="description-cell">
                        <h5 class="item-title"><?php the_title(); ?></h5>
                        <div class="subtitle"><?php echo $terms[0]->name; ?></div>
                    </div>
                    </div>
                </div>
                <div class="liquid-container">
            		<?php 
					if ( '' == $team_mb->get_the_value('image') ) {
						the_post_thumbnail('full'); 
                    } else { ?>
					<img class="img-responsive" src="<?php echo $team_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
					<?php }
					?>	
                </div>
            </a>
        </div>
		<?php endwhile; ?>
		<div class="clear-float"></div>
        
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('team', 'po_team_sc');

// Team avec photo en grand et sans description 

function po_teamsic_sc($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'number' => '20',
			'columnwidth' => '2',
			'order' => 'DESC',
			'orderby' => 'date',
		), $atts ) 
	);
	
	
	
	$args = array( 'post_type' => 'team', 'team_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post, $team_mb;
	$team_mb->the_meta();
	$terms = wp_get_object_terms( $post->ID, 'team_categories');
	$lien = "#";
	$titre = "";
	if ( $post->ID == 148 ) {
		$lien = get_permalink();
	} else {
		$titre = "À Venir";
	}
	
	?>
    	<div class="po-column portfolio-item col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
        	<a id="ateam-<?php echo($post->ID);?>" href="<?php echo $lien; ?>" title="<?php echo $titre; ?>" style="padding-bottom: 0px !important;">
        		<div class="portfolio-details">
                	<div class="item-description id-<?php echo($post->ID);?>" style="display: none;">
                    	<div class="description-cell" style="padding-bottom: 11%;">
                    		<h4 class="item-title" style="font-family: 'Open Sans', sans-serif, Semi-Bold; font-size: 18.5px; text-transform: none;"><?php the_title(); ?></h4>
                    		<div class="subtitle" style="font-family: 'Open Sans', sans-serif, Semi-Bold; margin-top: 16%; font-size: 11.9px;"><?php echo $terms[0]->name; ?></div>
                    	</div>
                    </div>
                </div>
                <div class="liquid-container liquid-cont-team">
            		<?php 
					if ( '' == $team_mb->get_the_value('image') ) {
						the_post_thumbnail('full'); 
                    } else { ?>
					<img class="img-responsive" src="<?php echo $team_mb->get_the_value('image'); ?>" alt="<?php the_title(); ?>"/>
					<?php }
					?>	
                </div>
            </a>
        </div>

    <script type="text/javascript">
    	$("#ateam-<?php echo($post->ID);?>").mouseover(function(){
    		$(".id-<?php echo($post->ID);?>").show();
    	});
    	$("#ateam-<?php echo($post->ID);?>").mouseleave(function(){
    		$(".id-<?php echo($post->ID);?>").hide();
    	});
    </script>

		<?php endwhile; ?>
		<div class="clear-float"></div>
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('teamsic', 'po_teamsic_sc');


/* CLIENTS SHORTCODE
------------------------------------------------------- */  

function po_clients($atts) {
	
	ob_start();
	
	extract(shortcode_atts( 
		array (
			'category' => '',
			'number' => '20',
			'columnwidth' => '3',
			'order' => 'DESC',
			'orderby' => 'rand',
		), $atts ) 
	);
	
	?> 
    
    <div class="col-sm-10 col-sm-offset-1">
	
    <?php
	
	$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
	global $post;
	$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
	
	?>
    	
    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
        <div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
        <div class="client-details">
                	<div class="item-description">
                    
                    </div>
                </div>
                <div class="liquid-container-clients">
            		<?php the_post_thumbnail('full'); ?>
                </div>
            </div>
        </div>
		<?php endwhile; ?>
		<div class="clear-float"></div>
        </div>
		
	<?php wp_reset_postdata();
	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('clients', 'po_clients');

//------------------------------------------------------------------------------------------------
//Custom Clients Shortcode
function po_clients2($atts) {
	
	ob_start();
	
?>

<div id="clientsCarousel" class="carousel slide" data-ride="carousel" data-interval="0" style="margin-left: 10% !important; max-width: 80%;">

	<ol class="carousel-indicators" style="top: 170px; display: none;">
		<li data-target="#clientsCarousel" dat-slide-to="0" class="active"></li>
		<li data-target="#clientsCarousel" dat-slide-to="1"></li>
		<li data-target="#clientsCarousel" dat-slide-to="2"></li>
		<li data-target="#clientsCarousel" dat-slide-to="3"></li>
	</ol>

	<div class="carousel-inner" role="listbox">

		<div class="item active">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '7',
					'columnwidth' => '2',
					'order' => 'DESC',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => '7', 'order' => $order, 'orderby' => $orderby );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$titre = $post->post_title;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php echo $titre; ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>

		<div class="item">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '7',
					'columnwidth' => '2',
					'order' => 'DESC',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby, 'offset' => '7' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>

		<div class="item">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '7',
					'columnwidth' => '2',
					'order' => 'DESC',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby, 'offset' => '14' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>

		<div class="item">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '7',
					'columnwidth' => '2',
					'order' => 'DESC',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => '7', 'order' => $order, 'orderby' => $orderby, 'offset' => '21' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>

		<div class="item">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '7',
					'columnwidth' => '2',
					'order' => 'DESC',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => '7', 'order' => $order, 'orderby' => $orderby, 'offset' => '28' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>

		<!--
		<div class="item">

			<?php extract(shortcode_atts( 
				array (
					'category' => '',
					'number' => '5',
					'columnwidth' => '3',
					'order' => 'DESC',
					'offset' => '15',
					'orderby' => 'modified',
				), $atts ) 
			); ?> 
		    
		    <div class="col-sm-10 col-sm-offset-1">
			
		    <?php
			
			$args = array( 'post_type' => 'clients', 'client_categories' => $category, 'posts_per_page' => $number, 'order' => $order, 'orderby' => $orderby, 'offset' => '15' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
			global $post;
			$terms = get_the_term_list( $post->ID, 'client_categories', '', ' <span class"cat-sep"></span> ', '' );
			
			?>
		    	
		    	<div class="po-column client-image col-sm-<?php echo $columnwidth; ?> column-<?php echo $columnwidth; ?>" data-delay="0" data-animation="fade-in">
		        	<div class="client-container" data-toggle="tooltip" data-placement="top" title="<?php the_title(); ?>">
		        		<div class="client-details">
		                	<div class="item-description">
		                    
		                    </div>
		                </div>
		                <div class="liquid-container-clients">
		            		<?php the_post_thumbnail('full'); ?>
		                </div>
		            </div>
		        </div>
				<?php endwhile; ?>
				<div class="clear-float"></div>
		        </div>
				
			<?php wp_reset_postdata(); ?>

		</div>
		-->

	</div>

	<a class="left carousel-control" href="#clientsCarousel" role="button" data-slide="prev" style="margin-left: -15%; max-height: 50%; top: 110%; left: 45%;">
		<span class="fa fa-angle-left" aria-hidden="true" style=""></span>
	    <span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#clientsCarousel" role="button" data-slide="next" style="margin-right: -15%; max-height: 50%; top: 110%; right: 45%;">
	    <span class="fa fa-angle-right" aria-hidden="true" style=""></span>
	    <span class="sr-only">Next</span>
	</a>

</div>

<?php 

	$output = ob_get_clean();
	return $output;
	
}
add_shortcode('clients2', 'po_clients2');


/* TESTIMONIALS SHORTCODE
------------------------------------------------------- */  

function po_test( $atts ) {
	
	extract( shortcode_atts(
		array(
			'category' => '',
			'backgroundcolor' => '#f5f5f5',
			'textcolor' => '#333',
			'paddingtop' => '50',
			'paddingbottom' => '50',
			'header' => '',
			'width' => '',
		), $atts )
	);
	
	if ($header == "") {
		if ($width == "sidebar") {
		ob_start();
		?> 	
			<div class="test-container" style="padding-top:<?php echo $paddingtop; ?>px; padding-bottom:<?php echo $paddingbottom; ?>px; background-color:<?php echo $backgroundcolor; ?>; color:<?php echo $textcolor; ?>;">
			<div class="row po-full-width">
			<div class="col-sm-12" style="padding:0;">
			<div class="po-testimonials">
			<ul class="po-test-slide" style="padding:0;">
				 <?php  $args = array( 'post_type' => 'testimonials', 'testimonial_categories' => $category, 'posts_per_page' => '20', 'order' => 'DESC', 'orderby' => 'date' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					global $post;
					?>
				  <li style="color:<?php echo $textcolor; ?>;"><div class="test-text text-center"><?php the_content(); ?></div><div class="test-person text-center"><?php the_title(); ?></div></li>
				  <?php endwhile; ?>
				</ul>
			</div>
			<div class="clear-float"></div>
			</div>
			</div>
			</div>
		
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		}
		else {
		ob_start();
		?> 
        	</div></div></div></div><div class="entry-content">
			<div class="test-container" style="padding-top:<?php echo $paddingtop; ?>px; padding-bottom:<?php echo $paddingbottom; ?>px; background-color:<?php echo $backgroundcolor; ?>; color:<?php echo $textcolor; ?>;">
			<div class="row po-full-width">
			<div class="col-sm-12" style="padding:0;">
			<div class="po-testimonials">
			<ul class="po-test-slide" style="padding:0;">
				 <?php  $args = array( 'post_type' => 'testimonials', 'testimonial_categories' => $category, 'posts_per_page' => '20', 'order' => 'DESC', 'orderby' => 'date' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					global $post;
					?>
				  <li style="color:<?php echo $textcolor; ?>;"><div class="test-text text-center"><?php the_content(); ?></div><div class="test-person text-center"><?php the_title(); ?></div></li>
				  <?php endwhile; ?>
				</ul>
			</div>
			<div class="clear-float"></div>
			</div>
			</div>
			</div>
            </div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content">
		
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		}
	}
	
	else {
		if ($width == "sidebar") { 
		ob_start();
		?> 
			
			<div class="test-container" style="padding-top:<?php echo $paddingtop; ?>px; padding-bottom:<?php echo $paddingbottom; ?>px; background-color:<?php echo $backgroundcolor; ?>; color:<?php echo $textcolor; ?>;">
			<div class="row po-full-width">
			<h3 class="text-center"><?php echo $header; ?></h3>
			<div class="header-line" style="padding:0; margin:40px auto 30px;"></div>
			
			<div class="col-sm-12" style="padding:0;">
			<div class="po-testimonials">
			<ul class="po-test-slide" style="padding:0;">
				 <?php  $args = array( 'post_type' => 'testimonials', 'testimonial_categories' => $category, 'posts_per_page' => '20', 'order' => 'DESC', 'orderby' => 'date' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					global $post;
					?>
				  <li><div class="test-text text-center"><?php the_content(); ?></div><p class="test-person text-center"><?php the_title(); ?></p></li>
				  <?php endwhile; ?>
				</ul>
			</div>
			<div class="clear-float"></div>
			</div>
			</div>
			</div>
			
		
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		}
		else {
		
		ob_start();
		?> 
        	</div></div></div></div><div class="entry-content">
			<div class="test-container" style="padding-top:<?php echo $paddingtop; ?>px; padding-bottom:<?php echo $paddingbottom; ?>px; background-color:<?php echo $backgroundcolor; ?>; color:<?php echo $textcolor; ?>;">
			<div class="row po-full-width">
			<h3 class="text-center"><?php echo $header; ?></h3>
			<div class="header-line" style="padding:0; margin:40px auto 30px;"></div>
			
			<div class="col-sm-12" style="padding:0;">
			<div class="po-testimonials">
			<ul class="po-test-slide" style="padding:0;">
				 <?php  $args = array( 'post_type' => 'testimonials', 'testimonial_categories' => $category, 'posts_per_page' => '20', 'order' => 'DESC', 'orderby' => 'date' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					global $post;
					?>
				  <li><div class="test-text text-center"><?php the_content(); ?></div><p class="test-person text-center"><?php the_title(); ?></p></li>
				  <?php endwhile; ?>
				</ul>
			</div>
			<div class="clear-float"></div>
			</div>
			</div>
			</div>
            </div><div class="container po-container-section"><div class="row"><div class="col-sm-12"><div class="entry-content"> 
			
		
		<?php wp_reset_postdata();
		$output = ob_get_clean();
		}
	}
	
	return $output;

}
add_shortcode( 'testimonials', 'po_test' );


/* SOCIAL SHORTCODE
------------------------------------------------------- */  

function po_social( $atts ) {
	
	extract( shortcode_atts(
		array(  
			'type' => '',
		), $atts )
	);
	
	$email = get_theme_mod( 'email_address' );
	$twitter = get_theme_mod( 'twitter_username' );
	$facebook = get_theme_mod( 'facebook_url' );
	$googleplus = get_theme_mod( 'googleplus_url' );
	$pinterest = get_theme_mod( 'pinterest_username' );
	$youtube = get_theme_mod( 'youtube_url' );
	$vimeo = get_theme_mod( 'vimeo_username' );
	$linkedin = get_theme_mod( 'linkedin_url' );
	$github = get_theme_mod( 'github_url' );
	$foursquare = get_theme_mod( 'foursquare_url' );
	$instagram = get_theme_mod( 'instagram_username' );
	$flickr = get_theme_mod( 'flickr_url' );
	
	$social="";
	
	if ($type == '') {
		if ($email) {
			$social .= '<li><a href="mailto:'.$email.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
		} 
		if ($twitter) {
			$social .= '<li><a href="http://www.twitter.com/'.$twitter.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
		} 
		if ($facebook) {
			$social .= '<li><a href="'.$facebook.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
		} 
		if ($googleplus) {
			$social .= '<li><a href="'.$googleplus.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
		} 
		if ($pinterest) {
			$social .= '<li><a href="http://www.pinterest.com/'.$pinterest.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
		} 
		if ($youtube) {
			$social .= '<li><a href="'.$youtube.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
		} 
		if ($vimeo) {
			$social .= '<li><a href="http://www.vimeo.com/'.$vimeo.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
		}
		if ($linkedin) {
			$social .= '<li><a href="'.$linkedin.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
		} 
		if ($github) {
			$social .= '<li><a href="'.$github.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
		} 
		if ($foursquare) {
			$social .= '<li><a href="'.$foursquare.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
		} 
		if ($instagram) {
			$social .= '<li><a href="http://instagram.com/'.$instagram.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
		} 
		if ($flickr) {
			$social .= '<li><a href="'.$flickr.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
		} 
	} else {
		
			$social_list = explode(', ', $type);
			foreach ($social_list as $list) {
				
				if ($list == "email") {
					$social .= '<li><a href="mailto:'.$email.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
				} 
				if ($list == "twitter") {
					$social .= '<li><a href="http://www.twitter.com/'.$twitter.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
				} 
				if ($list == "facebook") {
					$social .= '<li><a href="'.$facebook.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
				} 
				if ($list == "googleplus") {
					$social .= '<li><a href="'.$googleplus.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
				} 
				if ($list == "pinterest") {
					$social .= '<li><a href="http://www.pinterest.com/'.$pinterest.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
				} 
				if ($list == "youtube") {
					$social .= '<li><a href="'.$youtube.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
				} 
				if ($list == "vimeo") {
					$social .= '<li><a href="http://www.vimeo.com/'.$vimeo.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
				}
				if ($list == "linkedin") {
					$social .= '<li><a href="'.$linkedin.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
				} 
				if ($list == "github") {
					$social .= '<li><a href="'.$github.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
				} 
				if ($list == "foursquare") {
					$social .= '<li><a href="'.$foursquare.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
				} 
				if ($list == "instagram") {
					$social .= '<li><a href="http://instagram.com/'.$instagram.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
				} 
				if ($list == "flickr") {
					$social .= '<li><a href="'.$flickr.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
				} 
			
			}
	}
	
	$output = '';
	$output .= '<ul class="social-links">'."\n";
	$output .= $social;
	$output .= '</ul><div class="clear-float"></div>';
	
	return $output;

}
add_shortcode( 'social', 'po_social' );


/* SOCIAL PERSON SHORTCODE
------------------------------------------------------- */  

function po_social_person( $atts ) {
	
	extract( shortcode_atts(
		array(  
			'type' => '',
			'emailaddress' => '',
			'twitterusername' => '',
			'facebookurl' => '',
			'googleplusurl' => '',
			'pinterestusername' => '',
			'youtubeurl' => '',
			'vimeousername' => '',
			'linkedinurl' => '',
			'githuburl' => '',
			'foursquareurl' => '',
			'instagramusername' => '',
			'flickrurl' => '',
		), $atts )
	);
	
	
	$social="";
	
	if ($type == '') {
		if ( $emailaddress) {
			$social .= '<li><a href="mailto:'.$emailaddress.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
		} 
		if ($twitterusername) {
			$social .= '<li><a href="http://www.twitter.com/'.$twitterusername.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
		} 
		if ($facebookurl) {
			$social .= '<li><a href="'.$facebookurl.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
		} 
		if ($googleplusurl) {
			$social .= '<li><a href="'.$googleplusurl.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
		} 
		if ($pinterestusername) {
			$social .= '<li><a href="http://www.pinterest.com/'.$pinterestusername.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
		} 
		if ($youtubeurl) {
			$social .= '<li><a href="'.$youtubeurl.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
		} 
		if ($vimeousername) {
			$social .= '<li><a href="http://www.vimeo.com/'.$vimeousername.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
		}
		if ($linkedinurl) {
			$social .= '<li><a href="'.$linkedinurl.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
		} 
		if ($githuburl) {
			$social .= '<li><a href="'.$githuburl.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
		} 
		if ($foursquareurl) {
			$social .= '<li><a href="'.$foursquareurl.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
		} 
		if ($instagramusername) {
			$social .= '<li><a href="http://instagram.com/'.$instagramusername.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
		} 
		if ($flickrurl) {
			$social .= '<li><a href="'.$flickrurl.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
		} 
	} else {
		
			$sociallist = explode(', ', $type);
			foreach ($sociallist as $list) {
				
				if ($list == "email") {
					$social .= '<li><a href="mailto:'.$emailaddress.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
				} 
				if ($list == "twitter") {
					$social .= '<li><a href="http://www.twitter.com/'.$twitterusername.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
				} 
				if ($list == "facebook") {
					$social .= '<li><a href="'.$facebookurl.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
				} 
				if ($list == "googleplus") {
					$social .= '<li><a href="'.$googleplusurl.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
				} 
				if ($list == "pinterest") {
					$social .= '<li><a href="http://www.pinterest.com/'.$pinterestusername.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
				} 
				if ($list == "youtube") {
					$social .= '<li><a href="'.$youtubeurl.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
				} 
				if ($list == "vimeo") {
					$social .= '<li><a href="http://www.vimeo.com/'.$vimeousername.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
				}
				if ($list == "linkedin") {
					$social .= '<li><a href="'.$linkedinurl.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
				} 
				if ($list == "github") {
					$social .= '<li><a href="'.$githuburl.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
				} 
				if ($list == "foursquare") {
					$social .= '<li><a href="'.$foursquareurl.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
				} 
				if ($list == "instagram") {
					$social .= '<li><a href="http://instagram.com/'.$instagramusername.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
				} 
				if ($list == "flickr") {
					$social .= '<li><a href="'.$flickrurl.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
				} 
			
			}
	}
	
	$output = '';
	$output .= '<ul class="social-links">'."\n";
	$output .= $social;
	$output .= '</ul><div class="clear-float"></div>';
	
	return $output;

}
add_shortcode( 'social_person', 'po_social_person' );


/* SOCIAL FOOTER SHORTCODE
------------------------------------------------------- */  

function po_social_footer( $atts ) {
	
	extract( shortcode_atts(
		array(  
			'type' => '',
		), $atts )
	);
	
	$email = get_theme_mod( 'email_address' );
	$twitter = get_theme_mod( 'twitter_username' );
	$facebook = get_theme_mod( 'facebook_url' );
	$googleplus = get_theme_mod( 'googleplus_url' );
	$pinterest = get_theme_mod( 'pinterest_username' );
	$youtube = get_theme_mod( 'youtube_url' );
	$vimeo = get_theme_mod( 'vimeo_username' );
	$linkedin = get_theme_mod( 'linkedin_url' );
	$github = get_theme_mod( 'github_url' );
	$foursquare = get_theme_mod( 'foursquare_url' );
	$instagram = get_theme_mod( 'instagram_username' );
	$flickr = get_theme_mod( 'flickr_url' );
	
	$social="";
	
	if ($type == '') {
		if ($email) {
			$social .= '<li><a href="mailto:'.$email.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
		} 
		if ($twitter) {
			$social .= '<li><a href="http://www.twitter.com/'.$twitter.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
		} 
		if ($facebook) {
			$social .= '<li><a href="'.$facebook.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
		} 
		if ($googleplus) {
			$social .= '<li><a href="'.$googleplus.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
		} 
		if ($pinterest) {
			$social .= '<li><a href="http://www.pinterest.com/'.$pinterest.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
		} 
		if ($youtube) {
			$social .= '<li><a href="'.$youtube.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
		} 
		if ($vimeo) {
			$social .= '<li><a href="http://www.vimeo.com/'.$vimeo.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
		}
		if ($linkedin) {
			$social .= '<li><a href="'.$linkedin.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
		} 
		if ($github) {
			$social .= '<li><a href="'.$github.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
		} 
		if ($foursquare) {
			$social .= '<li><a href="'.$foursquare.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
		} 
		if ($instagram) {
			$social .= '<li><a href="http://instagram.com/'.$instagram.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
		} 
		if ($flickr) {
			$social .= '<li><a href="'.$flickr.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
		} 
	} else {
		
			$social_list = explode(', ', $type);
			foreach ($social_list as $list) {
				
				if ($list == "email") {
					$social .= '<li><a href="mailto:'.$email.'"><span class="index green"><i class="fa fa-envelope-o"></i></span></a></li>'."\n";
				} 
				if ($list == "twitter") {
					$social .= '<li><a href="http://www.twitter.com/'.$twitter.'"><span class="index twit"><i class="fa fa-twitter"></i></span></a></li>'."\n";
				} 
				if ($list == "facebook") {
					$social .= '<li><a href="'.$facebook.'"><span class="index fb"><i class="fa fa-facebook"></i></span></a></li>'."\n";
				} 
				if ($list == "googleplus") {
					$social .= '<li><a href="'.$googleplus.'"><span class="index goog"><i class="fa fa-google-plus"></i></span></a></li>'."\n";
				} 
				if ($list == "pinterest") {
					$social .= '<li><a href="http://www.pinterest.com/'.$pinterest.'"><span class="index pint"><i class="fa fa-pinterest"></i></span></a></li>'."\n";
				} 
				if ($list == "youtube") {
					$social .= '<li><a href="'.$youtube.'"><span class="index yt"><i class="fa fa-youtube"></i></span></a></li>'."\n";
				} 
				if ($list == "vimeo") {
					$social .= '<li><a href="http://www.vimeo.com/'.$vimeo.'"><span class="index vimeo"><i class="fa fa-vimeo-square"></i></span></a></li>'."\n";
				}
				if ($list == "linkedin") {
					$social .= '<li><a href="'.$linkedin.'"><span class="index linked"><i class="fa fa-linkedin"></i></span></a></li>'."\n";
				} 
				if ($list == "github") {
					$social .= '<li><a href="'.$github.'"><span class="index git"><i class="fa fa-github"></i></span></a></li>'."\n";
				} 
				if ($list == "foursquare") {
					$social .= '<li><a href="'.$foursquare.'"><span class="index fours"><i class="fa fa-foursquare"></i></span></a></li>'."\n";
				} 
				if ($list == "instagram") {
					$social .= '<li><a href="http://instagram.com/'.$instagram.'"><span class="index insta"><i class="fa fa-instagram"></i></span></a></li>'."\n";
				} 
				if ($list == "flickr") {
					$social .= '<li><a href="'.$flickr.'"><span class="index flickr"><i class="fa fa-flickr"></i></span></a></li>'."\n";
				} 
			
			}
	}
	
	$output = '';
	$output .= '<ul class="footer-links">'."\n";
	$output .= $social;
	$output .= '</ul><div class="clear-float"></div>';
	
	return $output;

}
add_shortcode( 'social_footer', 'po_social_footer' );

// Fonction de creation du babillard sicotte 

function sicotte_babillard() {

	ob_start();

	?>

	<div class="bab-buttons">
		
	</div>

	<div class="bab-content" style="width: 100%;">
		<div class="babillard baillard-col1 col-md-3">
			<a class="aEricBloc" href="<?php echo get_site_url(); ?>/team/eric-sicotte/">
			<div class="ericBloc" style="margin-bottom: 9%;">
				<div class="imgEric"></div>
				<div style="margin-bottom: 5%;">
					<i class="fa fa-user fa-4x" style="float: left; color: #f13247; margin-left: 12%; margin-right: -14%;"></i>
					<div>
						<h2 style="text-align: center;">Eric Sicotte</h2><hr style="width: 35%; color: black; height: 2px;">
					</div>
				</div>
				<div>
					<i class="fa fa-key fa-3x" style="float: left; color: #f13247; margin-left: 12%; margin-right: 19%;"></i>
					<div>
						<h4><?php echo __("La clé du succés"); ?></h4>
					</div>
				</div>
			</div>
			</a>
			<div class="ClubDecBloc">
				<a class="aBabillardClubLecture" href="<?php echo get_site_url(); ?>/club-de-lecture/">
				<div class="titre" style="text-align: left; margin-left: 30%;">
					<h1 style="font-weight: bold;">Club</h1>
					<h1 style="font-weight: bold;">de Lecture</h1>
				</div>
				<!--
				<div style="margin-top: 8%;">
					<i class="fa fa-picture-o fa-3x" style="float: left; margin-left: 18%; margin-right: 12%; color: #f13247;"></i>
					<h2>10 photos</h2>
				</div> -->
				<div style="margin-top: 8%; margin-bottom: 8%;">
					<i class="fa fa-book fa-3x" style="float: left; margin-left: 12%; margin-right: 10%; color: #f13247;"></i>
					<h2>5 <?php echo __("livres"); ?></h2>
				</div>
				</a>
			</div>
			<div class="diversiteBloc">
				<a class="aDiversiteBloc" title="<?php echo __("À Venir"); ?>" style="color: black;">
				<div class="col-md-3">
					<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/paint.png" style="margin-top: 2px; zoom: 125%; margin-left: 14px; margin-bottom: 14px;">
				</div>
				<div class="col-md-9" style="margin-top: 5%; padding-left: 5%;">
					<h1 style="font-weight: bold;"><?php echo __("Rubrique"); ?></h1>
					<h1 style="font-weight: bold;"><?php echo __("Diversité"); ?></h1>
				</div>
				</a>
			</div>
		</div>
		<div class="babillard baillard-col2 col-md-6">
			<div class="CarrierePTBloc" style="height: 400px;">
				<div class="col-md-6" style="height: 100%;">
					<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/CarrierePourTous.png" style="width: 50%; margin-left: 20%; margin-top: 10%;">
				</div>
				<div class="col-md-6" style="margin-top: 6%;">
					<h2 style="text-align: center; font-weight: bold;"><?php echo __("Causes soutenues"); ?></h2>
					<h3 style="text-align: center;">Carrière pour tous est une fondation dont...<br>- texte à venir</h3>
					<ul style="margin-left: 7%; margin-top: 7%;">
						<li style="padding-top: 0px !important; padding-bottom: 0px !important;">Fondation Simple Plan</li>
						<li style="padding-top: 0px !important; padding-bottom: 0px !important;">Fondation Kovalev et ses Amis</li>
						<li style="padding-top: 0px !important; padding-bottom: 0px !important;">20XX : gestes poses par l'organisme - texte a venir</li>
					</ul>
				</div>
			</div>
			<div class="ImgViktorTatran"></div>
			<div class="CollecSicotte" style="padding-top: 2%; padding-bottom: 4%;">
				<h1 style="font-weight: bold; text-align: center;"><?php echo __("La collection "); ?><span style="color: #f13247;">Sicotte</span></h1>
				<h3 style="text-align: center; margin-left: 7%; margin-right: 7%;">Eric Sicotte souhaite venir en aide aux jeunes artistes et à la scène artistique locale en general - Texte a venir</h3>
			</div>
		</div>
		<div class="babillard baillard-col3 col-md-3">
			<div class="CarriereBloc">
				<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/Carrière_apollotv-01.png" style="zoom: 200%; margin-left: 20%;">
				<div style="margin-bottom: 5%;">
					<i class="fa fa-thumbs-o-up fa-4x fa-flip-horizontal" style="float: left; color: #f13247; margin-left: 12%; margin-right: -14%;"></i>
					<div>
						<h2 style="text-align: center;"><?php echo __("Opportunités"); ?></h2><hr style="width: 35%; color: black; height: 2px;">
					</div>
					<p style="text-align: left; width: 60%; margin-left: 36%;"><?php echo __("Les offres SICOTTE Recrutement sont egalement disponible sur d'autres sites"); ?></p>
					<a href="http://apollo1.tv/category/carrieres-et-opportunites/" style="color: #428bca;"><h3 style="margin-left: 18%; margin-top: 6%;"><?php echo __("Visionez nos offres sur Apollo1.tv"); ?></h3></a>
				</div>
			</div>
			<div class="CarriereBloc">
				<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/metropole.gif" style="margin-left: 27%;">
				<div style="margin-bottom: 5%;">
					<i class="fa fa-thumbs-o-up fa-4x fa-flip-horizontal" style="float: left; color: #f13247; margin-left: 12%; margin-right: -14%;"></i>
					<div>
						<h2 style="text-align: center;"><?php echo __("Opportunités"); ?></h2><hr style="width: 35%; color: black; height: 2px;">
					</div>
					<p style="text-align: left; width: 60%; margin-left: 36%;"><?php echo __("Les offres SICOTTE Recrutement sont egalement disponible sur d'autres sites"); ?></p>
					<a href="http://www.lametropole.com/" style="color: #428bca;"><h3 style="margin-left: 18%; margin-top: 6%; text-align: center;"><?php echo __("Visionez nos offres sur La Metropole"); ?></h3></a>
				</div>
			</div>
			<a class="aTravailEU" title="<?php echo __("À Venir"); ?>">
			<div class="TravailEU" style="padding-top: 2%; padding-bottom: 5%; background-color: rgb(225,225,225);">
				<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/statue_liberté-01.png" style="float: left; zoom: 135%; margin-left: 14%;">
				<div>
					<h3 style="text-align: center; margin-left: 24%;"><?php echo __("Travailler aux Etats-Unis"); ?></h3><hr style="width: 51%; color: black; height: 2px;">
					<p style="width: 60%; margin-left: 38%;">Aujourd'hui de plus en plus de jeunes s'exportent...     Texte sur le debut de l'article</p>
				</div>
			</div>
			</a>
		</div>
	</div>

	<style type="text/css">

		.babillard {
			background-color: rgb(241,241,241);
			border: 2px solid;
			border-color: rgb(210,210,210);
			padding-left: 0px !important;
			padding-right: 0px !important;
		}
		.imgEric {
			width: inherit;
			height: 345px;
			background-image: url("<?php echo get_site_url(); ?>/wp-content/uploads/2014/11/Eric_Sicotte.png");
			background-size: cover;
		}
		.ImgViktorTatran {
			background-image: url("<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/Viktor_Tatran02.jpg");
			background-size: cover;
			width: 100%;
			height: 476px;
			border-top: 8px solid;
			border-color: rgb(210,210,210);
		}
		.ClubDecBloc, .CarriereBloc, .TravailEU, .CollecSicotte, .CarrierePTBloc, .diversiteBloc {
			border: 2px solid;
			border-color: rgb(210,210,210);
		}

		h1, h2, h3, h4, h5, p {
			font-family: 'Open Sans', sans-serif;
		}

		@media screen and (max-width: 990px) {
			.ImgViktorTatran { height: 236px; }
		}

		#post-23 > div:nth-child(16) > div > div > div > div.bab-content > div.babillard.baillard-col1.col-md-3 > a > div > div:nth-child(2) > div > h2,
		#post-23 > div:nth-child(16) > div > div > div > div.bab-content > div.babillard.baillard-col1.col-md-3 > a > div > div:nth-child(3) > div > h4,
		#post-23 > div:nth-child(16) > div > div > div > div.bab-content > div.babillard.baillard-col3.col-md-3 > a > div > div > h3,
		#post-23 > div:nth-child(16) > div > div > div > div.bab-content > div.babillard.baillard-col3.col-md-3 > a > div > div > p {
			color: black;
		}

		.aEricBloc:hover, .aTravailEU:hover {
			color: #f4545f;
		}

		.aDiversiteBloc:hover {
			color: #f4545f !important;
		}

	</style>

	<?php

	$output = ob_get_clean();

	return $output;

}
add_shortcode( 'babillard', 'sicotte_babillard' );

function bouton_babillard() {

	ob_start(); ?>

	<div class="boutonsBabi" style="text-align: center; margin-bottom: 2%;">
		<button class="BBabi">Recents <span class="ButCompteur"> 7</span></button>
		<button class="BBabi">Tout <span class="ButCompteur"> 13</span></button>
		<button class="BBabi">Eric Sicotte <span class="ButCompteur"> 3</span></button>
		<button class="BBabi">Carritatif <span class="ButCompteur"> 8</span></button>
		<button class="BBabi">Bon plan <span class="ButCompteur"> 3</span></button>
		<button class="BBabi">Opportunites <span class="ButCompteur"> 2</span></button>
	</div>

	<style type="text/css">
		.ButCompteur {
			color: rgb(202,202,202);
		}
		.BBabi {
			margin-left: -3px !important;
			margin-right: -3px !important;
			padding-left: 1.5%;
			padding-right: 1.5%;
			background-color: rgb(245,245,245);
			border-radius: 4px;
			border: 1px solid;
			border-color: rgb(231,231,231);
		}
		.BBabi:hover {
			background-color: rgb(238,238,238);
		}
	</style>

	<?php
	$output = ob_get_clean();
	return $output;

}
add_shortcode( 'boutonsbabillard', 'bouton_babillard' );

function footer_sicotte() {
	
	ob_start(); ?>

	<div style="position: absolute; color: darkgray; display: none; width: 100%; padding-top: 10px;">
		
		<div class="col-md-4 dateheure" style="float: left; text-align: center;">
			<!--
			<?php 
			date_default_timezone_set('America/Montreal');
			$heure = date("H:i"); 
			setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
			?>
			<div style="width: 200px;border:1px solid white;border-bottom: 0;padding: 5px 0;text-transform: uppercase;color: white;text-align: center;font-size: large; margin-left: auto; margin-right: auto;">
				<?php echo (strftime("%d %B %Y")); ?>
			</div>
			<div style="height:85px;width: 200px;border: 1px solid white;padding: 5px 0;text-transform:uppercase;color: white;text-align:center;font-size: -webkit-xxx-large; margin-left: auto; margin-right: auto;">
				<?php echo $heure; ?>
			</div>
			-->
			<?php echo do_shortcode('[coolclock skin="chunkySwissOnBlack" radius="55" showdigital=true]'); ?>
			<h5>© 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h5>
		</div>

		<div class="col-md-4 copyright" style="text-align: center;">
			<img src="<?php echo get_site_url(); ?>/wp-content/themes/nibiru/images/LOGO-SICOTTE-simple-blanc.png" style="max-height: 100px;">
			
		</div>

		<div class="col-md-4 liens" style="float: right; padding-left: 10%; margin-top: 15px;">
			<a href="http://sicotterecrutement.ca/"><h5>ACCUEIL</h5></a>
			<a href="http://sicotterecrutement.ca/?post_type=portfolio"><h5>CATÉGORIES</h5></a>
			<a href="http://sicotterecrutement.ca/offres-demploi/"><h5>OFFRES D'EMPLOI</h5></a>
			<a href="http://sicotterecrutement.ca/contact/"><h5>CONTACT</h5></a>
		</div>
		
	</div>

	<style type="text/css">
		.liens > a:hover {
			color: white;
		}
	</style>

	<?php
	$output = ob_get_clean();
	return $output;
}
add_shortcode( 'footerSicotte', 'footer_sicotte' );

?>