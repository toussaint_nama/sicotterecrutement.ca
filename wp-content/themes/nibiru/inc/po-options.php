<?php

/* LOGO 
------------------------------------------------------- */ 

function po_logo( $wp_customize ) {
    $wp_customize->add_section(
        'logo',
        array(
            'title' => 'Logo',
			'description' => 'For retina, upload a logo double in size, then enter half the height of the uploaded logo (in px).',
            'priority' => 1,
        )
    );
	$wp_customize->add_setting( 'logo_upload' );
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'logo_upload',
			array(
				'label' => 'Upload logo',
				'section' => 'logo',
				'settings' => 'logo_upload',
				'priority' => 1,
			)
		)
	);
	$wp_customize->add_setting(
		'logo_width'
	);
	$wp_customize->add_control(
		'logo_width',
		array(
			'label' => 'Logo half width',
			'section' => 'logo',
			'type' => 'text',
			'priority' => 2,
		)
	);
	$wp_customize->add_setting(
		'logo_url'
	);
	$wp_customize->add_control(
		'logo_url',
		array(
			'label' => 'Logo URL',
			'section' => 'logo',
			'type' => 'text',
			'priority' => 3,
		)
	);
	$wp_customize->add_setting(
		'blog_logo_hide'
	);
	$wp_customize->add_control(
		'blog_logo_hide',
		array(
			'type' => 'checkbox',
			'label' => 'Show blog logo',
			'section' => 'logo',
			'priority' => 4,
		)
	);
	$wp_customize->add_setting( 'blog_logo_upload' );
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'blog_logo_upload',
			array(
				'label' => 'Upload blog logo (optional)',
				'section' => 'logo',
				'settings' => 'blog_logo_upload',
				'priority' => 5,
			)
		)
	);
	$wp_customize->add_setting(
		'blog_logo_width'
	);
	$wp_customize->add_control(
		'blog_logo_width',
		array(
			'label' => 'Blog logo half width',
			'section' => 'logo',
			'type' => 'text',
			'priority' => 6,
		)
	);

}
add_action( 'customize_register', 'po_logo' );


/* GENERAL 
------------------------------------------------------- */ 

function po_general( $wp_customize ) {
    $wp_customize->add_section(
        'general',
        array(
            'title' => 'General',
            'priority' => 1,
        )
    );
	$wp_customize->add_setting( 'blog_image_upload' );
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'blog_image_upload',
			array(
				'label' => 'Blog background',
				'section' => 'general',
				'settings' => 'blog_image_upload',
				'priority' => 5,
			)
		)
	);
	$wp_customize->add_setting(
		'blog_image_overlay',
		array(
			'default' => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'blog_image_overlay',
			array(
				'label' => 'Blog background overlay color',
				'section' => 'general',
				'settings' => 'blog_image_overlay',
				'priority' => 6,
			)
		)
	);
	$wp_customize->add_setting(
		'blog_image_overlay_hide'
	);
	$wp_customize->add_control(
		'blog_image_overlay_hide',
		array(
			'type' => 'checkbox',
			'label' => 'Show blog background overlay',
			'section' => 'general',
			'priority' => 7,
		)
	);
	$wp_customize->add_setting(
		'social_icons',
		array(
			'default' => '#B9BABB',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'social_icons',
			array(
				'label' => 'Social icons',
				'section' => 'general',
				'settings' => 'social_icons',
				'priority' => 8,
			)
		)
	);
	$wp_customize->add_setting(
		'favicon_url'
	);
	$wp_customize->add_control(
		'favicon_url',
		array(
			'label' => 'Favicon URL',
			'section' => 'general',
			'type' => 'text',
			'priority' => 9,
		)
	);
}
add_action( 'customize_register', 'po_general' );


/* PORTFOLIO DETAILS 
------------------------------------------------------- */ 

function po_portfolio( $wp_customize ) {
    $wp_customize->add_section(
        'portfolio',
        array(
            'title' => 'Portfolio Details',
            'priority' => 2,
        )
    );
	$wp_customize->add_setting(
		'portfolio_title',
		array(
			'default' => __('Portfolio','pixelobject'),
		)
	);
	$wp_customize->add_control(
		'portfolio_title',
		array(
			'label' => 'Page title',
			'section' => 'portfolio',
			'type' => 'text',
			'priority' => 1,
		)
	);
	$wp_customize->add_setting(
		'portfolio_category'
	);
	$wp_customize->add_control(
		'portfolio_category',
		array(
			'label' => 'Portfolio category',
			'section' => 'portfolio',
			'type' => 'text',
			'priority' => 3,
		)
	);
	$wp_customize->add_setting(
    'portfolio_order',
		array(
			'default' => 'DESC',
		)
	);
	 
	$wp_customize->add_control(
		'portfolio_order',
		array(
			'type' => 'select',
			'label' => 'Order of portfolio:',
			'section' => 'portfolio',
			'priority' => 4,
			'choices' => array(
				'DESC' => 'Descending',
				'ASC' => 'Ascending',
			),
		)
	);
	$wp_customize->add_setting(
    'portfolio_orderby',
		array(
			'default' => 'date',
		)
	);
	 
	$wp_customize->add_control(
		'portfolio_orderby',
		array(
			'type' => 'select',
			'label' => 'Order portfolio by:',
			'section' => 'portfolio',
			'priority' => 5,
			'choices' => array(
				'date' => 'Date',
				'title' => 'Title',
				'modified' => 'Last modified',
				'rand' => 'Random',
			),
		)
	);
	$wp_customize->add_setting(
		'portfolio_slug',
		array(
			'default' => __('portfolio','pixelobject'),
		)
	);
	$wp_customize->add_control(
		'portfolio_slug',
		array(
			'label' => 'Slug',
			'section' => 'portfolio',
			'type' => 'text',
			'priority' => 6,
		)
	);
}
add_action( 'customize_register', 'po_portfolio' );


/* TEAM DETAILS 
------------------------------------------------------- */ 

function po_team( $wp_customize ) {
    $wp_customize->add_section(
        'team',
        array(
            'title' => 'Team Details',
            'priority' => 3,
        )
    );
	$wp_customize->add_setting(
		'team_title',
		array(
			'default' => __('Meet the team','pixelobject'),
		)
	);
	$wp_customize->add_control(
		'team_title',
		array(
			'label' => 'Page title',
			'section' => 'team',
			'type' => 'text',
			'priority' => 1,
		)
	);
	$wp_customize->add_setting(
		'team_subtitle'
	);
	$wp_customize->add_control(
		'team_subtitle',
		array(
			'label' => 'Page subtitle',
			'section' => 'team',
			'type' => 'text',
			'priority' => 2,
		)
	);
	$wp_customize->add_setting(
		'team_category'
	);
	$wp_customize->add_control(
		'team_category',
		array(
			'label' => 'Team category',
			'section' => 'team',
			'type' => 'text',
			'priority' => 3,
		)
	);
	$wp_customize->add_setting(
    'team_order',
		array(
			'default' => 'DESC',
		)
	);
	 
	$wp_customize->add_control(
		'team_order',
		array(
			'type' => 'select',
			'label' => 'Order of team:',
			'section' => 'team',
			'priority' => 4,
			'choices' => array(
				'DESC' => 'Descending',
				'ASC' => 'Ascending',
			),
		)
	);
	$wp_customize->add_setting(
    'team_orderby',
		array(
			'default' => 'date',
		)
	);
	 
	$wp_customize->add_control(
		'team_orderby',
		array(
			'type' => 'select',
			'label' => 'Order team by:',
			'section' => 'team',
			'priority' => 5,
			'choices' => array(
				'date' => 'Date',
				'title' => 'Title',
				'modified' => 'Last modified',
				'rand' => 'Random',
			),
		)
	);
	$wp_customize->add_setting(
    'team_columns',
		array(
			'default' => '3',
		)
	);
	 
	$wp_customize->add_control(
		'team_columns',
		array(
			'type' => 'select',
			'label' => 'Columns',
			'section' => 'team',
			'priority' => 6,
			'choices' => array(
				'6' => '2',
				'4' => '3',
				'3' => '4',
				'2' => '6',
			),
		)
	);
	$wp_customize->add_setting(
		'team_slug',
		array(
			'default' => __('team','pixelobject'),
		)
	);
	$wp_customize->add_control(
		'team_slug',
		array(
			'label' => 'Slug',
			'section' => 'team',
			'type' => 'text',
			'priority' => 7,
		)
	);
}
add_action( 'customize_register', 'po_team' );

/* SOCIAL PROFILES
------------------------------------------------------- */ 

function po_social_profiles( $wp_customize ) {
    $wp_customize->add_section(
        'social_profiles',
        array(
            'title' => 'Social Profiles',
            'description' => 'These are the fields that power the social shortcode. If you include a link/username here, then the icon will be included in the shortcodes output.',
            'priority' => 4,
        )
    );
	$wp_customize->add_setting(
		'email_address'
	);
	$wp_customize->add_control(
		'email_address',
		array(
			'label' => 'Email address',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'twitter_username'
	);
	$wp_customize->add_control(
		'twitter_username',
		array(
			'label' => 'Twitter username',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'facebook_url'
	);
	$wp_customize->add_control(
		'facebook_url',
		array(
			'label' => 'Facebook URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'googleplus_url'
	);
	$wp_customize->add_control(
		'googleplus_url',
		array(
			'label' => 'Google Plus URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'pinterest_username'
	);
	$wp_customize->add_control(
		'pinterest_username',
		array(
			'label' => 'Pinterest username',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'youtube_url'
	);
	$wp_customize->add_control(
		'youtube_url',
		array(
			'label' => 'Youtube URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'vimeo_username'
	);
	$wp_customize->add_control(
		'vimeo_username',
		array(
			'label' => 'Vimeo username',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'linkedin_url'
	);
	$wp_customize->add_control(
		'linkedin_url',
		array(
			'label' => 'LinkedIn URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'github_url'
	);
	$wp_customize->add_control(
		'github_url',
		array(
			'label' => 'Github URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'foursquare_url'
	);
	$wp_customize->add_control(
		'foursquare_url',
		array(
			'label' => 'Foursquare URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'instagram_username'
	);
	$wp_customize->add_control(
		'instagram_username',
		array(
			'label' => 'Instagram username',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting(
		'flickr_url'
	);
	$wp_customize->add_control(
		'flickr_url',
		array(
			'label' => 'Flickr URL',
			'section' => 'social_profiles',
			'type' => 'text',
		)
	);
}
add_action( 'customize_register', 'po_social_profiles' );


/* FOOTER DETAILS
------------------------------------------------------- */ 

function po_footer_details( $wp_customize ) {
    $wp_customize->add_section(
        'footer_details',
        array(
            'title' => 'Footer Details',
            'priority' => 5,
        )
    );
	$wp_customize->add_setting(
    'footer_button_color',
		array(
			'default' => 'DESC',
		)
	);
	 
	$wp_customize->add_control(
		'footer_button_color',
		array(
			'type' => 'select',
			'label' => 'CTA button color',
			'section' => 'footer_details',
			'priority' => 1,
			'choices' => array(
				'light' => 'Light',
				'dark' => 'Dark',
			),
		)
	);
	$wp_customize->add_setting(
		'footer_button_icon'
	);
	$wp_customize->add_control(
		'footer_button_icon',
		array(
			'label' => 'CTA button icon',
			'section' => 'footer_details',
			'type' => 'text',
			'priority' => 2,
		)
	);
	$wp_customize->add_setting(
		'footer_button_link'
	);
	$wp_customize->add_control(
		'footer_button_link',
		array(
			'label' => 'CTA button link',
			'section' => 'footer_details',
			'type' => 'text',
			'priority' => 3,
		)
	);
	$wp_customize->add_setting(
		'footer_button_text'
	);
	$wp_customize->add_control(
		'footer_button_text',
		array(
			'label' => 'CTA button text',
			'section' => 'footer_details',
			'type' => 'text',
			'priority' => 4,
		)
	);
	$wp_customize->add_setting(
		'copyright_textbox',
		array(
			'default' => '<a href="http://pixelobject.com/themes/nibiru/">Nibiru</a> by <a href="http://pixelobject.com">Pixel Object</a>.',
		)
	);
	$wp_customize->add_control(
		'copyright_textbox',
		array(
			'label' => 'Copyright text',
			'section' => 'footer_details',
			'type' => 'text',
			'priority' => 12,
		)
	);
}
add_action( 'customize_register', 'po_footer_details' );


/* TYPOGRAPHY
------------------------------------------------------- */ 

function po_typography( $wp_customize ) {
    $wp_customize->add_section(
        'typography',
        array(
            'title' => 'Typography',
			'description' => 'First, get <a href="https://www.google.com/fonts" target="_blank">Google font</a>.',
            'priority' => 6,
        )
    );
	$wp_customize->add_setting(
		'family_code',
		array(
			'default' => 'Source+Sans+Pro:300,400,600',
		)
	);
	$wp_customize->add_control(
		'family_code',
		array(
			'label' => 'Font family code',
			'section' => 'typography',
			'type' => 'text',
			'priority' => 1,
		)
	);
	$wp_customize->add_setting(
		'header_font',
		array(
			'default' => "'Source Sans Pro', sans-serif;",
		)
	);
	$wp_customize->add_control(
		'header_font',
		array(
			'label' => 'Header font CSS',
			'section' => 'typography',
			'type' => 'text',
			'priority' => 2,
		)
	);
	$wp_customize->add_setting(
		'body_font',
		array(
			'default' => "'Source Sans Pro', sans-serif;",
		)
	);
	$wp_customize->add_control(
		'body_font',
		array(
			'label' => 'Body font CSS',
			'section' => 'typography',
			'type' => 'text',
			'priority' => 3,
		)
	);
}
add_action( 'customize_register', 'po_typography' );


/* COLORS - ACCENT
------------------------------------------------------- */ 

function po_accent_colors( $wp_customize ) {
    $wp_customize->add_section(
        'accent_colors',
        array(
            'title' => 'Colors - Accent',
            'priority' => 7,
        )
    );
	$wp_customize->add_setting(
		'accent',
		array(
			'default' => '#20C596',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'accent',
			array(
				'label' => 'Accent',
				'section' => 'accent_colors',
				'settings' => 'accent',
				'priority' => 1,
			)
		)
	);
	$wp_customize->add_setting(
		'secondary_accent',
		array(
			'default' => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'secondary_accent',
			array(
				'label' => 'Secondary accent',
				'section' => 'accent_colors',
				'settings' => 'secondary_accent',
				'priority' => 2,
			)
		)
	);
}
add_action( 'customize_register', 'po_accent_colors' );



/* COLORS - NAV
------------------------------------------------------- */ 

function po_nav_colors( $wp_customize ) {
    $wp_customize->add_section(
        'nav_colors',
        array(
            'title' => 'Colors - Nav Bar',
            'priority' => 8,
        )
    );
	$wp_customize->add_setting(
		'nav_text_custom'
	);
	$wp_customize->add_control(
		'nav_text_custom',
		array(
			'type' => 'checkbox',
			'label' => 'Use custom text colors',
			'section' => 'nav_colors',
			'priority' => 1,
		)
	);
	$wp_customize->add_setting(
		'nav_text',
		array(
			'default' => '#5B5E5F',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_text',
			array(
				'label' => 'Nav bar text',
				'section' => 'nav_colors',
				'settings' => 'nav_text',
				'priority' => 2,
			)
		)
	);
	$wp_customize->add_setting(
		'nav_text_hover',
		array(
			'default' => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_text_hover',
			array(
				'label' => 'Nav bar text hover',
				'section' => 'nav_colors',
				'settings' => 'nav_text_hover',
				'priority' => 3,
			)
		)
	);
	$wp_customize->add_setting(
		'nav_background',
		array(
			'default' => '#15191B',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_background',
			array(
				'label' => 'Background',
				'section' => 'nav_colors',
				'settings' => 'nav_background',
				'priority' => 4,
			)
		)
	);
	$wp_customize->add_setting(
		'search_background',
		array(
			'default' => '#20C596',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'search_background',
			array(
				'label' => 'Search background',
				'section' => 'nav_colors',
				'settings' => 'search_background',
				'priority' =>5,
			)
		)
	);
}
add_action( 'customize_register', 'po_nav_colors' );


/* COLORS - SLIDER
------------------------------------------------------- */ 

function po_slider_colors( $wp_customize ) {
    $wp_customize->add_section(
        'slider_colors',
        array(
            'title' => 'Colors - Slider',
            'priority' => 9,
        )
    );
	$wp_customize->add_setting(
		'slider_overlay',
		array(
			'default' => '#20C596',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slider_overlay',
			array(
				'label' => 'Overlay',
				'section' => 'slider_colors',
				'settings' => 'slider_overlay',
				'priority' => 1,
			)
		)
	);
	$wp_customize->add_setting(
		'slider_title',
		array(
			'default' => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slider_title',
			array(
				'label' => 'Title',
				'section' => 'slider_colors',
				'settings' => 'slider_title',
				'priority' => 2,
			)
		)
	);
	$wp_customize->add_setting(
		'slider_button',
		array(
			'default' => '#20C596',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slider_button',
			array(
				'label' => 'Button',
				'section' => 'slider_colors',
				'settings' => 'slider_button',
				'priority' => 3,
			)
		)
	);
	$wp_customize->add_setting(
		'slider_button_text',
		array(
			'default' => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slider_button_text',
			array(
				'label' => 'Button text',
				'section' => 'slider_colors',
				'settings' => 'slider_button_text',
				'priority' => 4,
			)
		)
	);
}
add_action( 'customize_register', 'po_slider_colors' );


/* COLORS - CONTACT
------------------------------------------------------- */ 

function po_contact_colors( $wp_customize ) {
    $wp_customize->add_section(
        'contact_colors',
        array(
            'title' => 'Colors - Contact Form',
            'priority' => 10,
        )
    );
	$wp_customize->add_setting(
		'form_borders',
		array(
			'default' => '#ddd',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'form_borders',
			array(
				'label' => 'Form borders',
				'section' => 'contact_colors',
				'settings' => 'form_borders',
				'priority' => 1,
			)
		)
	);
}
add_action( 'customize_register', 'po_contact_colors' );


/* COLORS - FOOTER
------------------------------------------------------- */ 

function po_footer_colors( $wp_customize ) {
    $wp_customize->add_section(
        'footer_colors',
        array(
            'title' => 'Colors - Footer',
            'priority' => 11,
        )
    );
	$wp_customize->add_setting(
		'footer_background',
		array(
			'default' => '#15191B',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_background',
			array(
				'label' => 'Background',
				'section' => 'footer_colors',
				'settings' => 'footer_background',
				'priority' => 1,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_headers',
		array(
			'default' => '#97999A',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_headers',
			array(
				'label' => 'Headers',
				'section' => 'footer_colors',
				'settings' => 'footer_headers',
				'priority' => 2,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_social',
		array(
			'default' => '#B9BABB',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_social',
			array(
				'label' => 'Social icons',
				'section' => 'footer_colors',
				'settings' => 'footer_social',
				'priority' => 3,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_twitter_username',
		array(
			'default' => '#3D4142',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_twitter_username',
			array(
				'label' => 'Twitter username',
				'section' => 'footer_colors',
				'settings' => 'footer_twitter_username',
				'priority' => 4,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_twitter_username_hover',
		array(
			'default' => '#797B7C',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_twitter_username_hover',
			array(
				'label' => 'Twitter username hover',
				'section' => 'footer_colors',
				'settings' => 'footer_twitter_username_hover',
				'priority' => 5,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_text',
		array(
			'default' => '#6B6E6F',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_text',
			array(
				'label' => 'Text',
				'section' => 'footer_colors',
				'settings' => 'footer_text',
				'priority' => 6,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_links',
		array(
			'default' => '#999B9C',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_links',
			array(
				'label' => 'Links',
				'section' => 'footer_colors',
				'settings' => 'footer_links',
				'priority' => 7,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_links_hover',
		array(
			'default' => '#E1E1E1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_links_hover',
			array(
				'label' => 'Links hover',
				'section' => 'footer_colors',
				'settings' => 'footer_links_hover',
				'priority' => 8,
			)
		)
	);
	$wp_customize->add_setting(
		'portfolio_hover',
		array(
			'default' => '#20C596',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'portfolio_hover',
			array(
				'label' => 'Portfolio hover',
				'section' => 'footer_colors',
				'settings' => 'portfolio_hover',
				'priority' => 9,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_bottom_background',
		array(
			'default' => '#1B2022',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bottom_background',
			array(
				'label' => 'Bottom background',
				'section' => 'footer_colors',
				'settings' => 'footer_bottom_background',
				'priority' => 10,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_bottom_text',
		array(
			'default' => '#636768',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bottom_text',
			array(
				'label' => 'Bottom text',
				'section' => 'footer_colors',
				'settings' => 'footer_bottom_text',
				'priority' => 11,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_bottom_links',
		array(
			'default' => '#818485',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bottom_links',
			array(
				'label' => 'Bottom links',
				'section' => 'footer_colors',
				'settings' => 'footer_bottom_links',
				'priority' => 12,
			)
		)
	);
	$wp_customize->add_setting(
		'footer_bottom_links_hover',
		array(
			'default' => '#D9D9DA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bottom_links_hover',
			array(
				'label' => 'Bottom links hover',
				'section' => 'footer_colors',
				'settings' => 'footer_bottom_links_hover',
				'priority' => 13,
			)
		)
	);
}
add_action( 'customize_register', 'po_footer_colors' );


?>