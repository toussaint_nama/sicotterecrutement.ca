<?php
/**
 * The template for displaying all portfolio items.
 */

include 'CorrespLogo.php';

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

$sect = $_GET['portfolio']; 
$url = $_SERVER["REQUEST_URI"];
$lang = substr($url, 1, 2);
$titre = get_the_title();
if ($lang == "en") {
    $titre = $correspTitleEn[get_the_title()];
}
$catid = $correspCatId[$titre]; 

$args = array(  'posts_per_page'   => -1,
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'post_type'        => 'rsjp_job_postings',
                'cat'    => $catid,
                'post_status'      => 'publish');
$offres = get_posts( $args );
$offreId = "";
$offreNom = "";
$offreLieu = "";
$offreContrat = "";
$offreStatut = "";
$offreLink = "";
$checkColor = "";

$categorie = $correspCatId[$titre];  
/*
$argsEvent = array( 'posts_per_page'   => -1,
                    'category_name'    => $titre,
                    'category_and'     => array( 94, $categorie ),
                    'orderby'          => 'post_date',
                    'order'            => 'DESC',
                    'post_status'      => 'publish' );
$events = get_posts( $argsEvent );
*/

echo do_shortcode("[nav_bar_noslide slider='none']");	?>

<meta property="og:image" content="http://sicotterecrutement.ca/wp-content/themes/nibiru/images/partage/<?php echo $correspLogo[$titre] ?>.png" />

    
    <div class="po-page">

        <?php 
		global $page_mb;
		if ( 'show' == $page_mb->get_the_value( 'cb_single' ) ) { ?>	                           
            <div class="entry-header po-portfolio-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="text-right" style="margin-top:26px;"><?php if (function_exists('po_breadcrumbs')) po_breadcrumbs(); ?></div>
                    </div>
                    
                    </div>
                </div>
            </div>
		<?php } ?>
        
        <div class="row po-full-width">
        <div class="portfolio-page col-sm-12 column-12" style="background-image: none !important;">
            <div class="liquidcontainerpage">
                <div class="titleHead col-sm-10">
                    <div class="logoTitle">
                        <span class="icon-<?php echo $correspLogo[$titre]; ?>" style="font-size: 70px; color: #f4545f;"></span>
                    </div>
                    <div class="titleTitle">
						<h3 style="color: #848484;"><?php the_title(); ?></h3>
                    </div>
                    <div class="reseauxTitle">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                        <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                        <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                    </div>
                    <div class="buttonsTitle">
                        <button class="indusButton buttonTitle">
                            <h4 style="font-size: 14px !important;"><?php echo __("Industrie"); ?></h4>
                        </button>
                        <button class="actuButton buttonTitle">
                            <h4 style="font-size: 14px !important;"><?php echo __("Actualités"); ?></h4>
                        </button>
                        <button class="eventButton buttonTitle">
                            <h4 style="font-size: 14px !important;"><?php echo __("Événements"); ?></h4>
                        </button>
                        <button class="jobButton buttonTitle">
                            <h4 style="font-size: 14px !important;"><?php echo __("Offres d'emploi"); ?></h4>
                        </button>
                        <a href="http://sicotterecrutement.ca/offres-demploi/">
                        <button class="listButton buttonTitle">
                            <h4 style="font-size: 14px !important;"><?php echo __("Toutes nos offres"); ?></h4>
                        </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    	</div>
            
		<?php while ( have_posts() ) : the_post(); ?>
        <?php global $page_mb;
				if ( 'hide' == $page_mb->get_the_value( 'top_padding' ) ) {
				?>	
            	<div class="container">
                <?php 
            	} else { ?>
                <div class="container page-padding">
                <?php 
				}
				?>

            <div class="row" style="margin-bottom: 10%;">

                <?php global $page_mb; ?>

                <div class="content col-sm-9">
                    <div class="indusContent" style="text-align: justify;">
                        <?php 
                        if ( $titre == "Environnement" ) { ?>
                            <div class="col-md-4" style="border: 4px solid; border-color: #f4545f; margin-right: 3%; margin-bottom: 2%;">
                                <p><?php echo __("Parce que chaque petit geste compte, SICOTTE Recrutement en collaboration avec We Forest (<a href='www.weforest.org'>www.weforest.org</a>) s’engage à planter 40 arbres chaque fois que nous procédons au recrutement de l’un de nos candidats dans les secteurs de l’environnement et du développement durable."); ?></p>
                                <img src="http://sicotterecrutement.ca/wp-content/themes/nibiru/images/LogoWeForest300x173.jpg" style="max-width: 65%; margin-left: 17.5%; margin-bottom: 5%;">
                            </div>
                        <?php } ?>
                        <?php echo the_content(); ?>
                    </div>
                    <div class="actuContent">
                        <div>
                            <?php 
                            if ( $lang == "en" ) {
                                echo $correspSpundgeEn[$titre];
                            } else {
                                echo $correspSpundge[$titre];
                            } 
                            ?>
                        </div>
                    </div>
                    <div class="eventContent">
                        <!--
                        <?php foreach ($events as $event) {
                            $eventID = $event->ID;
                            $eventNom = $event->post_title; 
                            $eventLieu = get_post_meta($eventID, 'jp_lieu', true);
                            $eventDeb = get_post_meta($eventID, 'jp_datDeb', true);
                            $eventFin = get_post_meta($eventID, 'jp_datFin', true);
                            $eventContent = $event->post_content;
                            $eventImage = wp_get_attachment_image_src(get_post_thumbnail_id ( $eventID ), 'medium');
                            ?>
                            <div class="offre offre-<?php echo $offreId; ?>" style="width: 100%;">
                                <div class="col-md-3" style="margin-top: 4%;">
                                    <img class="img-responsive" src="<?php echo $eventImage[0]; ?>">
                                </div>
                                <div class="col-md-9">
                                    <h3 style="width: 100%; color: #4d4d4d; font-weight: bold;"><?php echo $eventNom; ?></h3>
                                    <h4 style="width: 100%";><?php echo $eventContent; ?></h4>
                                    <h5 class="detOffreSide" style="float: left; width: 100%;"><?php echo __("lieu"); ?>: <b style="color: #4d4d4d;"><?php echo $eventLieu; ?></b></h5>
                                    <h5 class="detOffreSide" style="float: left;"><?php echo __("Date de debut"); ?>: <b style="color: #4d4d4d;"><?php echo $eventDeb; ?></b></h5>
                                    <h5 class="detOffreSide" style="float: right;"><?php echo __("Date de fin"); ?>: <b style="color: #4d4d4d;"><?php echo $eventFin; ?></b></h5>
                                </div>
                                <div class="col-md-12" style="height: 1px; background-color: #f4545f; width: 80%; text-align: center; margin-left: 10%; margin-bottom: 10px; margin-top: 5px;"></div>
                            </div>
                        <?php } ?>
                        -->
                        <div>
                            <?php 
                            if ( $lang == "en" ) {
                                echo $correspEventEn[$titre];
                            } else {
                                echo $correspEvent[$titre];
                            } 
                            ?>
                        </div>
                    </div>
                    <div class="jobContent">
                        <?php foreach ($offres as $offre) {
                                $offreId = $offre->ID;
                                $offreNom = $offre->post_title; 
                                $offreName = $offre->post_name;
                                $offreLink = $offre->guid;
                                $offreLieu = get_post_meta($offreId, 'jp_lieu', true);
                                $offreContrat = get_post_meta($offreId, 'jp_type', true);
                                $offreStatut = get_post_meta($offreId, 'jp_statut', true);
                                $offreDate = substr($offre->post_date,0,10);
                                if (($offreStatut == "Comblé")||($offreStatut == "Combl&eacute")) {
                                    $checkColor = "#f4545f";
                                } elseif ($offreStatut == "En Entrevue") {
                                    $checkColor = "#ffa500";
                                } elseif ($offreStatut == "Non Disponible") {
                                    $checkColor = "lightgrey";
                                } else { $checkColor = "#1abf52"; }
                                if ( $lang == "en" ) {
                                    $offreLink = "http://sicotterecrutement.ca/en/job-postings/".$offreName."/";
                                }
                                ?>
                                <div class="offre offre-<?php echo $offreId; ?>" style="width: 100%;">
                                    <a href="<?php echo $offreLink; ?>">
                                    <div class="col-md-3" style="margin-top: 4%;">
                                        <span class="icon-valide fa-4x" style="color: <?php echo $checkColor; ?>"></span>
                                    </div>
                                    <div class="col-md-9">
                                        <h4 style="width: 100%; color: #4d4d4d; font-weight: bold;"><?php echo $offreNom; ?></h4>
                                        <h5 class="detOffreSide" style="float: left; width: 35%;"><?php echo __("lieu"); ?>: <b style="color: #4d4d4d;"><?php echo $offreLieu; ?></b></h5>
                                        <h5 class="detOffreSide" style="float: left; width: 65%;"><?php echo __("contrat"); ?>: <b style="color: #4d4d4d;"><?php echo $offreContrat; ?></b></h5>
                                       <!--  <h5 class="detOffreSide" style="float: left; width: 35%;">Date: <b style="color: #4d4d4d;"><?php // echo $offreDate; ?></b></h5> -->
                                        <h5 class="detOffreSide" style="color: <?php echo $checkColor; ?>; float: left;"><?php echo $offreStatut; ?></h5>
                                    </div>
                                    </a>
                                    <div class="col-md-12" style="height: 1px; background-color: #f4545f; width: 80%; text-align: center; margin-left: 10%; margin-bottom: 10px; margin-top: 5px;"></div>
                                </div>
                            <?php } ?>
                    </div>
                </div>

                <div class="col-sm-3">
                    <?php get_sidebar(); ?>
                    <div class="titleSide">
                        <h4 style="text-aligne: left; font-weight: bold;"><?php echo __("Offres"); ?></h4>
                    </div>
                    <div class="offresSide">
                        
                        <div class="offresListe">
                            <?php 
                            $countOffres = count($offres);
                            if ($countOffres <= 4) {
                                $max = $countOffres;
                            } else {
                                $max = 5;
                            }
                            for ($i=0; $i < $max; $i++) {
                                $offre = $offres[$i];
                                $offreId = $offre->ID;
                                $offreNom = $offre->post_title; 
                                $offreLink = $offre->guid;
                                $offreLieu = get_post_meta($offreId, 'jp_lieu', true);
                                $offreContrat = get_post_meta($offreId, 'jp_type', true);
                                $offreStatut = get_post_meta($offreId, 'jp_statut', true);
                                if (($offreStatut == "Comblé")||($offreStatut == "Combl&eacute")) {
                                    $checkColor = "#f4545f";
                                } else { $checkColor = "#1abf52"; }
                                ?>
                                <div class="offre offre-<?php echo $offreId; ?>" style="width: 100%;">
                                    <a href="<?php echo $offreLink; ?>">
                                    <div class="col-md-3" style="margin-top: 10%;">
                                        <span class="icon-valide fa-2x" style="color: <?php echo $checkColor; ?>"></span>
                                    </div>
                                    <div class="col-md-9">
                                        <h5 style="width: 100%; color: #4d4d4d; font-weight: bold;"><?php echo $offreNom; ?></h5>
                                        <h6 class="detOffreSide"><?php echo __("lieu"); ?>: <b style="color: #4d4d4d;"><?php echo $offreLieu; ?></b></h6>
                                        <h6 class="detOffreSide"><?php echo __("contrat"); ?>: <b style="color: #4d4d4d;"><?php echo $offreContrat; ?></b></h6>
                                        <h6 class="detOffreSide" style="color: <?php echo $checkColor; ?>"><?php echo $offreStatut; ?></h6>
                                    </div>
                                    </a>
                                    <div class="col-md-12" style="height: 1px; background-color: black; width: 80%; text-align: center; margin-left: 10%; margin-bottom: 10px; margin-top: 5px;"></div>
                                </div>
                            <?php } ?>
                        </div>

                        <a href="<?php echo ("".get_site_url()."/offres-demploi/"); ?>"><div class="boutonToutSide col-md-12" style="width: 70%;"><h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Toutes nos offres"); ?></h5></div></a>

                        <div class="textSide col-md-12" style="margin-top: 40px;">
                            <div style="margin-top: 15px; margin-bottom: 15px;">
                                <h4 style="font-weight: bold;"><?php echo __("Pas d'offres pour vous?"); ?></h4>
                                <h6><?php echo __("Aucune offre ne correspond à votre profil, faites nous"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("parvenir votre CV"); ?></a><?php echo __(' et remplissez '); ?><a href="http://sicotterecrutement.ca/fiche-candidat/" style="color: blue;"><?php echo __('la fiche de renseignement candidats'); ?></a></h6>
                            </div>
                            <div>
                                <h4 style="font-weight: bold;">Infos SICOTTE Recrutement</h4>
                                <h6><?php echo __("Vous avez besoin de plus d'informations?"); ?> <a href="mailto:info@sicotterecrutement.ca" style="color: blue;"><?php echo __("Ecrivez-nous un courriel"); ?></a></h6>
                            </div>
                        </div>

                    </div>
                </div>	

            </div>

        </div>

        <div style="background-color: rgb(245,245,245); width: 200px; margin-left: auto; margin-right: auto;">
            <?php echo do_shortcode("[button style='top' color='light' link='".get_post_type_archive_link( 'portfolio' )."' icon='fa-folder-open-o' width='200']". __('Toutes les industries','pixelobject')."[/button]"); ?>           
        </div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>
        
        <?php 
        // get the custom post type's taxonomy terms
          
        $custom_taxterms = wp_get_object_terms( $post->ID, 'portfolio_categories', array('fields' => 'ids') );
        // arguments
        $args = array(
        'post_type' => 'portfolio',
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'portfolio_categories',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array ($post->ID),
        );
        $related_items = new WP_Query( $args );

        // Reset Post Data
        wp_reset_postdata();
        ?>
           
		<?php endwhile; // end of the loop. ?>

<script type="text/javascript">

    jQuery(".actuContent").hide();
    jQuery(".eventContent").hide();
    jQuery(".indusContent").hide();
    jQuery(".collaborators").hide();
    jQuery(".jobButton").css("background-color", "rgb(215,215,215)");

    jQuery(".indusButton").click(function(){
        jQuery(".indusContent").show();
        jQuery(".actuContent").hide();
        jQuery(".eventContent").hide();
        jQuery(".jobContent").hide();
        jQuery(".indusButton").css("background-color", "rgb(215,215,215)");
        jQuery(".actuButton").css("background-color", "rgb(245,245,245)");
        jQuery(".eventButton").css("background-color", "rgb(245,245,245)");
        jQuery(".jobButton").css("background-color", "rgb(245,245,245)"); 
    });
    jQuery(".actuButton").click(function(){
        jQuery(".indusContent").hide();
        jQuery(".actuContent").show();
        jQuery(".eventContent").hide();
        jQuery(".jobContent").hide();
        jQuery(".actuButton").css("background-color", "rgb(215,215,215)");
        jQuery(".indusButton").css("background-color", "rgb(245,245,245)");
        jQuery(".eventButton").css("background-color", "rgb(245,245,245)");
        jQuery(".jobButton").css("background-color", "rgb(245,245,245)"); 
    });
    jQuery(".eventButton").click(function(){
        jQuery(".indusContent").hide();
        jQuery(".actuContent").hide();
        jQuery(".eventContent").show();
        jQuery(".jobContent").hide();
        jQuery(".eventButton").css("background-color", "rgb(215,215,215)");
        jQuery(".actuButton").css("background-color", "rgb(245,245,245)");
        jQuery(".indusButton").css("background-color", "rgb(245,245,245)");
        jQuery(".jobButton").css("background-color", "rgb(245,245,245)");  
    });
    jQuery(".jobButton").click(function(){
        jQuery(".indusContent").hide();
        jQuery(".actuContent").hide();
        jQuery(".eventContent").hide();
        jQuery(".jobContent").show();
        jQuery(".jobButton").css("background-color", "rgb(215,215,215)");
        jQuery(".actuButton").css("background-color", "rgb(245,245,245)");
        jQuery(".indusButton").css("background-color", "rgb(245,245,245)");
        jQuery(".eventButton").css("background-color", "rgb(245,245,245)");  
    });

</script>

<style type="text/css">
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    .buttonTitle {
        width: 150px;
        margin-left: -3px !important;
        margin-right: -3px !important;
        padding-left: 1.5%;
        padding-right: 1.5%;
        background-color: rgb(245,245,245);
        border-radius: 4px;
        border: 1px solid;
        border-color: rgb(231,231,231);
    }

    .buttonTitle:hover {
        background-color: rgb(215,215,215);
    }

    .logoTitle {
        margin-top: 4%;
    }

    #search-2 > form > input { background-color: white; }

    body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width > div > div { height: 240px; }

    .content {
        background-color: rgb(245,245,245);
        height: 100%;
    }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(5) > b,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(8) > strong,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(11) > strong,
    body > div.po-nav.nav-fixed-padding > div > div.container.page-padding > div > div.content.col-sm-9 > p:nth-child(15) > strong { color: #f4545f; }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    .indusContent {
        padding: 25px;
    }

    #saved-articles-main > p, .collaborators {
        display: none !important;
    }

    .boutonToutSide {
        border: 1px solid;
        border-color: #f4545f;
        background-color: rgb(245,245,245);
        margin-left: 15%;
        margin-top: 40px;
    }

    ul, li {
        list-style: inherit;
    }

    #nblikefacebook {
        zoom: 100% !important;
    }

    .indusContent > p > a {
        word-wrap: break-word;
    }

    @media (max-width: 330px) {
        body > div.entry-content > div > div > div > div:nth-child(2) > div > div.icon-bg > div.text-center.float-text > p > a {
            font-size: 17px;
        }
        body > div.po-nav.nav-fixed-padding > div > div.container.page-padding {
            padding-top: 125px;
        }
    }

</style>

<?php get_footer(); ?>

