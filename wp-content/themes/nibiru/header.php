<?php
/**
 * The header for Nibiru.
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title>SICOTTE RECRUTEMENT | Agence de recrutement à Montréal</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
<link rel="icon" href="/favicon.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="http://www.google.com/fonts#ChoosePlace:select/Collection:Open+Sans">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/font/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/hover.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/font/specialelite_regular_macroman/stylesheet.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/font/ArchitectsDaughter/stylesheet.css" type="text/css" charset="utf-8" />
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:creator" content="@author_handle">
<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/images/meta-logo.jpg">

<!-- Open Graph data -->
<meta property="og:title" content="<?php the_title(); ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="http://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>" />
<meta property="og:site_name" content="SICOTTE Recrutement, le Point entre Être et Devenir" />
<?php
// you only need home, because you can edit the front page via wp-admin
if (  is_front_page()  ) { ?>
<meta name="description" content="SICOTTE Recrutement - Agence de recrutement et chasseur de têtes à Montréal proposant des offres d'emplois pour cadres, ingénieurs, techniciens en ressources humaines, marketing, communication, informatique, etc." />
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/meta-logo-wall.jpg" />
<?php } elseif ( is_singular( 'team' ) ) { ?>
<meta name="description" content="<?php the_excerpt(); ?>" />
<meta property="og:image" content="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" />	
<?php } elseif ( is_singular( 'portfolio' ) ) { ?>
<meta name="description" content="<?php the_excerpt(); ?>" />
<?php } else { ?>
<meta name="description" content="<?php the_excerpt(); ?>" />
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/meta-logo-wall.jpg" />
<?php } ?>

<?php 
$openOffers = new WP_Query( "post_type=rsjp_job_postings&meta_key=jp_statut&meta_value=En Recrutement&order=ASC" );
$countOpen = $openOffers->found_posts;
?>

<div id="vignetteJob" style="width: 20px; height: 20px; background-image: url('<?php echo get_template_directory_uri(); ?>/images/vignette.png'); float: right; margin-top: -10px; background-size: contain; color: #f4545f;"><?php echo $countOpen; ?></div>

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>

</head>

<?php 
$url = $_SERVER["REQUEST_URI"];
$lang = substr($url, 1, 2);
if ( $lang == "en" ) { ?>
	<script type="text/javascript">
		jQuery(function($){
			$('.logo.center-block').attr("src","http://sicotterecrutement.ca/wp-content/uploads/2015/07/LOGO-SICOTTE-ENG1.png");
			$('#menu-item-820 > a').attr("href", "http://sicotterecrutement.ca/en/");
		});
	</script>	
<?php } ?>

<script type="text/javascript">
	
jQuery(function($){

$("#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current > ul").hide();
$("#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current > ul").hide()

$("#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current").mouseenter(function(){
	$("#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current > ul").show();
});
$("#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current").mouseleave(function(){
	$("#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current > ul").hide();
});

$("#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current").mouseenter(function(){
	$("#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current > ul").show();
});
$("#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current").mouseleave(function(){
	$("#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current > ul").hide();
});

jQuery("#menu-item-312 > a").append(jQuery("#vignetteJob"));
jQuery("#menu-item-944 > a").append(jQuery("#vignetteJob"));

jQuery(".navbar-collapse").append(jQuery("#boutonPanier"));

});

</script>

<style type="text/css">
	
#menu-menu-1 > li.menu-item.menu-item-language.menu-item-language-current {
	left: 60%;
}

#menu-menu-anglais > li.menu-item.menu-item-language.menu-item-language-current {
	left: 90%;
}

.boutonPanierDiv {
	position: absolute;
	top: 60px;
	right: 25px;
	padding-right: 5px;
	padding-left: 5px;
}

</style>

<body <?php body_class(); ?>>