<?php
/*
Template Name: Conseils Page
*/

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

echo do_shortcode("[nav_bar_noslide slider='none']");   ?>
    
    <div class="po-page">

        <div class="title" style="text-align: center; min-height: 215px;">

            <div style="padding-top: 4%;">
                <span class="icon-Sicotte logTitle" style="font-size: 60px;"></span>
                <span class="icon-cle logTitle" style="font-size: 30px;"></span>
            </div>
            <h1 style="color: #848484;"><?php echo __("Conseils"); ?></h1>
            <div class="reseauxTitle" style="padding-bottom: 3%;">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
            </div>
        </div>

            <div class="row">

                <?php global $page_mb; ?>

                <div class="page col-md-9" style="margin-bottom: 5%; margin-left: 10%; margin-right: 10%;">
                 
                    <div class="content col-md-9" style="padding: 60px;">

                            <?php echo the_content(); ?>

                            <h4 style="font-weight: bolder; text-align: center;"><?php echo __("Partager sur les réseaux sociaux"); ?></h4>

                            <div class="reseauxTitle" style="text-align: center;">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                            </div>
                        
                    </div>

                    <!-- Sidebar -->
                    <div class="col-md-3">

                        <?php get_sidebar(); ?>

                        <a href="<?php echo ("".get_site_url()."/offres-demploi/"); ?>">
                        <div class="boutonToutSide col-md-12" style="width: 100%; margin-top: -40px;">
                            <h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Voir toutes nos offres"); ?></h5>
                        </div>
                        </a>

                        <a href="<?php echo ("".get_site_url()."/?post_type=portfolio"); ?>">
                        <div class="boutonToutSide col-md-12" style="width: 100%; margin-top: 15px;">
                            <h5 class="boutonToutSideTexte" style="color: #f4545f; text-align: center; font-weight: bold;"><?php echo __("Voir toutes nos Catégories"); ?></h5>
                        </div>
                        </a>

                    </div>  

                </div>


            </div>

            <!-- Bouton reseaux sociaux -->
        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-youtube"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-facebook" size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-linkedin"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" icon-viadeo"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-pinterest"  size="20"][/column]
                                    [column width="2" animation="pull-up"] [iconboxsic icon=" fa-twitter"  size="20"][/column] ' ); ?>

        <div class="clearfix"></div>
        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title="'.__("Appelez-nous").'"]'.__("Notre équipe est prête à vous répondre").': [text align="center" ]<br/>'.__("au").' <a href="tel:+15143601304">514-360-1304</a> '.__("les Lundi-Vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title="'.__("Ecrivez-nous").'"] '.__("Envoyez nous un courriel à").':  [text align="center" size="15"]<br/><a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title="'.__("Venez nous voir").'"] '.__("Notre équipe vous accueille durant nos horaires de bureau").': [text align="center" ]<br/>'.__("Lundi-Vendredi, 9am-6pm, au 129 rue de la Commune Est, H2Y 1J1, Montréal").' [/text] [/iconbox][/column]

                                    [/section]'); ?>

        </div>
            
<style type="text/css">
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .logTitle {
        color: #f4545f;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    #search-2 > form > input { background-color: white; }

    .content {
        background-color: #f0f0f0;
    }

    .icon-box { zoom: 200%; }

    .boutonToutSide {
        border: 1px solid;
        border-color: #f4545f;
        background-color: rgb(245,245,245);
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    body > div.container.po-container-section {
        visibility: hidden;
    }

    hr { border-color: #848484; }

    body > div.container.po-container-section > div {
        position: fixed;
    }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    body > div.po-nav.nav-fixed-padding > div > div.row > div > div.content.col-md-9 > p:nth-child(6) > span > strong {
        font-size: 20px;
        font-style: oblique;
    }

    body > div.po-nav.nav-fixed-padding > div > div.row > div > div.content.col-md-9 > p:nth-child(1) > span > strong {
        font-size: 30px;
    }

</style>

<?php get_footer(); ?>

