<?php
/**
 * The template for displaying all team members.
 */

include 'ArticlesConferenciers.php';

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

session_start();

echo do_shortcode("[nav_bar_noslide slider='none']");
	?>




<!-- Style place au debut pour accelerer le chargement de la page et son affichage -->
<style type="text/css">
    
.row {
    margin-left: 5%;
    margin-right: 5%;
}

.po-page {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg');
}

.textContent {
    -webkit-column-count: 3; /* Chrome, Safari, Opera */
    -moz-column-count: 3; /* Firefox */
    column-count: 3;
    -webkit-column-gap: 40px; /* Chrome, Safari, Opera */
    -moz-column-gap: 40px; /* Firefox */
    column-gap: 40px;
    text-align: justify;
    min-height: 270px;
    margin-top: 1%;
    overflow: auto;
}

.articleContent {
    text-align: justify;
    min-height: 270px;
    margin-top: 1%;
}

.textTitle {
    margin-top: 2%;
    margin-left: 1%;
}

.boutonConf {
    border: 1px solid;
    border-color: #848484;
    color: #f4545f;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    text-align: center;
    zoom: 150%;
    width: 58%;
    margin-left: 21%;
}

.boutonConf:hover {
    color: white;
    background-color: #f4545f;
    border-color: #848484;
}

.textBottom {
    text-align: center;
    margin-top: 22%;
    margin-bottom: 2%;
}

.rowUp, .rowDownm, .rowMid {
    margin-top: 2%;
}

.rowDown {
    margin-bottom: 5%;
}

.rowMid {
    text-align: -webkit-center;
    padding-bottom: 10px;
}

body > div.po-nav.nav-fixed-padding > div.po-page > div.row > div.col-md-12.rowUp > div.photo.col-md-4 > a > img {
    width: 95%;
    height: 95%;
}

.icon-box { zoom: 200%; }

.domaineText {
    font-size: 9px;
    font-weight: bold;
    color: #4d4d4d;
}

.articleText {
    color: #3f94e0;
    font-size: 20px;
    line-height: 40px;
}

body > div.container.po-container-section > div {
    position: fixed;
}

.footer-container, .footer-container-bottom {
    position: fixed;
}

body > div.po-nav.nav-fixed-padding > div > div.row > div.col-md-12.rowUp > div.sidebar.col-md-4 > a > div {
    width: 80%;
    margin-left: 10%;
}

#primary-sidebar {
    max-width: 80%;
    margin-left: 10%;
}

body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p, 
body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
    color: #F4545F;
}

.iResTitle {
    font-size: 26px;
    color: #f4545f;
    margin-left: 20px;
    margin-right: 20px;
    margin-bottom: 1%;
    margin-top: 1%;
}

.reseauxTitle {
    margin-bottom: 1%;
}

/*body > div.entry-content {
    display: none !important;
}*/

body > div.po-nav.nav-fixed-padding > div > div.title > h1 {
    margin-top: 0px !important;
}

#myCarousel > ol > li.active {
    background-color: #f4545f;
}
#myCarousel > ol > li {
    background-color: darkgray;
}

.photo {
    background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>");
    background-size: cover;
    background-position-x: 50%;
    background-position-y: 50%;
    min-height: 360px;
}

.boutonConf > a:hover {
	color: white;
}

.boutonRondPanier {
	height: 90px;
	width: 90px;
	text-align: center;
	border: 1px solid;
	border-radius: 50%;
	background-color: #f0f0f0;
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red-round.png');
	background-size: contain;
	border-color: #f4545f;
	display: inline-block;
}

.boutonRondPanier:hover {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-white-round.png');
	background-color: #f4545f;
}

.boutonRondEnlever {
	height: 90px;
	width: 90px;
	text-align: center;
	border: 1px solid;
	border-radius: 50%;
	background-color: #f0f0f0;
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-remove-red-round.png');
	background-size: contain;
	border-color: #f4545f;
	display: inline-block;
}

.boutonRondEnlever:hover {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-remove-white-round.png');
	background-color: #f4545f;
}

#nblikefacebook {
    zoom: 100% !important;
}

.boutonAjoutPanier {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red2.png');
	background-size: contain;
	background-repeat: no-repeat;
}

.boutonAjoutPanier:hover {
	background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red2-hover.png');
	background-size: contain;
	background-repeat: no-repeat;
}

.boutonRetraitPanier {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red-remove.png');
    background-size: contain;
    background-repeat: no-repeat;
}

.boutonRetraitPanier:hover {
    background-image: url('<?php echo get_template_directory_uri(); ?>/images/cart-brain-red-remove-hover.png');
    background-size: contain;
    background-repeat: no-repeat;
}

.container, .row {
	padding: 0px;
	margin-left: 0px;
	margin-right: 0px;
}

body > div.entry-content > div > div {
	margin-left: auto;
	margin-right: auto;
}

body > div.container.po-container-section > div {
	position: relative !important;
}

body > div.container.po-container-section {
	margin-right: auto;
	margin-left: auto;
}

body > div.po-nav.nav-fixed-padding > div > div.container.header-conf > div > div.col-md-4.barre-recherche > div > form > input {
    color: darkgray;
}

</style>



    
    <div class="po-page">

        <div class="container header-conf" style="width: 100%; background-color: black; height: 120px; margin-bottom: 65px;">
            <div class="row" style="width: 60%; margin-left: auto; margin-right: auto;">
            	<div class="col-md-6" style="padding-left: 0px;">
            		<img src="<?php echo get_template_directory_uri(); ?>/images/Logo-SIC-Talk-blanc.png" style="max-height: 90px; float: left; margin-top: 15px;">
            	</div>
            	<div class="col-md-4 barre-recherche" style="margin-top: 40px; height: 45px; padding-right: 0px;">
                    <div style="width: 170px; float: right;">
                        <?php echo do_shortcode('[wpbsearch]'); ?>
                    </div>
            	</div>
                <?php if ( isset($_SESSION["produits"]) ) { ?>
            	    <div class="col-md-2" style="float: right; margin-top: 40px; padding-right: 0px;">
                        <a href="http://sicotterecrutement.ca/panier-conferenciers/"><div style="background-color: #f4545f; border: 1px solid; border-color: white; color: white; height: 39px; width: 140px; padding-left: 10px; float: right; padding-top: 6px;">
                			<?php echo __('Mes conférenciers'); ?>	
                		</div></a>
            	    </div>
                <?php } else { ?>
                    <style type="text/css">
                        .barre-recherche {
                            float: right;
                        }
                    </style>
                <?php }?>
            </div>
        </div>

        <div class="page-content container " style="width: 60%; margin-left: auto; margin-right: auto;">
            
            <div class="menu-sup row" style="">
                <div class="" style="float: right; text-align: right; padding-right: 0px; margin-bottom: 65px;">
                    <a href=""><h3 style="color: #808080 !important; font-weight: bold; max-width: 25%; float: right; margin-left: 20px;">Contact</h3></a>
                    <a href=""><h3 style="color: #808080 !important; font-weight: bold; max-width: 25%; float: right; margin-left: 20px;">Blog</h3></a>
                    <a href=""><h3 style="color: #808080 !important; font-weight: bold; max-width: 25%; float: right; margin-left: 20px;"><?php echo __('À propos') ?></h3></a>
                    <a href=""><h3 style="color: #808080 !important; font-weight: bold; float: right; margin-left: 20px;"><?php echo __('Conférenciers') ?></h3></a>
                </div>
            </div>

            <div class="img-conferencier row" style="padding: 0px; width: 100%;">
                <?php
                $video = get_post_meta( get_the_ID(), 'jp_videoTeam', true );
                if ( $video != "") { ?>
                    <a class="bouton-play"><img src="<?php echo get_template_directory_uri(); ?>/images/play-bouton.png" style="height: 50px; width: 50px; margin-top: 10px; position: absolute; right: 21%;"></a>
                    <iframe width="500" height="500" src="https://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen style="width: 100%; height: 500px; display: none;"></iframe>
                <?php } ?>
                <img id="photo-conferencier" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" style="min-width: 100%;">
                
            </div>

            <div class="row info-sup" style="padding-left: 0px; width: 100%;">
            	
                <div class="col-md-8" style="padding-left: 0px;">
            		<h1 style="padding-top: 4%; color: #848484; font-weight: bold; font-size: 50px;"><?php the_title(); ?></h1>
                    <?php $poste = get_post_meta( get_the_ID(), 'jp_poste', true); ?>
                    <h2 style="color: darkgray; margin-top: 0px;"><?php echo $poste; ?></h2>
            		<div class="reseauxTitle" style="float: left; width: 100%;">
		                <a href='http://www.facebook.com/sharer.php?s=100&p[title]=<?php the_title(); ?>&p[summary]=<?php the_content() ?>&p[url]=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>&p[images][0]=<?php the_post_thumbnail("full"); ?>' ><span class="fa fa-facebook iResTitle"></span></a>
		                <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>&text=<?php the_excerpt(); ?>"><span class="fa fa-twitter iResTitle"></span></a>
		                <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
		                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
		            </div>
		            <?php 
		            $contenu = get_the_content($post->ID);
		            $resume = wp_trim_words( $contenu, $num_words = 90, $more = '[...]' );
		            ?>
		            <p style="text-align: justify;"><?php echo $resume; ?></p>
		            <a href="#bio"><div style="width: 35%; background-color: #cccccc; border: 1px solid; border-color: #f4545f; border-radius: 2px; text-align: center;"><?php echo __('Plus d\'informations') ?></div></a>
            	</div>

            	<div class="col-md-4" style="margin-top: 15px; padding-top: 4%;">
                    
                    <?php 
                    $confId = get_the_ID();
                    $url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                    $confId = get_the_ID();
                    $classe = "boutonAjoutPanier";
                    $titreBouton = "Ajouter à mes conférenciers";
                    if ( isset($_SESSION["produits"]) ) {
                        $conferenciers = $_SESSION["produits"];
                        $found = false;
                        foreach ( $conferenciers as $conferencier ) {
                            if ( $conferencier == $confId ) { 
                                $found =true; ?>
                            <?php }
                        }
                        if ( $found == true ) { 
                            $classe = "boutonRetraitPanier"; ?>
                            <a href="<?php echo get_template_directory_uri(); ?>/panier-update.php?remove=<?php echo $confId; ?>&returnUrl=<?php echo $url; ?>">
                            <div class="boutonRetraitPanier hvr-bounce-in" style="height: 90px; width: 90px; float: left;"></div>
                            </a>
                            <div class="" style="text-align: left; margin-left: 110px; padding: 0px;">
                                <h4 style="margin-bottom: 0px; color: #f4545f; font-size: 27px;"><?php echo __('Retirez') ?></h4>
                                <h4 style="margin-top: 0px; color: #f4545f; font-size: 27px;"><?php echo __('ce conférencier') ?></h4>
                            </div>
                        <?php } else { ?>
                            <a href="#"  onclick="document.forms['formAdd'].submit();">
                            <div class="boutonAjoutPanier hvr-bounce-in" style="height: 90px; width: 90px; float: left;"></div>
                            </a>
                            <div class="" style="text-align: left; margin-left: 110px; padding: 0px;">
                                <h4 style="margin-bottom: 0px; color: #f4545f; font-size: 27px;"><?php echo __('Ajoutez') ?></h4>
                                <h4 style="margin-top: 0px; color: #f4545f; font-size: 27px;"><?php echo __('ce conférencier') ?></h4>
                            </div>
                        <?php } 
                    } else { ?>
                        <a href="#"  onclick="document.forms['formAdd'].submit();">
                        <div class="boutonAjoutPanier hvr-bounce-in" style="height: 90px; width: 90px; float: left;"></div>
                        </a>
                        <div class="" style="text-align: left; margin-left: 110px; padding: 0px;">
                            <h4 style="margin-bottom: 0px; color: #f4545f; font-size: 27px;"><?php echo __('Ajoutez') ?></h4>
                            <h4 style="margin-top: 0px; color: #f4545f; font-size: 27px;"><?php echo __('ce conférencier') ?></h4>
                        </div>
                    <?php } ?>


                    <form name="formAdd" type="post" action="<?php echo get_template_directory_uri(); ?>/panier-update.php">  
                        <input type="hidden" name="type" value="add">
                        <input type="hidden" name="id" value="<?php echo $confId; ?>">
                        <input type="hidden" name="returnUrl" value="<?php echo $url; ?>">
                    </form>

                    <!--
            		<div class="boutonAjoutPanier" style="height: 90px; width: 90px; float: left;"></div>
            		<div class="" style="text-align: left; margin-left: 110px; padding: 0px;">
            			<h4 style="margin-bottom: 0px; color: #f4545f; font-size: 27px;"><?php echo __('Ajoutez') ?></h4>
            			<h4 style="margin-top: 0px; color: #f4545f; font-size: 27px;"><?php echo __('ce conférencier') ?></h4>
            		</div>
                    -->

            		<h3 style="width: 100%;"><?php echo __('Domaines d\'éxpertise') ?></h3>
            		<div class="col-md-12" style="padding-left: 0px;">
            			<?php 
            			$domTitre = get_post_meta( get_the_ID(), 'jp_domaineTitre', true );
            			$domCount = count($domTitre);
            			for ($i=0; $i < $domCount; $i++) { ?>
            				<span class="fa fa-check-circle-o" style="width: 6%; color: #cccccc;"></span>
            				<h5 style="width: 94%; color: #f4545f; display: initial; font-weight: bold;"><?php echo $domTitre[$i]; ?></br></h5>
            			<?php }
            			?>
            		</div>
            	</div>
            </div>

        </div>

        <div class="temoignages container" style="width: 100%;">
            <?php 
            $temoignages = get_post_meta(get_the_ID(), 'jp_temoignage', true);
            $temCount = count($temoignages);
            if ($temCount >= 1) { ?>
            <div class="row rowMid rowTenoignage" style="background-color: #f4545f; margin-bottom: 2%; width: 100%;">
                
                <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 80% !important;">
                  <!-- Indicators -->
                  <ol class="carousel-indicators" style="bottom: -22px;">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <?php if ($temCount >= 2) {
                        for ($i=1; $i < $temCount; $i++) { ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
                        <?php }
                    } ?>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox" style="margin-top: 25px; margin-bottom: 25px;">
                    <div class="item active">
                      <h4 style="font-family: 'special_eliteregular'; color: #f0f0f0;"><?php echo $temoignages[0]; ?></h4>
                    </div>

                    <?php if ($temCount >= 2) {
                        for ($i=1; $i < $temCount; $i++) { ?>
                            <div class="item">
                              <h4 style="font-family: 'special_eliteregular'; color: #f0f0f0;"><?php echo $temoignages[$i]; ?></h4>
                            </div>
                        <?php }
                    } ?>
                    
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="color: #f0f0f0; margin-left: -15%; padding-top: 30px;">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="color: #f0f0f0; margin-right: -15%; padding-top: 30px;">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>    
            <?php } else { ?>
                <div class="row" style="width: 100%; height: 40px;"></div>
            <?php } ?>
        </div>

        <div class="page-content container" style="width: 60%; margin-left: auto; margin-right: auto;">
        	
        	<div class="row" style="background-color: #f0f0f0; width: 100%; padding-left: 10px; padding-right: 10px;">
        		
        		<div class="text-conferences" style="width: 100%;">
            		<h3 style="color: #f4545f;"><?php echo __("Conférences réalisées"); ?></h3>
            		<h3 style="zoom: 80%;"><?php
            		$confs = get_post_meta( get_the_ID(), 'jp_conf', true );
            		echo $confs;
            		?></h3>
            	</div>

            	<h3 style="color: #f4545f;"><?php echo __("Biographie"); ?></h3>
            	<div id="bio" class="textContent biographie" style="width: 100%;">
                    <?php the_content(); ?><br/>
                </div>

                <h3 style="color: #f4545f;"><?php echo __("Articles média"); ?></h3>
                <div class="row" style="width: 100%;">
                	<?php
                	$idInt = get_the_ID();
                	$ref = $correspTableau[$idInt];
                	$articles = $confMedia[$ref];
                	$compte = count($articles);
                	$rowC = 0;
                	for ($i=0; $i < $compte; $i++) { 
                		$titre = $articles[$i][0];
                		$date = $articles[$i][1];
                		$imageLien = $articles[$i][2];
                		$articleLien = $articles[$i][3]; 
                		$rowC++;
                		?>

                		<div class="col-md-3">
                			<h5 style="color: #f4545f;"><?php echo $date; ?></h5>
                			<h3 style="text-align: justify; font-size: 18px; font-weight: bold;"><?php echo $titre; ?></h3>
                			<img src="<?php echo $imageLien; ?>" style="max-width: 100%;">
                			<a href="<?php echo $articleLien; ?>"><h5><?php echo __('Lire plus') ?></h5></a>
                		</div>

                		<?php 
                		if ( ($rowC%4) == 0 ) { ?>
                			<div class="col-md-12" style="width: 100%; height: 2px; border: 1px dotted; border-color: #f4545f; margin-top: 10px; margin-bottom: 10px;"></div>
                		<?php } ?>

                	<?php }
                	?>
                </div>

                <div class="row" style="margin-top: 40px; margin-bottom: 25px; ">
                	<div class="col-md-5"></div>
            		<div class="col-md-2" style="margin-left: auto; margin-right: auto;">
            			
                        <?php 
                        $confId = get_the_ID();
                        $url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                        $confId = get_the_ID();
                        $classe = "boutonAjoutPanier";
                        $titreBouton = "Ajouter à mes conférenciers";
                        if ( isset($_SESSION["produits"]) ) {
                            $conferenciers = $_SESSION["produits"];
                            $found = false;
                            foreach ( $conferenciers as $conferencier ) {
                                if ( $conferencier == $confId ) { 
                                    $found =true; ?>
                                <?php }
                            }
                            if ( $found == true ) { 
                                $classe = "boutonRetraitPanier"; ?>
                                <a href="<?php echo get_template_directory_uri(); ?>/panier-update.php?remove=<?php echo $confId; ?>&returnUrl=<?php echo $url; ?>">
                                <div class="boutonRetraitPanier hvr-bounce-in" style="height: 90px; width: 90px; margin-left: auto; margin-right: auto;"></div>
                                </a>
                            <?php } else { ?>
                                <a href="#"  onclick="document.forms['formAdd'].submit();">
                                <div class="boutonAjoutPanier hvr-bounce-in" style="height: 90px; width: 90px; margin-left: auto; margin-right: auto;"></div>
                                </a>
                            <?php } 
                        } else { ?>
                            <a href="#"  onclick="document.forms['formAdd'].submit();">
                            <div class="boutonAjoutPanier hvr-bounce-in" style="height: 90px; width: 90px; margin-left: auto; margin-right: auto;"></div>
                            </a>
                        <?php } ?>

            		</div>
                	<div class="col-md-12" style="text-align: center; margin-top: 40px;">
                		<h4 style="margin-bottom: 0px;"><?php echo __('Pour plus de renseignements merci de communiquer') ?></h4>
                		<h4 style="margin-top: 0px;"><?php echo __('au ') ?><a href="" style="color: #f4545f;">514 360-1304</a><?php echo __(' et/ou ') ?><a href="" style="color: #f4545f;">sictalk@sicotterecrutement.ca</a>.</h4>
                	</div>
                </div>

        	</div>

        </div>
      

        <div style="height: 2px; background-color: #848484; width: 12%; margin-left: 44%; margin-top: 2%;"></div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic</a><a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic</a><a href="http://fr.pinterest.com/sicotter/" target="_blank"> icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic</a><a href="https://twitter.com/SICOTTERecrute" target="_blank"> icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>  

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>

    </div>
<script type="text/javascript">
	
<?php 
$ccc = count(get_post_meta( get_the_ID(), 'jp_domaineTitre', true ));
for ($i=0; $i < $ccc; $i++) { ?>
	
	jQuery('.dropD-<?php echo $i; ?>').click(function() {
		jQuery('.det-<?php echo $i; ?>').toggle();
		if (jQuery('.dropD-<?php echo $i; ?>').hasClass("fa-angle-down")) {jQuery('.dropD-<?php echo $i; ?>').removeClass( "fa-angle-down" ).addClass("fa-angle-up");} else {jQuery('.dropD-<?php echo $i; ?>').removeClass( "fa-angle-up" ).addClass("fa-angle-down");};
	});

<?php } ?>

jQuery('.bouton-play').click(function() {
    jQuery('iframe').toggle();
    jQuery('.bouton-play').hide();
    jQuery('#photo-conferencier').css("display", "none");
});

</script>		
<?php get_footer(); ?>

