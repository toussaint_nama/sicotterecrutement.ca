<?php
/*
Template Name: Panier Conférenciers
Author: Aziz Zahar
*/

// Recuperation de la langue de la page
    $url = $_SERVER["REQUEST_URI"];
    $lang = substr($url, 1, 2);

session_start();

get_header(); 
get_template_part( 'searchform-noslide', 'page' ); 

if ( isset($_POST["submitPanier"]) ) {

    if ( (isset($_POST["namePanier"])) && (isset($_POST["mailPanier"])) && (isset($_POST["listePanier"])) ) {
        
        $nomForm = $_POST["namePanier"];
        $mailForm = $_POST["mailPanier"];
        $messageForm = $_POST["messagePanier"];
        $listeForm = $_POST["listePanier"];

        $admin_to      = 'mohamed@agenceapollo.ca';
        $admin_subject = 'Nouvelle requete pour des conférenciers';
        $admin_message = '<html>
                                  <head>
                                    <title>Nouvelle requete pour des conférenciers</title>
                                  </head>
                                  <body>
                                      <p>' . $nomForm . ' a fait une demande pour des conférenciers</p>
                                      <p>La liste des conferenciers séléctionnés est:</p>
                                      <p>'.$listeForm.'</p>
                                      <p>Avec le message suivant:</p>
                                      <p>'.$messageForm.'</p>
                                      <p>Le client peut etre joint par email: '.$mailForm.'</p>                              
                                  </body>
                              </html>';
                    
        $admin_headers  = 'MIME-Version: 1.0' . "\r\n";
        $admin_headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $admin_headers .= 'From: "' . $nomForm . '"<' . $mailForm . '>' . "\r\n";

        wp_mail( $admin_to, $admin_subject, $admin_message, $admin_headers );

        echo '<script type="text/javascript">
              alert("Merci de faire confiance à SICOTTE Recrutement. Votre séléction nous a bien été transmise. Nous reviendrons vers vous le plus vite possible avec de plus amples informations.");
              </script>';

    }
}

echo do_shortcode("[nav_bar_noslide slider='none']");	?>
    
    <div class="po-page">

        <div class="row po-full-width">
        <div class="portfolio-page col-sm-12 column-12" style="background-image: none !important;">
            <div class="liquidcontainerpage">
                <div class="titleHead col-sm-10" style="margin-top: 4%;">
                    <div class="titleTitle">
                        <h1 style="color: #848484;"><?php echo __("Mes Conférenciers"); ?></h1>
                    </div>
                    <div class="reseauxTitle">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-facebook iResTitle"></span></a>
                        <a href="http://twitter.com/intent/tweet/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-twitter iResTitle"></span></a>
                        <a href="http://www.viadeo.com/shareit/share/?url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="icon-viadeo iResTitle"></span></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"><span class="fa fa-linkedin iResTitle"></span></a>
                    </div>
                    
                </div>
            </div>
        </div>
    	</div>
            

        <div class="container">
                

            <div class="row" style="margin-bottom: 10%;">

                <div class="content col-sm-9" style="padding-bottom: 4%;">
                        
                    <div class="up">
                        <!--<h3>Votre panier de conférenciers</h3>-->
                    </div>

                    <div class="liste">
                        <div class="shopping-cart">
							<h2 style="margin-bottom: 8%;">Votre choix de conférenciers</h2>
							<?php
                            $current_url = base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
							if(isset($_SESSION["produits"]))
							{
							    $total = 0;
							    echo '<ol>';
                                $conférenciers = $_SESSION["produits"];
							    foreach ($_SESSION["produits"] as $confId)
							    {
                                    $titre = get_the_title($confId);
                                    $contenu = get_post($confId)->post_content;
                                    $resume = wp_trim_words( $contenu, 50);
                                    $image = wp_get_attachment_url( get_post_thumbnail_id($confId) );
                                    $domaines = get_post_meta( $confId, 'jp_domaineTitre', true );
                                    $lien = get_permalink($confId);
							        echo '<li class="cart-itm" style="text-align: center; margin-bottom: 8%;">';
                                    echo '<a href="'.$lien.'">';
                                    echo '<div class="photoConf" style="background-image: url('.$image.'); background-size: cover; width: 50%; min-height: 300px; margin-left: 25%;">';
                                    echo '<div class="detailConf" style="background-color: rgba(0,0,0,0.6); width: 100%; min-height: 300px; display: none;">';
                                    echo '<p style="padding-top: 20%;">'.$resume.'</p>';
                                    echo '</div>';
                                    echo '</div>';
                                    /*echo '<img src="'.$image.'">';*/
                                    echo '</a>';
							        echo '<h3 style="margin-bottom: 35px;">'.$titre.'</h3>';
                                    foreach ($domaines as $domaine) {
                                        echo '<div style="border: 2px solid; border-radius: 12px; border-color: #f4545f; display: inline-block; padding-left: 5px; padding-right: 5px; margin-right: 5px; margin-bottom: 5px;">';
                                        echo '<h5>'.$domaine.'</h5>';
                                        echo '</div>';
                                    }
                                    echo '<a href="'.get_template_directory_uri().'/panier-update.php?remove='.$confId.'&returnUrl='.$current_url.'" style="text-align: right;"><h5>Retirer du panier</h5></a>';
							        echo '<div style="width: 80%; height: 2px; margin-left: auto; margin-right: auto; background-color: #f4545f; margin-top: 25px;"></div>';
                                    echo '</li>';
							    }
							    echo '</ol>';
							    echo '<span ><a href="'.get_template_directory_uri().'/panier-update.php?emptycart=1&returnUrl='.$current_url.'">Vider le panier</a></span>';
							}else{
							    echo 'Your Cart is empty';
							}
							?>
						</div>
                    </div>

                </div>

                    

                <div class="col-sm-3">

                    <div class="side" style="text-align: center; padding-bottom: 15px; padding-left: 5px; padding-right: 5px;">
                        <div class="boutonSide" style="border: 1px solid; border-color: #f4545f; color: #f4545f; background-color: white; display: inline-block; padding-left: 5px; padding-right: 5px; margin-top: 15px;">
                            <h3 style="margin-top: 10px;">Exporter en PDF</h3>
                        </div>
                        <div class="boutonSide boutonPanier" style="border: 1px solid; border-color: #f4545f; color: #f4545f; background-color: white; display: inline-block; padding-left: 5px; padding-right: 5px; margin-top: 15px;">
                            <h3 style="margin-top: 10px;">Valider mon panier</h3>
                        </div>
                        <div class="formulaire" style="border: 1px solid; border-color: #f4545f; color: #f4545f; background-color: white; display: inline-block; padding-left: 5px; padding-right: 5px; margin-top: 15px; padding-bottom: 8px; display: none;">
                            <?php 
                            if ( isset($_SESSION["produits"]) ) {
                                $conferenciers = $_SESSION["produits"];
                                $liste = "";
                                foreach ($conferenciers as $confId) {
                                    $nom = get_the_title($confId);
                                    $liste .= "- ".$nom."<br>";
                                }
                            }
                            ?>
                            <form  action="<?php the_permalink(); ?>" method="post">
                                <input type="text" name="namePanier" placeholder="Nom">
                                <input type="text" name="mailPanier" placeholder="Courriel">
                                <textarea name="messagePanier" placeholder="Message" rows="5"></textarea>
                                <input type="hidden" name="listePanier" value="<?php echo $liste; ?>">
                                <input type="submit" name="submitPanier" value="Envoyer">
                            </form>
                        </div>
                    </div>

                </div>	

            </div>

        </div>

        <?php echo do_shortcode(' [header paddingtop="20" paddingbottom="20"]
                                    [column width="2" animation="pull-up"]<a href="https://www.youtube.com/user/SiCOTTERecrutement19" target="_blank"> [iconboxsic icon=" fa-youtube"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://www.facebook.com/Sicotterecrutement" target="_blank"> [iconboxsic icon=" fa-facebook" size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="https://ca.linkedin.com/pub/sicotte-recrutement/24/270/b93" target="_blank"> [iconboxsic icon=" fa-linkedin"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://www.viadeo.com/en/profile/sicotte.recrutement" target="_blank"> [iconboxsic icon=" icon-viadeo"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"]<a href="http://fr.pinterest.com/sicotter/" target="_blank"> [iconboxsic icon=" fa-pinterest"  size="20"]</a>[/column]
                                    [column width="2" animation="pull-up"] <a href="https://twitter.com/SICOTTERecrute" target="_blank"> [iconboxsic icon=" fa-twitter"  size="20"]</a>[/column] ' ); ?>

        <div class="clearfix"></div>

        <?php echo do_shortcode('[section background="image-fixed" paddingtop="100" paddingbottom="100" imageurl="'.get_site_url().'/wp-content/uploads/2014/11/header.png"]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-phone" title=""][text align="center" size="25"]<a href="tel:+15143601304">514-360-1304</a>[/text][text align="center" ]'.__("lundi au vendredi").', 9am-6pm [/text] [/iconbox][/column]

                                    [column width="4" animation="bounce-in"][iconbox type="icon-float" icon="fa-envelope" title=""][text align="center" size="25"]<a href="mailto:info@sicotterecrutement.ca">info@sicotterecrutement.ca</a> [/text] [/iconbox][/column]

                                    [column width="4"  animation="bounce-in"][iconbox type="icon-float" icon="fa-comment" title=""][text align="center" size="25"]'.__("129 rue de la Commune Est H2Y 1J1 Montréal Canada").'[/text][/iconbox][/column]

                                    [/section]'); ?>

        <div style="position: absolute; color: darkgray;"><h4>&copy; 2015 - APOLLO L'AGENCE pour SICOTTE Recrutement</h4></div>
        <?php echo do_shortcode('[footerSicotte]'); ?>

<!-- Scripts de pagination -->			
<script type="text/javascript">

jQuery(".boutonSide").mouseover( function() {
    jQuery(this).css("background-color", "#f4545f");
    jQuery(this).css("color", "white");
});

jQuery(".boutonSide").mouseout( function() {
    jQuery(this).css("background-color", "white");
    jQuery(this).css("color", "#f4545f");
});

jQuery(".boutonPanier").click( function() {
    jQuery(".formulaire").toggle();
});

jQuery(".photoConf").mouseover( function() {
    jQuery(this).children().css("display", "block");
});
jQuery(".photoConf").mouseout( function() {
    jQuery(this).children().css("display", "none");
});

</script>

<style type="text/css">
    
    .po-page {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/texturefond.jpg");
    }

    .iResTitle {
        font-size: 26px;
        color: #f4545f;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 1%;
        margin-top: 1%;
    }

    .reseauxTitle {
        margin-bottom: 1%;
    }

    #search-2 > form > input { background-color: white; }

    body > div.po-nav.nav-fixed-padding > div > div.row.po-full-width > div > div { height: 240px; }

    .content, .side {
        background-color: rgb(245,245,245);
        height: 100%;
    }

    .icon-box { zoom: 200%; }

    .footer-container, .footer-container-bottom {
        position: fixed;
    }

    body > div.entry-content > div > div > div > div:nth-child(1) > div > div.icon-bg > div.text-center.float-text > p,
    body > div.entry-content > div > div > div > div:nth-child(3) > div > div.icon-bg > div.text-center.float-text > p {
        color: #F4545F;
    }

    input, textarea {
        width: 100%;
        margin-top: 8px;
    }

    .boutonSide {
        width: 100%;
    }

    .detailConf {
        color: white;
        padding-left: 10px;
        padding-right: 10px;
        text-align: justify;
    }

    #nblikefacebook {
        zoom: 100% !important;
    }

</style>

<?php get_footer(); ?>